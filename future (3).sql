-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 11, 2019 at 03:31 PM
-- Server version: 5.7.25-0ubuntu0.16.04.2
-- PHP Version: 7.1.29-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `future`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog_comments`
--

CREATE TABLE `blog_comments` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL,
  `message` varchar(2042) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog_comments`
--

INSERT INTO `blog_comments` (`id`, `user_id`, `blog_id`, `message`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 45, 1, 'I love this application', '2018-04-26 11:14:57', '2018-04-26 11:14:57', 45, 45),
(2, 45, 1, 'hello', '2018-04-26 11:59:45', '2018-04-26 11:59:45', 45, 45),
(3, 80, 2, 'test blog', '2018-08-27 13:31:18', '2018-08-27 13:31:18', 80, 80),
(4, 45, 4, 'it nice', '2018-08-28 14:48:49', '2018-08-28 14:48:49', 45, 45),
(5, 45, 4, 'happy', '2018-08-28 14:49:00', '2018-08-28 14:49:00', 45, 45),
(6, 45, 2, 'dsds', '2018-08-29 08:53:06', '2018-08-29 08:53:06', 45, 45),
(7, 81, 6, 'qwe', '2018-08-30 09:42:23', '2018-08-30 09:42:23', 81, 81),
(8, 1, 6, 'asdasdasds', '2018-09-07 14:15:52', '2018-09-07 14:15:52', 1, 1),
(9, 81, 4, 'comment', '2018-09-08 05:41:23', '2018-09-08 05:41:23', 81, 81),
(10, 1, 6, 'first', '2018-09-08 07:21:01', '2018-09-08 07:21:01', 1, 1),
(11, 81, 2, 'buyeripad', '2018-09-10 10:54:51', '2018-09-10 10:54:51', 81, 81),
(12, 81, 5, 'buyer ipad', '2018-09-10 10:57:25', '2018-09-10 10:57:25', 81, 81),
(13, 81, 4, '4. comment', '2018-09-10 11:16:21', '2018-09-10 11:16:21', 81, 81),
(14, 80, 1, 'dfdf', '2018-09-12 10:26:23', '2018-09-12 10:26:23', 80, 80),
(15, 80, 1, 'gfdgfdgfdg', '2018-09-12 10:26:26', '2018-09-12 10:26:26', 80, 80),
(16, 98, 1, 'Comment done by sellerads', '2018-09-14 10:49:23', '2018-09-14 10:49:23', 98, 98),
(17, 113, 6, '123', '2018-10-15 10:56:59', '2018-10-15 10:56:59', 113, 113),
(18, 45, 1, 'hii', '2018-10-18 07:23:37', '2018-10-18 07:23:37', 45, 45),
(19, 45, 3, 'aaaaaaa', '2018-10-18 07:26:42', '2018-10-18 07:26:42', 45, 45),
(20, 45, 3, '21212121221', '2018-10-18 07:31:54', '2018-10-18 07:31:54', 45, 45),
(21, 1, 4, 'sdasd', '2018-10-18 11:36:46', '2018-10-18 11:36:46', 1, 1),
(22, 168, 5, 'testing', '2019-01-09 06:28:31', '2019-01-09 06:28:31', 168, 168),
(23, 183, 3, 'Hello World. This is just for testing purpose.', '2019-02-18 11:55:13', '2019-02-18 11:55:13', 183, 183),
(24, 183, 3, 'again', '2019-02-18 11:58:20', '2019-02-18 11:58:20', 183, 183),
(25, 183, 3, 'abc', '2019-02-18 11:59:14', '2019-02-18 11:59:14', 183, 183),
(26, 183, 3, 'xyz', '2019-02-18 12:03:05', '2019-02-18 12:03:05', 183, 183),
(27, 183, 6, 'Good things', '2019-03-22 12:40:47', '2019-03-22 12:40:47', 183, 183),
(28, 198, 6, 'hii guys', '2019-03-23 06:36:28', '2019-03-23 06:36:28', 198, 198),
(29, 216, 1, 'Hi', '2019-03-26 11:10:46', '2019-03-26 11:10:46', 216, 216);

-- --------------------------------------------------------

--
-- Table structure for table `blog_contents`
--

CREATE TABLE `blog_contents` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(225) NOT NULL,
  `blog_img` varchar(225) NOT NULL,
  `content` longtext CHARACTER SET utf8,
  `date` date NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog_contents`
--

INSERT INTO `blog_contents` (`id`, `category_id`, `title`, `blog_img`, `content`, `date`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 1, 'What Makes a Great Author?', 'talent-mall-category-images/blog/great_author.jpg', '<p>Becoming a published author can be a difficult road and understanding what sets great writers apart from mediocre ones is key when you want your writing to stand out against the crowd. If you want to make your work shine, there a few traits every great writer has that allows their work to excel past the competition. If you already have these qualities, then you’re one step closer to the best sellers list, but if you\'re unsure, read on to learn what you should be doing to improve your writing.\n    </p>\n<p>A fantastic writer has the ability to size up any and all content, whether they\'re identifying problems in a speech or knowing exactly where a story fails and why. They also have an instinct for words. Good writers know what will catch attention and how each and every word and sentence flows together to present a story or idea. Remarkable writers are able to connect the dots of every little detail and understand how to bring characters to life, tap into an emotion, and close out a paragraph with lasting meaning.</p>\n<p>Writing is a lot like problem solving and requires the ability to clearly express ideas. Its one thing to get thesaurus-happy when producing a mass creative piece, but it\'s important to understand how to translate big words and ideas into a format that nearly anyone can comprehend.\n    </p>\n<p>If you think you’ve got what it takes to be a famous author, playwright, poet, or any other type of writer, visit FutureStarr.com today and sign up for access to our talent mall and Starr search features. Through\n        <font color="#0000ff"><u><a href="https://www.futurestarr.com/starr-search/">Starr\nsearch</a></u></font>, you’ll be able to read other work from aspiring authors like you and even scope out the competition to see what you might be doing right or wrong.</p>', '2018-04-11', '2018-04-11 14:51:23', '2018-04-11 14:51:23', 1, 1),
(2, 4, 'How to Get Your Foot in the Door of the Music Industry?', 'talent-mall-category-images/blog/dosoftelentmanage2cc8c171.jpg', '<p>Whether you want to be the next big rock star or the one behind the scenes, getting a head start in the music industry isn\'t easy. Very few industries are more exciting to work in and though it\'s not all fame and fortune, a music career can be extremely rewarding for those fit for the job. When you\'re looking to get your foot in the door and sell your talent to record labels, there are a few things you can do that won\'t necessarily guarantee you success, but will put you well on your way towards it.</p>\n<ul>\n        <li/>\n        <p style="margin-bottom: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; line-height: 115%">\n            <span style="display: inline-block; border: none; padding: 0in"><font face="Calibri, serif"><font size="3" style="font-size: 12pt"><span style="background: #ffffff"><font color="#000000"><b>Be\n	specific with what you want.</b> If music is your passion and you\n	just want to be working with it in any way possible, that\'s great.\n	However, choosing the career you want within the industry will allow\n	you to not only pin-point your own personal goals, but to market\n	yourself to potential employers and talent hunters. </span></span>\n            </font>\n            </font>\n            </font\'\n        </p>\n        <li/>\n        <p style="margin-bottom: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; line-height: 115%">\n            <span style="display: inline-block; border: none; padding: 0in"><font face="Calibri, serif"><font size="3" style="font-size: 12pt"><span style="background: #ffffff"><font color="#000000"><b>Invest\n	in education and internships.</b> Whether you\'re self-taught or\n	had been taking classes for years, doing what you can to further\n	your musical education can go a long way in moving you closer to\n	your goal. There are a variety of music-related programs available\n	and these can also lead you into valuable internships that can get\n	you in the door of the company or label you want to work for in the\n	future.</span></span>\n            </font>\n            </font>\n            </font>\n        </p>\n        <li/>\n        <p style="margin-bottom: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; line-height: 115%">\n            <span style="display: inline-block; border: none; padding: 0in"><font face="Calibri, serif"><font size="3" style="font-size: 12pt"><span style="background: #ffffff"><font color="#000000"><b>Join\n	a talent and recruitment agency.</b> At FutureStarr.com, we provide\n	a variety of different resources for talents in all creative\n	industries. You can <font color="#0000ff"><u><a href="https://www.futurestarr.com/talent-mall/">sell\n	your talent</a></u></font> through our talent mall, where users and\n	potential employers can view and purchase your work. Sign up for\n	Future Starr today and get on the path to a bright musical career!</span></span>\n            </font>\n            </font>\n            </font>\n        </p>\n    </ul>', '2018-04-11', '2018-04-11 14:51:23', '2018-04-11 14:51:23', 1, 1),
(3, 7, 'So you want to be America’s Next Male Model?', 'talent-mall-category-images/blog/America_Next_Male_Model.jpg', '<p>A career in modeling\nrequires a lot of hard work and dedication and is just as difficult\nfor men as it can be for women. If you think you have what it takes\nto become a male model you have a long road ahead, but with some\npatience and determination you could very well find yourself in the\nfashion world someday.</p>\n<p >One of the first and\nmost important steps is to gain exposure. You can do this by finding\nand submitting your photos to <font color="#0000ff"><u><a href="https://www.futurestarr.com/modeling/">male\nmodeling agencies</a></u></font> such as FutureStarr.com. Just like\nwith women, there are industry standards that most men are expected\nto meet in order to receive work in the modeling industry.</p>\n<ul>\n	<li/>\n<p style="margin-bottom: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; line-height: 115%">\n	<span style="display: inline-block; border: none; padding: 0in"><font face="Calibri, serif"><font size="3" style="font-size: 12pt"><span style="background: #ffffff"><font color="#000000">Industry\n	standards require men to be between 5\'11” and 6\'2”.</span></span></font></font></font></p>\n	<li/>\n<p style="margin-bottom: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; line-height: 115%">\n	<span style="display: inline-block; border: none; padding: 0in"><font face="Calibri, serif"><font size="3" style="font-size: 12pt"><span style="background: #ffffff"><font color="#000000">The\n	“young men\'s” market requires models aged from 15 to 25.</span></span></font></font></font></p>\n	<li/>\n<p style="margin-bottom: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; line-height: 115%">\n	<span style="display: inline-block; border: none; padding: 0in"><font face="Calibri, serif"><font size="3" style="font-size: 12pt"><span style="background: #ffffff"><font color="#000000">The\n	“adult men\'s” market then requires models aged 25 to 35.</span></span></font></font></font></p>\n	<li/>\n<p style="margin-bottom: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; line-height: 115%">\n	<span style="display: inline-block; border: none; padding: 0in"><font face="Calibri, serif"><font size="3" style="font-size: 12pt"><span style="background: #ffffff"><font color="#000000">Average\n	weight for men is expected to be between 140 and 165 pounds, but can\n	vary depending on you BMI.</span></span></font></font></font></p>\n	<li/>\n<p style="margin-bottom: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; line-height: 115%">\n	<span style="display: inline-block; border: none; padding: 0in"><font face="Calibri, serif"><font size="3" style="font-size: 12pt"><span style="background: #ffffff"><font color="#000000">Average\n	measurements are 40 regular to 42 long.</span></span></font></font></font></p>\n</ul>\n<p style="margin-bottom: 0in; line-height: 115%">At FutureStarr.com,\nwe give you the opportunity to promote yourself through our Talent\nMall to get yourself in front of potential employers and other male\nmodeling agencies. If you think you\'ve got what it takes to be the\nhottest new male model, sign up for an account with us today!</p>', '2018-08-28', '2018-08-28 14:00:06', '2018-08-28 14:00:06', 1, 1),
(4, 1, 'The\r\n“Dos” of Talent Management', 'talent-mall-category-images/blog/talent_management.jpg', '<p style="margin-bottom: 0in; line-height: 115%">When you\'re in the\nbusiness of <font color="#0000ff"><u><a href="http://futurestarr.com/future_star/contact-us">talent\nmanagement</a></u></font>, it\'s important to make sure you\'re\nutilizing all of your resources and skills to not only keep business\nafloat, but to keep your talent happy as well. Below are 3 tips on\nhow to run a successful agency.</p>\n<p align="left" style="margin-bottom: 0.14in; line-height: 0.2in"><b>1.\nCreate the Right Look</b></p>\n<p style="margin-bottom: 0in; line-height: 115%">It\'s important\nthat the talent and recruitment facing appearance doesn\'t only fit\nwith the industry you\'re marketing towards, but also shows that\nyour company produces results. By featuring jobs and talent\nprogressions through social media and other aspects of your business,\nyou\'ll prove that you\'re what the client is looking for and that\nyou\'ll help them get to where they want to be.</p>\n<p align="left" style="margin-bottom: 0.14in; line-height: 0.2in"><b>2.\nNever Stop Investing in Your Talent and Staff</b></p>\n<p style="margin-bottom: 0in; line-height: 115%">By executing\ndevelopment plans, learning materials (digital or physical), and\ntraining teams or courses can help keep your employees and talent\ninvolved within the company so they don\'t feel that they\'re on\ntheir own in terms of promotion and getting the skills and assistance\nthey may need.</p>\n<p align="left" style="margin-bottom: 0.14in; line-height: 0.2in"><b>3.\nIdentify Potential Early On</b></p>\n<p style="margin-bottom: 0in; line-height: 115%">By identifying what\npeople want when they join your agency right away, you can work to\nhelp them progress through your talent management while still meeting\nbusiness objectives. Recruitment is only the first step for your\ncompany and in order to maintain your client base and bring in new\ntalent, you\'ll need to do everything possible to help each\nindividual succeed.</p>', '2018-08-28', '2018-08-28 14:01:32', '2018-08-28 14:01:32', 1, 1),
(5, 16, 'Tattoo\r\nArtists: What Happens When You Choose this Awesome Career Path', 'talent-mall-category-images/blog/tatoo_artist_290820180101.jpg', '<p>Becoming a\nprofessional tattoo artist demands a lot of time, talent, and\ntenacity. Well-known and sought-after experts in this field go\nthrough numerous hurdles before they achieve their goal. The good\nnews is that there are tried and true methods on how you can become a\nrespected and certified body arts specialist. \n</p>\n\n<p ><b>Surefire\nWays to Become a Professional Tattoo Artist</b></p>\n\n<p >Learning the trade\nand continuously improving your craft as a tattoo artist is an\nongoing process. There are new trends and techniques about body arts\nthat debut in its niche every now and then. \n</p>\n\n<p >Here are some of the\nimportant ways to help you have a solid stronghold for a successful\ncareer:</p>\n\n<p ><b>Understand\nyour Artwork</b></p>\n\n<p >Tattoos are not just\nabout inking the body with graphics and images. As a certified tattoo\nartist, you need to know the heart and soul of your artwork.\nTattooing is a craft and not just superficial skin arts. The best way\nto establish your career as a tattoo artist is to imbibe a passionate\ndrive in learning tattoos from their origins to the latest trends. \n</p>\n\n<p ><b>Be\nVersatile in your Talent</b></p>\n\n<p >Body art is a\ndynamic and ever-changing field. Thus, you need to maintain an open\nmind if you want better opportunities, new learning, and more\nprofound ideas. You can focus on one particular genre but make sure\nyou also know the other facets of body arts. Talented and flexible\ntattoo artists specialize in various fields from Celtic to Asian to\ntribal and so much more.</p>\n\n<p ><b>Persevere\nand Persevere Some More</b></p>\n\n<p >Learning the ropes\nof the trade is different from mastery of the trade. Tattooing\nrequires hard work, dedication, and perseverance. Even if you have\nthe raw materials and talents, you need to learn, train, and perfect\nyour craft. The adage “Practice Makes Perfect” is highly\napplicable in this particular field. Make sure you are also open to\nnew trends and more advanced techniques to cope with the demands of\nyour customer base. \n</p>\n\n<p ><b>Become\nan Apprentice</b></p>\n\n<p >Every master tattoo\nartist starts with becoming a student in their trade. Thus,\napprenticeship is a vital part of your journey to become the best in\nyour craft. Find established artists that will take you on as an\napprentice. Apprenticeship usually takes place for a specific period\nof time that you agreed upon. It takes a lot of observation and\npractice before an aspiring tattoo artist is allowed to use the\nmachine and ink living skin.</p>\n\n<p ><b>Use\nTalent Marketing Sites</b></p>\n\n<p >Once you are fully\nready to jump start your career, it is high time to find your\ncustomer base. Nowadays, it is extra challenging to compete with\nother tattoo artists. You can have an edge if you use online talent\nmarketing sites to promote your services, prosper online, and\nestablish your clientele. \n</p>\n\n<p >The art of tattooing\nis becoming more than just a form of body art. More and more people\ndiscover the essence of tattoos and the beauty of self-expression\nwith it. Begin your journey as a certified tattoo artist today!</p>', '2018-08-29', '2018-08-29 13:39:36', '2018-08-29 13:39:36', 1, 1),
(6, 2, 'One Thing All Successful Celebrities Have in Common', '', '<p>Do you want to be\nsuccessful? It is not enough to want success, you must be passionate\nabout it, you must be determined and ready to make sacrifices to\nachieve what you want. In this time and age, talent is overrated. In\norder to become successful in your niche, you need to sell your\ntalent online. Your approach must be creative and long-reaching. Show\nprospects that you have the edge against your competitors. Give them\nthe reason why they should pick you among dozens of other qualified\ncandidates. Before you outline your skills to your potential audience\nor employer, it is imperative to know how you can actually reach\nthem. \n</p>\n\n<p >\n<b>Sell your Talent Online with a Trusted Online Talent Marketplace</b></p>\n\n<p >A talent marketplace\nis the best starting point where you can reach your target employer\nor audience. Whatever talent you may have, you can get the services\nyou need in order to promote it and have your exposure. Talent\nmarketing allows you to be discovered. It is a vehicle where you can\noffer fresh ideas and share your talent to your preferred niche.</p>\n\n<p >The Internet is huge\nand free resources where you can jump start your endeavors. You sell\nyour talent online and get immediate results. Opting for talent\nmarketing in the Internet allows you to be more creative and even\nsave time, money, and effort. There\'s a big difference when you\npromote your skills and talents online than personally going to your\ntarget employer. It gives you farther, wider, and longer reach. \n</p>\n\n<p >\n<b>Helpful Pointers for Budding Talents</b></p>\n\n<p ><span style="background: #ffffff"><font color="#000000"><span style="background: #ffffff">Once\nyou have found the right venue where you can </span></font>sell your\ntalent online, it is about time you work on your promotional and\nmarketing schemes. Here are some practical and proven tips:</span></p>\n\n<ul>\n	<li/>\n<p ><font color="#000000"><b>Find\n	and Focus on Your Niche</b></font></p>\n</ul>\n\n<p ><font color="#000000">Identifying\nyour niche definitely works. The key is for you to have a solid and\ncoherent plan that focuses on the target audience or market. Your\nniche is your forte, somewhere your talent is most applicable and\nwhere you can work on your success in the future.</font></p>\n\n<ul>\n	<li/>\n<p ><font color="#000000"><b>Start\n	and Keep Going</b></font></p>\n</ul>\n\n<p ><span style="background: #ffffff"><font color="#000000"><span style="background: #ffffff">It\nis not enough that you </span></font>sell your talent online. Start\nyour talent marketing venture and never give up because there will\nalways be hurdles along the way. Stick with your plan, nurture it,\nand keep going especially when you encounter failures.</span></p>\n\n<p ><span style="background: #ffffff">The\nfirst step to realizing your dreams is to sell your talent online.\nThe good news is that you have numerous tools and resources to use\nfor endorsing your skills, whatever they may be. Find a trusted and\nreputable talent agency now!</span></p>', '2018-08-29', '2018-08-29 13:40:10', '2018-08-29 13:40:10', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `buyer_contacts`
--

CREATE TABLE `buyer_contacts` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `talent_id` int(10) NOT NULL,
  `msg_title` varchar(255) NOT NULL,
  `message` varchar(255) NOT NULL,
  `created_by` int(10) NOT NULL,
  `updated_by` int(10) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `buyer_contacts`
--

INSERT INTO `buyer_contacts` (`id`, `user_id`, `talent_id`, `msg_title`, `message`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 137, 'test', 'hello futuer starr', 1, 1, '2018-04-16 11:20:51', '2018-04-16 11:20:51'),
(2, 1, 137, 'test1', 'he', 1, 1, '2018-04-16 11:23:14', '2018-04-16 11:23:14'),
(3, 45, 137, 'test', 'hello', 45, 45, '2018-04-18 06:16:34', '2018-04-18 06:16:34'),
(4, 45, 178, 'dfsdf', 'fdgd', 45, 45, '2018-07-13 11:06:52', '2018-07-13 11:06:52'),
(5, 45, 178, 'dfsdf', 'fdgd', 45, 45, '2018-07-13 11:06:53', '2018-07-13 11:06:53'),
(6, 45, 178, 'dfsdf', 'fdgd', 45, 45, '2018-07-13 11:06:53', '2018-07-13 11:06:53'),
(7, 45, 178, 'dfsdf', 'fdgd', 45, 45, '2018-07-13 11:06:53', '2018-07-13 11:06:53'),
(8, 45, 173, 'gfdg', 'dfgdfg', 45, 45, '2018-07-16 09:14:37', '2018-07-16 09:14:37'),
(9, 45, 173, 'gfdg', 'dfgdfg', 45, 45, '2018-07-16 09:14:38', '2018-07-16 09:14:38'),
(10, 45, 173, 'gfdg', 'dfgdfg', 45, 45, '2018-07-16 09:14:38', '2018-07-16 09:14:38'),
(11, 45, 173, 'gfdg', 'dfgdfg', 45, 45, '2018-07-16 09:14:38', '2018-07-16 09:14:38'),
(12, 45, 174, 'dfgdf', 'dfgdfgdg', 45, 45, '2018-07-16 09:15:00', '2018-07-16 09:15:00'),
(13, 45, 173, 'chetan', 'chouahn chouahn', 45, 45, '2018-07-16 12:13:38', '2018-07-16 12:13:38'),
(14, 45, 173, 'dgdf', 'fdgdfgdfg', 45, 45, '2018-07-26 09:23:04', '2018-07-26 09:23:04'),
(15, 45, 173, 'GHGH', 'CHETAN CHOUHAN', 45, 45, '2018-07-26 10:11:25', '2018-07-26 10:11:25'),
(16, 45, 173, 'chetan chouhan', 'i love this project and its id', 45, 45, '2018-07-26 11:26:03', '2018-07-26 11:26:03'),
(17, 45, 174, 'FHFGH', 'CHETAN CHOUHAN', 45, 45, '2018-07-26 11:45:18', '2018-07-26 11:45:18'),
(18, 45, 173, 'dfgdfg', 'fsgdfg', 45, 45, '2018-07-30 13:03:30', '2018-07-30 13:03:30'),
(19, 45, 173, 'dsfsdf', 'dfgfdgfdgf', 45, 45, '2018-07-31 06:52:11', '2018-07-31 06:52:11'),
(20, 45, 173, 'dfgdfg', 'dfgdfgfg', 45, 45, '2018-07-31 06:52:30', '2018-07-31 06:52:30'),
(21, 45, 173, 'dfgdfgfg', 'dfgdfgfg', 45, 45, '2018-07-31 06:52:51', '2018-07-31 06:52:51'),
(22, 45, 173, 'dfgdfgfg', 'dfgdfgfg', 45, 45, '2018-07-31 06:53:05', '2018-07-31 06:53:05'),
(23, 45, 173, 'fgfh', 'fghgh', 45, 45, '2018-07-31 07:07:26', '2018-07-31 07:07:26'),
(24, 45, 173, 'fgfh', 'fghgh', 45, 45, '2018-07-31 07:07:35', '2018-07-31 07:07:35'),
(25, 45, 173, 'nmfghfg', 'fghfghfgh', 45, 45, '2018-07-31 07:07:54', '2018-07-31 07:07:54'),
(26, 45, 173, 'responsive', 'Responsiveness', 45, 45, '2018-08-06 06:36:28', '2018-08-06 06:36:28'),
(27, 45, 173, 'aaaaaaaaaaa', 'ssssssssssssssss', 45, 45, '2018-08-06 15:28:44', '2018-08-06 15:28:44'),
(28, 45, 173, 'test message function', 'Test data demo data (tester.)', 45, 45, '2018-08-07 05:40:25', '2018-08-07 05:40:25'),
(29, 45, 173, 'test message function', 'Test data demo data (tester.)', 45, 45, '2018-08-07 05:40:26', '2018-08-07 05:40:26'),
(30, 45, 173, 'test message function', 'Test data demo data (tester.)', 45, 45, '2018-08-07 05:40:27', '2018-08-07 05:40:27'),
(31, 45, 173, 'test message function', 'Test data demo data (tester.)', 45, 45, '2018-08-07 05:40:28', '2018-08-07 05:40:28'),
(32, 45, 173, 'test message function', 'Test data demo data (tester.)', 45, 45, '2018-08-07 05:40:28', '2018-08-07 05:40:28'),
(33, 45, 173, 'message sent to', 'FIVE SELLER', 45, 45, '2018-08-07 05:40:56', '2018-08-07 05:40:56'),
(34, 45, 173, 'qwee', 'qwe', 45, 45, '2018-08-07 05:46:35', '2018-08-07 05:46:35'),
(35, 45, 173, 'yjg', 'kmhju', 45, 45, '2018-08-07 07:09:06', '2018-08-07 07:09:06'),
(36, 63, 170, 'New Buyer', 'buyer test message to five seller', 63, 63, '2018-08-07 10:03:10', '2018-08-07 10:03:10'),
(37, 45, 173, 'vvvvvvvvvvvvvv', 'vvvvvvvvvvvvvv', 45, 45, '2018-08-07 11:33:25', '2018-08-07 11:33:25'),
(38, 45, 173, 'fffffffff', 'fff', 45, 45, '2018-08-07 11:34:26', '2018-08-07 11:34:26'),
(39, 63, 174, 'buyer 15', 'text', 63, 63, '2018-08-07 12:35:22', '2018-08-07 12:35:22'),
(40, 63, 180, 'praveen', 'praveen', 63, 63, '2018-08-07 13:01:36', '2018-08-07 13:01:36'),
(41, 45, 173, 'qwe', 'message from five buyer', 45, 45, '2018-08-07 13:22:11', '2018-08-07 13:22:11'),
(42, 45, 173, 'five buyer', 'message from five buyer for 3rd product', 45, 45, '2018-08-07 13:22:43', '2018-08-07 13:22:43'),
(43, 45, 174, 'annu', 'agarwal', 45, 45, '2018-08-08 13:51:02', '2018-08-08 13:51:02'),
(44, 45, 174, 'Here', 'Here', 45, 45, '2018-08-08 15:01:22', '2018-08-08 15:01:22'),
(45, 63, 181, 'seller', 'seller', 63, 63, '2018-08-10 12:01:04', '2018-08-10 12:01:04'),
(46, 63, 181, 'buyer 15', 'message from Buyer15', 63, 63, '2018-08-10 12:32:01', '2018-08-10 12:32:01'),
(47, 63, 187, 'seller42', 'sekker42', 63, 63, '2018-08-11 06:30:14', '2018-08-11 06:30:14'),
(48, 1, 192, 'Price for your product.', 'How much is your item?', 1, 1, '2018-08-16 03:42:03', '2018-08-16 03:42:03'),
(49, 81, 209, 'Test', 'Test', 81, 81, '2018-08-20 10:15:33', '2018-08-20 10:15:33'),
(50, 63, 193, 'qwerty', 'qwerty', 63, 63, '2018-08-21 10:28:13', '2018-08-21 10:28:13'),
(51, 81, 212, 'Buyer Ipad', 'Message send from buyer dashboard...', 81, 81, '2018-08-24 09:32:26', '2018-08-24 09:32:26'),
(52, 81, 212, 'Buyer ipad', 'Buyer ipad\'s message', 81, 81, '2018-08-24 09:34:47', '2018-08-24 09:34:47'),
(53, 80, 204, 'qwe', 'qweqw', 80, 80, '2018-08-28 09:09:09', '2018-08-28 09:09:09'),
(54, 80, 204, 'qwe', 'qwe', 80, 80, '2018-08-28 09:09:19', '2018-08-28 09:09:19'),
(55, 1, 211, 'How much?', 'Hi, how much is your new product?', 1, 1, '2018-08-29 03:20:08', '2018-08-29 03:20:08'),
(56, 92, 240, 'qwerty', 'qwerty', 92, 92, '2018-09-03 10:32:31', '2018-09-03 10:32:31'),
(57, 1, 240, 'Test', 'How much is your product?', 1, 1, '2018-09-04 03:06:43', '2018-09-04 03:06:43'),
(58, 45, 174, 'Your product', 'When are you coming out with more merchandise?', 45, 45, '2018-09-09 02:55:51', '2018-09-09 02:55:51'),
(59, 99, 260, 'Message', 'Test message from Buyer ads', 99, 99, '2018-09-14 06:24:16', '2018-09-14 06:24:16'),
(60, 185, 234, 'How much', 'What is the price of your product', 185, 185, '2018-12-27 05:38:20', '2018-12-27 05:38:20'),
(61, 197, 257, 'Your product', 'How much?', 197, 197, '2019-03-16 07:32:15', '2019-03-16 07:32:15'),
(62, 211, 276, 'message message', 'write here you prblem', 211, 211, '2019-03-26 10:00:33', '2019-03-26 10:00:33'),
(63, 184, 284, 'Hi', 'Can you tell me more about your product?', 184, 184, '2019-04-19 07:52:07', '2019-04-19 07:52:07');

-- --------------------------------------------------------

--
-- Table structure for table `buyer_products`
--

CREATE TABLE `buyer_products` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `buyer_id` int(11) NOT NULL,
  `talent_id` int(10) NOT NULL,
  `active` int(10) NOT NULL DEFAULT '1',
  `date` date NOT NULL,
  `created_by` int(10) DEFAULT NULL,
  `upadted_by` int(10) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `buyer_products`
--

INSERT INTO `buyer_products` (`id`, `user_id`, `buyer_id`, `talent_id`, `active`, `date`, `created_by`, `upadted_by`, `created_at`, `updated_at`) VALUES
(1, 197, 197, 277, 1, '2019-06-09', NULL, NULL, '2019-06-09 05:16:18', '2019-06-09 05:16:18'),
(2, 197, 197, 277, 1, '2019-06-09', NULL, NULL, '2019-06-09 05:20:59', '2019-06-09 05:20:59'),
(3, 197, 197, 277, 1, '2019-06-09', NULL, NULL, '2019-06-09 05:25:03', '2019-06-09 05:25:03'),
(4, 197, 197, 274, 1, '2019-06-09', NULL, NULL, '2019-06-09 05:25:05', '2019-06-09 05:25:05'),
(5, 197, 197, 277, 1, '2019-06-09', NULL, NULL, '2019-06-09 05:27:54', '2019-06-09 05:27:54'),
(6, 197, 197, 274, 1, '2019-06-09', NULL, NULL, '2019-06-09 05:27:55', '2019-06-09 05:27:55'),
(7, 197, 197, 285, 1, '2019-06-09', NULL, NULL, '2019-06-09 05:40:16', '2019-06-09 05:40:16'),
(8, 197, 197, 235, 1, '2019-06-09', NULL, NULL, '2019-06-09 12:06:30', '2019-06-09 12:06:30'),
(9, 197, 197, 272, 1, '2019-06-11', NULL, NULL, '2019-06-11 18:11:36', '2019-06-11 18:11:36');

-- --------------------------------------------------------

--
-- Table structure for table `chats`
--

CREATE TABLE `chats` (
  `id` int(11) NOT NULL,
  `sent_by` varchar(255) NOT NULL,
  `received_by` varchar(255) NOT NULL,
  `last_activity` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_message_sender` varchar(255) NOT NULL,
  `last_message` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  `message_media` varchar(255) DEFAULT NULL,
  `deleted_sent_by` datetime DEFAULT NULL,
  `deleted_received_by` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `chats`
--

INSERT INTO `chats` (`id`, `sent_by`, `received_by`, `last_activity`, `last_message_sender`, `last_message`, `message_media`, `deleted_sent_by`, `deleted_received_by`) VALUES
(1, '113', '103', '2018-10-15 09:57:39', '113', '', '', '2018-10-15 10:03:15', NULL),
(2, '113', '94', '2018-10-15 09:58:11', '113', '', '', '2018-10-15 10:03:11', NULL),
(3, '113', '85', '2018-10-15 12:38:46', '113', '', '', '2018-10-15 12:38:59', NULL),
(4, '113', '112', '2018-10-15 12:35:54', '113', '158', NULL, '2018-10-15 10:14:25', NULL),
(5, '113', '47', '2018-10-15 12:37:53', '113', 'delete', NULL, NULL, NULL),
(6, '45', '103', '2018-10-15 13:34:46', '45', '', '', '2018-10-15 13:40:55', NULL),
(7, '45', '113', '2018-10-15 13:41:43', '45', '', '', '2018-10-15 13:42:01', NULL),
(8, '45', '104', '2018-10-16 12:20:46', '45', NULL, 'uploads/message-media/45-20181016122046video-image.jpg', NULL, NULL),
(9, '1', '1', '2018-10-16 06:42:33', '1', 'Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.', 'uploads/message-media/1-20181016064233fitness2.jpg', NULL, NULL),
(10, '45', '47', '2018-10-18 09:29:32', '45', NULL, 'uploads/message-media/45-20181018092932184-download-(copy).jpeg', '2018-10-16 13:22:17', NULL),
(11, '116', '94', '2018-10-17 06:37:08', '116', '', '', '2018-10-17 06:37:19', NULL),
(12, '168', '172', '2019-01-09 06:30:10', '168', 'Hi', NULL, NULL, NULL),
(13, '168', '125', '2018-11-11 10:02:57', '168', 'Hi Roshan\r\n😄', NULL, NULL, NULL),
(14, '125', '172', '2018-11-11 13:31:23', '172', 'Test', NULL, NULL, NULL),
(15, '1', '181', '2019-04-04 16:56:07', '1', 'Automatic message system', '', NULL, NULL),
(16, '185', '172', '2018-12-27 05:37:16', '185', '😃', NULL, NULL, NULL),
(17, '185', '1', '2019-03-21 22:45:17', '1', '', '', '2019-03-21 22:45:40', NULL),
(18, '183', '192', '2019-03-14 09:34:15', '183', 'test', NULL, NULL, NULL),
(19, '197', '1', '2019-05-12 01:40:53', '197', 'Test', NULL, NULL, NULL),
(20, '197', '195', '2019-04-09 18:38:38', '197', 'test alpha', NULL, NULL, NULL),
(21, '197', '196', '2019-03-16 07:34:18', '197', 'Test Avi 😉', NULL, NULL, NULL),
(22, '197', '175', '2019-04-19 07:36:20', '197', 'New test', NULL, NULL, NULL),
(23, '1', '202', '2019-04-25 15:36:56', '1', 'Another test', NULL, NULL, NULL),
(24, '212', '211', '2019-03-26 11:57:30', '211', 'gfdhghjgdhfgdhfgj', NULL, NULL, NULL),
(25, '211', '195', '2019-03-26 06:58:28', '211', '😃', NULL, NULL, NULL),
(26, '212', '191', '2019-03-26 11:37:27', '212', 'hi php', NULL, NULL, NULL),
(27, '184', '212', '2019-04-19 07:52:07', '184', 'Can you tell me more about your product?', NULL, NULL, NULL),
(28, '220', '221', '2019-04-25 15:32:23', '220', 'test', NULL, NULL, NULL),
(29, '1', '234', '2019-04-28 15:24:09', '1', 'alpha', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `chat_messages`
--

CREATE TABLE `chat_messages` (
  `id` int(11) NOT NULL,
  `chat_id` varchar(255) NOT NULL,
  `sent_by` varchar(255) NOT NULL,
  `message` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  `message_media` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `read_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 = Read, 0 = Unread'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `chat_messages`
--

INSERT INTO `chat_messages` (`id`, `chat_id`, `sent_by`, `message`, `message_media`, `created_at`, `read_flag`) VALUES
(1, '1', '113', '123', NULL, '2018-10-15 09:57:39', 0),
(2, '2', '113', '741', NULL, '2018-10-15 09:58:11', 0),
(3, '3', '113', '7532145986', NULL, '2018-10-15 09:58:30', 0),
(4, '4', '113', '75', NULL, '2018-10-15 10:03:39', 0),
(5, '4', '113', '123', NULL, '2018-10-15 12:35:14', 0),
(6, '4', '113', '158', NULL, '2018-10-15 12:35:54', 0),
(7, '5', '113', 'delete', NULL, '2018-10-15 12:37:53', 0),
(8, '3', '113', 'jk', NULL, '2018-10-15 12:38:46', 0),
(9, '6', '45', 'aaaa', NULL, '2018-10-15 13:34:46', 0),
(10, '7', '45', 'aaaaa', NULL, '2018-10-15 13:39:07', 0),
(11, '7', '45', 'kjhkfdajhk;fdj;kjfd;kljfd;lkjfkldjfd', NULL, '2018-10-15 13:40:30', 0),
(12, '7', '45', 'hiii', NULL, '2018-10-15 13:41:43', 0),
(13, '8', '45', 'AAAA', NULL, '2018-10-15 15:15:55', 1),
(14, '8', '45', 'hi', NULL, '2018-10-16 05:37:43', 1),
(15, '8', '45', 'hiiii', NULL, '2018-10-16 05:38:13', 1),
(16, '8', '45', 'laaaaaaaaaaaast', NULL, '2018-10-16 05:38:43', 1),
(17, '8', '45', 'hii', NULL, '2018-10-16 05:42:51', 1),
(18, '8', '45', '12132', NULL, '2018-10-16 05:42:56', 1),
(19, '8', '45', 'heelooo', NULL, '2018-10-16 06:02:28', 1),
(20, '8', '45', '😁😁', NULL, '2018-10-16 06:04:11', 1),
(21, '8', '45', '😍', NULL, '2018-10-16 06:04:25', 1),
(22, '8', '45', NULL, 'uploads/message-media/45-20181016061611test-(2).pdf', '2018-10-16 06:16:11', 1),
(23, '8', '45', NULL, 'uploads/message-media/45-20181016062852writer.jpg', '2018-10-16 06:28:52', 1),
(24, '8', '45', '🤣', NULL, '2018-10-16 06:38:26', 1),
(25, '8', '45', '😃', NULL, '2018-10-16 06:38:35', 1),
(26, '9', '1', NULL, 'uploads/message-media/1-20181016063849entertainment2.jpg', '2018-10-16 06:38:49', 0),
(27, '8', '45', '😍', NULL, '2018-10-16 06:38:52', 1),
(28, '9', '1', 'Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.  Unless explicitly stated otherwise, this Privacy Policy covers any current, future, modified, updated and new products, properties, tools and services provided by FutureStarr.com. You acknowledge that your use of our Website shall be subject to this Privacy Policy and the FutureStarr Terms and Conditions, whether or not you register for an account, upload your products, or opt in to, interact with or inquire about any other services or products provided by FutureStarr.\r\n\r\nFutureStarr is an online, easy-to-use talent distribution platform based in Atlanta, GA where unsigned or undiscovered talent and talent lovers coexist. We have one mission: To give unsigned, undiscovered talent/artists more exposure and connect them with a larger audience of listeners and future fans who want to discover, promote and share their music. For more information about our goals and mission here at FutureStarr, and to read about our Founder and CEO, please visit our About Us page.', 'uploads/message-media/1-20181016064233fitness2.jpg', '2018-10-16 06:42:33', 0),
(29, '8', '45', NULL, 'uploads/message-media/45-20181016122046video-image.jpg', '2018-10-16 12:20:46', 1),
(30, '10', '45', 'a', NULL, '2018-10-16 13:21:38', 0),
(31, '10', '45', 'hi', NULL, '2018-10-16 13:21:54', 0),
(32, '10', '45', 'hhhhh', NULL, '2018-10-16 13:21:57', 0),
(33, '11', '116', '212', NULL, '2018-10-17 06:37:08', 0),
(34, '10', '45', 'hi', NULL, '2018-10-18 09:29:11', 0),
(35, '10', '45', NULL, 'uploads/message-media/45-20181018092932184-download-(copy).jpeg', '2018-10-18 09:29:32', 0),
(36, '12', '168', 'Hello', NULL, '2018-11-11 10:01:32', 1),
(37, '12', '168', 'How are you?😆', NULL, '2018-11-11 10:01:54', 1),
(38, '13', '168', 'Hi Roshan\r\n😄', NULL, '2018-11-11 10:02:57', 1),
(39, '14', '125', 'Hi testing\n😃', NULL, '2018-11-11 13:28:42', 1),
(40, '14', '172', 'Test', NULL, '2018-11-11 13:31:23', 1),
(41, '12', '172', 'Good. Thank you for testing.', NULL, '2018-11-11 13:32:09', 1),
(42, '15', '1', 'Hi', NULL, '2018-12-04 14:12:07', 1),
(43, '15', '181', 'Hi Five Seller', NULL, '2018-12-04 14:14:23', 1),
(44, '16', '185', 'Hello There?', NULL, '2018-12-27 05:37:06', 0),
(45, '16', '185', '😃', NULL, '2018-12-27 05:37:16', 0),
(46, '17', '185', 'What is the price of your product', NULL, '2018-12-27 05:38:20', 1),
(47, '17', '1', 'I\'m selling my product for $10', NULL, '2018-12-27 05:48:30', 1),
(48, '12', '168', 'Hi', NULL, '2019-01-09 06:30:10', 0),
(49, '18', '183', 'asfasfasfasfsafasfaSFAsf', 'uploads/message-media/183-20190220111752survey.png', '2019-02-20 11:17:52', 0),
(50, '18', '183', 'dddd', NULL, '2019-03-04 10:03:15', 0),
(51, '18', '183', 'test', NULL, '2019-03-14 09:34:15', 0),
(52, '19', '197', 'How much?', NULL, '2019-03-16 07:32:15', 1),
(53, '20', '197', 'Test', NULL, '2019-03-16 07:33:53', 0),
(54, '21', '197', 'Test Avi 😉', NULL, '2019-03-16 07:34:18', 0),
(55, '22', '197', 'Test', NULL, '2019-03-16 07:34:42', 0),
(56, '17', '185', 'would you like to purchase?', NULL, '2019-03-21 22:45:17', 1),
(57, '17', '1', 'Automatic message system', '', '2019-03-21 22:45:17', 1),
(58, '23', '1', 'Hello, Rahul J', NULL, '2019-03-23 07:28:03', 1),
(59, '23', '1', 'Automatic Message system', 'uploads/message-media/1-20190323072858result-clipboard.png', '2019-03-23 07:28:58', 1),
(60, '23', '202', 'hello Five Seller', NULL, '2019-03-23 07:31:05', 1),
(61, '24', '212', 'hi anjali', NULL, '2019-03-26 06:52:06', 1),
(62, '24', '211', 'hello lakshay', NULL, '2019-03-26 06:54:01', 1),
(63, '24', '211', 'how are you doing!', NULL, '2019-03-26 06:54:37', 1),
(64, '24', '212', 'good ! you tell ?', NULL, '2019-03-26 06:55:15', 1),
(65, '25', '211', 'hi ajay', NULL, '2019-03-26 06:58:20', 0),
(66, '25', '211', '😃', NULL, '2019-03-26 06:58:28', 0),
(67, '24', '211', 'write here you prblem', NULL, '2019-03-26 10:00:33', 1),
(68, '24', '212', 'hello', NULL, '2019-03-26 10:48:04', 1),
(69, '24', '211', 'hello anjali', NULL, '2019-03-26 10:49:50', 1),
(70, '24', '212', 'hello', NULL, '2019-03-26 10:50:13', 1),
(71, '26', '212', 'hi php', NULL, '2019-03-26 11:37:27', 0),
(72, '24', '211', 'gfdhghjgdhfgdhfgj', NULL, '2019-03-26 11:57:30', 0),
(73, '15', '181', 'Test', 'uploads/message-media/181-20190404165607Author.jpg', '2019-04-04 16:56:07', 1),
(74, '15', '1', 'Automatic message system', '', '2019-04-04 16:56:07', 1),
(75, '20', '197', 'test alpha', NULL, '2019-04-09 18:38:38', 0),
(76, '22', '197', 'New test', NULL, '2019-04-19 07:36:20', 0),
(77, '27', '184', 'Can you tell me more about your product?', NULL, '2019-04-19 07:52:07', 0),
(78, '28', '220', 'hi', NULL, '2019-04-23 09:33:47', 0),
(79, '23', '1', 'Hello Rahul', NULL, '2019-04-24 20:47:52', 0),
(80, '28', '220', '😄', NULL, '2019-04-25 19:07:51', 0),
(81, '28', '220', 'test', NULL, '2019-04-25 15:32:23', 0),
(82, '23', '1', 'Another test', NULL, '2019-04-25 15:36:56', 0),
(83, '29', '1', 'hi', NULL, '2019-04-28 14:18:57', 0),
(84, '29', '1', 'hey', NULL, '2019-04-28 15:05:55', 0),
(85, '29', '1', 'hey', NULL, '2019-04-28 15:06:39', 0),
(86, '29', '1', 'hey', NULL, '2019-04-28 15:11:53', 0),
(87, '29', '1', 'hey', NULL, '2019-04-28 15:12:52', 0),
(88, '29', '1', 'hey', NULL, '2019-04-28 15:13:53', 0),
(89, '29', '1', 'hey', NULL, '2019-04-28 15:15:33', 0),
(90, '29', '1', 'hey', NULL, '2019-04-28 15:16:04', 0),
(91, '29', '1', 'hey', NULL, '2019-04-28 15:20:12', 0),
(92, '29', '1', 'hey', NULL, '2019-04-28 15:21:01', 0),
(93, '29', '1', 'hey', NULL, '2019-04-28 15:21:57', 0),
(94, '29', '1', 'alpha', NULL, '2019-04-28 15:24:09', 0),
(95, '19', '197', 'Hello, Five Seller. How much is your product?', NULL, '2019-04-28 17:17:07', 1),
(96, '19', '1', 'Automatic message system', '', '2019-04-28 21:17:07', 1),
(97, '19', '1', 'Eski, I\'m selling my product for two dollers', NULL, '2019-04-28 17:19:29', 1),
(98, '19', '197', 'Ok, I will purchase now. Thanks!', NULL, '2019-04-28 17:21:22', 1),
(99, '19', '1', 'Sure, no problem', NULL, '2019-04-28 17:23:25', 1),
(100, '19', '1', 'Test', NULL, '2019-04-28 17:32:51', 1),
(101, '19', '197', 'Test', NULL, '2019-05-12 01:40:53', 1);

-- --------------------------------------------------------

--
-- Table structure for table `commercial_ads`
--

CREATE TABLE `commercial_ads` (
  `id` int(11) NOT NULL,
  `seller_plan_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `description` varchar(225) DEFAULT NULL,
  `video_name` varchar(225) DEFAULT NULL,
  `video_path` varchar(225) DEFAULT NULL,
  `product_id` varchar(255) DEFAULT NULL,
  `ad_status` int(1) NOT NULL DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `commercial_ads`
--

INSERT INTO `commercial_ads` (`id`, `seller_plan_id`, `user_id`, `description`, `video_name`, `video_path`, `product_id`, `ad_status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'first', '1-20180904133020Mask-Group-1.png', 'uploads/commercial-ads/1-20180904133020Mask-Group-1.png', '216', 1, 1, 1, '2018-09-04 13:30:20', '2018-09-04 13:30:20'),
(2, 1, 1, 'second', '1-20180904133042icon.png', 'uploads/commercial-ads/1-20180904133042icon.png', '217', 1, 1, 1, '2018-09-04 13:30:42', '2018-09-04 13:30:42'),
(3, 1, 1, 'video', '1-20180904133123step.mov', 'uploads/commercial-ads/1-20180904133123step.mov', '218', 1, 1, 1, '2018-09-04 13:31:23', '2018-09-04 13:31:23'),
(4, 2, 80, 'QWERTY', '80-20180905063720create-ads-social-buzz-image.png', 'uploads/commercial-ads/80-20180905063720create-ads-social-buzz-image.png', '219', 1, 80, 80, '2018-09-05 06:37:20', '2018-09-05 06:37:20'),
(5, 5, 85, 'New Fashion', '85-201809081146311114907_orig.jpg', 'uploads/commercial-ads/85-201809081146311114907_orig.jpg', '227', 1, 85, 85, '2018-09-08 11:46:31', '2018-09-08 11:46:31'),
(6, 5, 85, 'My music', '85-201809081153101114907_orig.jpg', 'uploads/commercial-ads/85-201809081153101114907_orig.jpg', '227', 1, 85, 85, '2018-09-08 11:53:10', '2018-09-08 11:53:10'),
(7, 5, 85, 'Model', '85-20180908120639download-(1).jpeg', 'uploads/commercial-ads/85-20180908120639download-(1).jpeg', '228', 1, 85, 85, '2018-09-08 12:06:39', '2018-09-08 12:06:39'),
(8, 6, 70, 'Ads by seller42', '70-20180910100113entertainment2.jpg', 'uploads/commercial-ads/70-20180910100113entertainment2.jpg', '213', 1, 70, 70, '2018-09-10 10:01:13', '2018-09-10 10:01:13'),
(9, 1, 1, 'Music', '1-20180910120359music.jpg', 'uploads/commercial-ads/1-20180910120359music.jpg', '235', 1, 1, 1, '2018-09-10 12:03:59', '2018-09-10 12:03:59'),
(10, 1, 1, 'Photography', '1-20180910120506photography.jpg', 'uploads/commercial-ads/1-20180910120506photography.jpg', '236', 1, 1, 1, '2018-09-10 12:05:06', '2018-09-10 12:05:06'),
(11, 1, 1, 'Mathematics', '1-20180910122436mathematics3.jpg', 'uploads/commercial-ads/1-20180910122436mathematics3.jpg', '257', 1, 1, 1, '2018-09-10 12:24:36', '2018-09-10 12:24:36'),
(12, 7, 98, 'Created By Seller ads', '98-20180914131130music.jpg', 'uploads/commercial-ads/98-20180914131130music.jpg', '260', 1, 98, 98, '2018-09-14 13:11:30', '2018-09-14 13:11:30'),
(13, 8, 100, 'kkkk', '100-20180914134604ios-reg.PNG', 'uploads/commercial-ads/100-20180914134604ios-reg.PNG', 'null', 1, 100, 100, '2018-09-14 13:46:04', '2018-09-14 13:46:04'),
(14, 8, 100, '123', '100-20180914134650IMG-0400(1).jpg', 'uploads/commercial-ads/100-20180914134650IMG-0400(1).jpg', 'null', 1, 100, 100, '2018-09-14 13:46:50', '2018-09-14 13:46:50'),
(15, 2, 80, 'tatoo ads', '80-20180914135742arrow.png', 'uploads/commercial-ads/80-20180914135742arrow.png', '225', 1, 80, 80, '2018-09-14 13:57:42', '2018-09-14 13:57:42'),
(16, 7, 98, 'Musiic', '98-20180915081343model2.jpg', 'uploads/commercial-ads/98-20180915081343model2.jpg', '260', 1, 98, 98, '2018-09-15 08:13:43', '2018-09-15 08:13:43'),
(17, 7, 98, 'Model', '98-20180915082205model.jpg', 'uploads/commercial-ads/98-20180915082205model.jpg', '261', 0, 98, 98, '2018-09-15 08:22:05', '2018-09-15 08:22:05');

-- --------------------------------------------------------

--
-- Table structure for table `commercial_ad_views`
--

CREATE TABLE `commercial_ad_views` (
  `id` int(11) NOT NULL,
  `seller_plan_id` int(10) NOT NULL,
  `plan_id` int(10) NOT NULL,
  `ad_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `commercial_ad_views`
--

INSERT INTO `commercial_ad_views` (`id`, `seller_plan_id`, `plan_id`, `ad_id`, `user_id`, `created_at`) VALUES
(1, 1, 3, 2, 1, '2018-09-06 06:13:41'),
(2, 1, 2, 2, 1, '2018-09-06 14:30:28'),
(3, 1, 2, 2, 80, '2018-09-07 07:16:13'),
(4, 1, 2, 2, 81, '2018-09-07 07:19:50'),
(5, 2, 2, 4, 81, '2018-09-07 07:29:22'),
(6, 2, 2, 4, 81, '2018-09-07 07:36:33'),
(7, 2, 2, 4, 81, '2018-09-07 07:36:38'),
(8, 2, 2, 4, 81, '2018-09-07 07:36:45'),
(9, 2, 2, 4, 81, '2018-09-07 07:36:50'),
(10, 2, 2, 4, 81, '2018-09-07 07:36:55'),
(11, 5, 1, 5, 85, '2018-09-08 12:03:45'),
(12, 5, 2, 7, 85, '2018-09-08 12:06:51'),
(13, 5, 2, 5, 85, '2018-09-08 12:42:37'),
(14, 1, 1, 3, 1, '2018-09-11 06:03:28'),
(15, 1, 1, 3, 1, '2018-09-14 03:15:53'),
(16, 1, 1, 1, 1, '2018-09-14 03:16:26'),
(17, 2, 2, 15, 80, '2018-09-14 13:58:18'),
(18, 7, 2, 17, 80, '2018-09-15 08:23:06'),
(19, 1, 2, 2, 80, '2018-09-15 08:23:17'),
(20, 7, 2, 17, 80, '2018-09-15 08:23:20'),
(21, 7, 2, 17, 80, '2018-09-15 08:23:29'),
(22, 7, 2, 17, 80, '2018-09-15 08:24:23'),
(23, 1, 2, 2, 80, '2018-09-15 08:25:46'),
(24, 7, 2, 17, 81, '2018-09-15 08:26:58');

-- --------------------------------------------------------

--
-- Table structure for table `commercial_media`
--

CREATE TABLE `commercial_media` (
  `id` int(11) NOT NULL,
  `talent_id` int(11) DEFAULT NULL,
  `image_name` varchar(225) NOT NULL,
  `image_path` varchar(225) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `commercial_media`
--

INSERT INTO `commercial_media` (`id`, `talent_id`, `image_name`, `image_path`, `created_at`, `updated_at`) VALUES
(64, 170, 'images.png', 'uploads/commercial-product/170-images.png', '2018-05-01 08:37:59', '2018-05-01 08:37:59'),
(65, 171, 'home-page.PNG', 'uploads/commercial-product/170-home-page.PNG', '2018-05-01 08:46:12', '2018-05-01 08:46:12'),
(66, 172, 'home-page.PNG', 'uploads/commercial-product/170-home-page.PNG', '2018-05-01 08:58:22', '2018-05-01 08:58:22'),
(67, 173, 'author.jpg', 'uploads/commercial-product/173-author.jpg', '2018-05-01 11:11:12', '2018-05-01 11:11:12'),
(68, 174, 'home-page.PNG', 'uploads/commercial-product/170-home-page.PNG', '2018-05-02 05:44:39', '2018-05-02 05:44:39'),
(69, 175, 'NEw site 2.PNG', 'uploads/commercial-product/175-NEw site 2.PNG', '2018-05-14 20:10:38', '2018-05-14 20:10:38'),
(70, 176, 'food.jpg', 'uploads/commercial-product/193-food.jpg', '2018-05-15 04:23:32', '2018-05-15 04:23:32'),
(72, 178, '8.jpg', 'uploads/commercial-product/178-8.jpg', '2018-07-09 06:01:26', '2018-07-09 06:01:26'),
(74, 180, 'seller.jpg', 'uploads/commercial-product/180-seller.jpg', '2018-08-02 11:22:03', '2018-08-02 11:22:03'),
(75, 182, 'Happy_Birthday.jpg', 'uploads/commercial-product/182-Happy_Birthday.jpg', '2018-08-06 13:39:28', '2018-08-06 13:39:28'),
(76, 184, '2000px-Face-smile.svg.png', 'uploads/commercial-product/184-2000px-Face-smile.svg.png', '2018-08-09 09:23:44', '2018-08-09 09:23:44'),
(80, 185, '2.jpg', 'uploads/commercial-product/185-2.jpg', '2018-08-09 10:43:05', '2018-08-09 10:43:05'),
(81, 186, 'entertainment.jpg', 'uploads/commercial-product/186-entertainment.jpg', '2018-08-10 13:26:46', '2018-08-10 13:26:46'),
(82, 187, 'Music.jpg', 'uploads/commercial-product/187-Music.jpg', '2018-08-10 13:41:51', '2018-08-10 13:41:51'),
(83, 188, 'model.jpg', 'uploads/commercial-product/188-model.jpg', '2018-08-10 14:15:49', '2018-08-10 14:15:49'),
(84, 189, 'Tatoo artist.jpg', 'uploads/commercial-product/189-Tatoo artist.jpg', '2018-08-11 10:14:18', '2018-08-11 10:14:18'),
(85, 190, 'Fashion Design.jpg', 'uploads/commercial-product/190-Fashion Design.jpg', '2018-08-11 10:18:56', '2018-08-11 10:18:56'),
(86, 191, 'mathematics.jpg', 'uploads/commercial-product/191-mathematics.jpg', '2018-08-11 10:37:51', '2018-08-11 10:37:51'),
(87, 192, 'Nutrition.jpg', 'uploads/commercial-product/192-Nutrition.jpg', '2018-08-11 12:31:35', '2018-08-11 12:31:35'),
(88, 193, 'food.jpg', 'uploads/commercial-product/193-food.jpg', '2018-08-11 12:32:23', '2018-08-11 12:32:23'),
(89, 194, 'science.png', 'uploads/commercial-product/194-science.png', '2018-08-11 12:33:08', '2018-08-11 12:33:08'),
(90, 195, 'natural geography.jpg', 'uploads/commercial-product/195-natural geography.jpg', '2018-08-11 12:35:20', '2018-08-11 12:35:20'),
(91, 196, 'cosmatic.jpg', 'uploads/commercial-product/196-cosmatic.jpg', '2018-08-11 12:38:58', '2018-08-11 12:38:58'),
(92, 197, 'fitness.jpg', 'uploads/commercial-product/197-fitness.jpg', '2018-08-11 12:42:04', '2018-08-11 12:42:04'),
(93, 198, 'buyer.jpg', 'uploads/commercial-product/198-buyer.jpg', '2018-08-14 06:20:40', '2018-08-14 06:20:40'),
(96, 199, 'science.png', 'uploads/commercial-product/199-science.png', '2018-08-16 11:08:59', '2018-08-16 11:08:59'),
(97, 200, 'science.png', 'uploads/commercial-product/200-science.png', '2018-08-16 11:18:26', '2018-08-16 11:18:26'),
(98, 201, '2.jpg', 'uploads/commercial-product/201-2.jpg', '2018-08-16 11:28:20', '2018-08-16 11:28:20'),
(99, 202, '1.png', 'uploads/commercial-product/202-1.png', '2018-08-16 11:57:11', '2018-08-16 11:57:11'),
(100, 203, 'Author.jpeg', 'uploads/commercial-product/203-Author.jpeg', '2018-08-16 11:58:52', '2018-08-16 11:58:52'),
(101, 204, '1.png', 'uploads/commercial-product/204-1.png', '2018-08-16 12:42:46', '2018-08-16 12:42:46'),
(102, 205, '1.png', 'uploads/commercial-product/205-1.png', '2018-08-16 13:40:19', '2018-08-16 13:40:19'),
(103, 206, '1.png', 'uploads/commercial-product/206-1.png', '2018-08-16 13:43:21', '2018-08-16 13:43:21'),
(104, 207, 'model.jpg', 'uploads/commercial-product/207-model.jpg', '2018-08-17 09:08:25', '2018-08-17 09:08:25'),
(105, 208, 'Music.jpg', 'uploads/commercial-product/208-Music.jpg', '2018-08-17 11:54:18', '2018-08-17 11:54:18'),
(106, 209, 'seller.jpg', 'uploads/commercial-product/209-seller.jpg', '2018-08-20 09:26:47', '2018-08-20 09:26:47'),
(107, 210, 'F8C4682E-D261-4E45-AA47-B30C552786E3.jpeg', 'uploads/commercial-product/210-F8C4682E-D261-4E45-AA47-B30C552786E3.jpeg', '2018-08-20 11:16:57', '2018-08-20 11:16:57'),
(108, 211, 'model.jpg', 'uploads/commercial-product/211-model.jpg', '2018-08-22 07:29:03', '2018-08-22 07:29:03'),
(109, 212, 'mathematics.jpg', 'uploads/commercial-product/212-mathematics.jpg', '2018-08-22 11:24:32', '2018-08-22 11:24:32'),
(110, 213, 'entertainment4.jpg', 'uploads/commercial-product/213-entertainment4.jpg', '2018-08-22 13:28:36', '2018-08-22 13:28:36'),
(111, 220, 'create ads social buzz image.png', 'uploads/commercial-product/220-create ads social buzz image.png', '2018-08-24 12:17:37', '2018-08-24 12:17:37'),
(112, 221, 'National Geography.jpg', 'uploads/commercial-product/221-National Geography.jpg', '2018-08-27 09:46:03', '2018-08-27 09:46:03'),
(113, 222, 'model2.jpg', 'uploads/commercial-product/222-model2.jpg', '2018-08-27 09:53:27', '2018-08-27 09:53:27'),
(114, 223, 'science.png', 'uploads/commercial-product/223-science.png', '2018-08-27 11:20:45', '2018-08-27 11:20:45'),
(115, 224, 'National Geography.jpg', 'uploads/commercial-product/224-National Geography.jpg', '2018-08-28 09:02:58', '2018-08-28 09:02:58'),
(116, 225, 'fitness2.jpg', 'uploads/commercial-product/225-fitness2.jpg', '2018-08-28 11:17:31', '2018-08-28 11:17:31'),
(117, 227, 'music.jpg', 'uploads/commercial-product/227-music.jpg', '2018-08-29 09:22:21', '2018-08-29 09:22:21'),
(118, 228, 'output_html_e66d0afebbada2b.jpg', 'uploads/commercial-product/228-output_html_e66d0afebbada2b.jpg', '2018-08-29 09:22:40', '2018-08-29 09:22:40'),
(119, 229, 'National Geography.jpg', 'uploads/commercial-product/229-National Geography.jpg', '2018-08-29 09:25:40', '2018-08-29 09:25:40'),
(120, 226, 'National Geography.jpg', 'uploads/commercial-product/226-National Geography.jpg', '2018-08-29 09:26:18', '2018-08-29 09:26:18'),
(121, 230, 'entertainment4.jpg', 'uploads/commercial-product/230-entertainment4.jpg', '2018-08-29 10:12:14', '2018-08-29 10:12:14'),
(122, 231, 'science fiction.jpeg', 'uploads/commercial-product/231-science fiction.jpeg', '2018-08-29 10:15:47', '2018-08-29 10:15:47'),
(123, 232, 'lets.png', 'uploads/commercial-product/232-lets.png', '2018-08-29 10:19:12', '2018-08-29 10:19:12'),
(124, 233, 'fb.png', 'uploads/commercial-product/233-fb.png', '2018-08-29 10:23:36', '2018-08-29 10:23:36'),
(125, 234, 'user-profile.png', 'uploads/commercial-product/234-user-profile.png', '2018-08-29 10:25:36', '2018-08-29 10:25:36'),
(126, 235, 'music.jpg', 'uploads/commercial-product/235-music.jpg', '2018-08-29 10:35:11', '2018-08-29 10:35:11'),
(127, 236, 'photography.jpg', 'uploads/commercial-product/236-photography.jpg', '2018-08-29 10:37:48', '2018-08-29 10:37:48'),
(128, 237, 'fitness.jpg', 'uploads/commercial-product/237-fitness.jpg', '2018-08-29 11:11:18', '2018-08-29 11:11:18'),
(129, 238, 'user-profile.png', 'uploads/commercial-product/238-user-profile.png', '2018-08-29 11:12:42', '2018-08-29 11:12:42'),
(130, 239, 'cosmatics1.png', 'uploads/commercial-product/239-cosmatics1.png', '2018-08-30 13:51:14', '2018-08-30 13:51:14'),
(131, 240, 'fitness.jpg', 'uploads/commercial-product/240-fitness.jpg', '2018-08-31 09:36:38', '2018-08-31 09:36:38'),
(132, 241, 'img1.jpeg', 'uploads/commercial-product/241-img1.jpeg', '2018-08-31 10:25:07', '2018-08-31 10:25:07'),
(133, 242, 'National Geography.jpg', 'uploads/commercial-product/242-National Geography.jpg', '2018-08-31 11:41:09', '2018-08-31 11:41:09'),
(134, 243, 'seller.jpg', 'uploads/commercial-product/243-seller.jpg', '2018-09-03 06:02:00', '2018-09-03 06:02:00'),
(141, 244, '555624c5-70fa-4560-99a7-e47ec7b412d0.jpg', 'uploads/commercial-product/244-555624c5-70fa-4560-99a7-e47ec7b412d0.jpg', '2018-09-03 06:25:32', '2018-09-03 06:25:32'),
(142, 245, 'photo.jpeg', 'uploads/commercial-product/245-photo.jpeg', '2018-09-03 08:51:47', '2018-09-03 08:51:47'),
(143, 246, 'mathematics3.jpg', 'uploads/commercial-product/246-mathematics3.jpg', '2018-09-03 11:17:08', '2018-09-03 11:17:08'),
(144, 247, 'nutrition.jpg', 'uploads/commercial-product/247-nutrition.jpg', '2018-09-03 11:34:19', '2018-09-03 11:34:19'),
(145, 248, 'national geo.png', 'uploads/commercial-product/248-national geo.png', '2018-09-04 09:20:55', '2018-09-04 09:20:55'),
(146, 249, 'seller.jpg', 'uploads/commercial-product/249-seller.jpg', '2018-09-07 11:45:06', '2018-09-07 11:45:06'),
(147, 218, 'entertainment.jpg', 'uploads/commercial-product/218-entertainment.jpg', '2018-09-07 11:57:20', '2018-09-07 11:57:20'),
(148, 219, 'fashion designer.jpg', 'uploads/commercial-product/219-fashion designer.jpg', '2018-09-07 11:59:35', '2018-09-07 11:59:35'),
(149, 216, 'food.jpg', 'uploads/commercial-product/216-food.jpg', '2018-09-07 12:00:47', '2018-09-07 12:00:47'),
(150, 217, 'comedy 1.jpeg', 'uploads/commercial-product/217-comedy 1.jpeg', '2018-09-07 12:04:06', '2018-09-07 12:04:06'),
(151, 250, 'Fs.PNG', 'uploads/commercial-product/250-Fs.PNG', '2018-09-09 03:05:11', '2018-09-09 03:05:11'),
(152, 252, 'SampleVideo_1280x720_5mb.mkv', 'uploads/commercial-product/252-SampleVideo_1280x720_5mb.mkv', '2018-09-10 05:37:45', '2018-09-10 05:37:45'),
(153, 253, 'SampleVideo_1280x720_5mb.mkv', 'uploads/commercial-product/253-SampleVideo_1280x720_5mb.mkv', '2018-09-10 05:42:58', '2018-09-10 05:42:58'),
(154, 256, 'SampleVideo_360x240_30mb.mp4', 'uploads/commercial-product/256-SampleVideo_360x240_30mb.mp4', '2018-09-10 06:01:32', '2018-09-10 06:01:32'),
(155, 257, 'model.jpg', 'uploads/commercial-product/257-model.jpg', '2018-09-10 06:13:57', '2018-09-10 06:13:57'),
(156, 254, 'fitness.jpg', 'uploads/commercial-product/254-fitness.jpg', '2018-09-10 07:13:43', '2018-09-10 07:13:43'),
(157, 258, 'Learn the secrets to making a fortune from your own ( Home Based Business).mp4', 'uploads/commercial-product/258-Learn the secrets to making a fortune from your own ( Home Based Business).mp4', '2018-09-10 13:37:09', '2018-09-10 13:37:09'),
(158, 0, '1114907_orig.jpg', 'uploads/commercial-product/[object Object]-1114907_orig.jpg', '2018-09-11 05:18:52', '2018-09-11 05:18:52'),
(159, 259, 'Mov.mov', 'uploads/commercial-product/259-Mov.mov', '2018-09-11 06:39:12', '2018-09-11 06:39:12'),
(160, 260, 'Mov.mov', 'uploads/commercial-product/260-Mov.mov', '2018-09-11 12:18:19', '2018-09-11 12:18:19'),
(161, 261, 'demo.jpg', 'uploads/commercial-product/261-demo.jpg', '2018-09-12 11:47:36', '2018-09-12 11:47:36'),
(162, 262, 'flv.flv', 'uploads/commercial-product/262-flv.flv', '2018-09-14 06:49:48', '2018-09-14 06:49:48'),
(163, 263, 'Mov.mov', 'uploads/commercial-product/263-Mov.mov', '2018-09-14 08:54:15', '2018-09-14 08:54:15'),
(164, 264, 'Capture.JPG', 'uploads/commercial-product/264-Capture.JPG', '2018-10-11 09:28:24', '2018-10-11 09:28:24'),
(165, 265, 'buyer-banner.jpg', 'uploads/commercial-product/265-buyer-banner.jpg', '2018-10-13 12:24:39', '2018-10-13 12:24:39'),
(166, 266, 'ext.jpeg', 'uploads/commercial-product/266-ext.jpeg', '2018-10-18 13:35:35', '2018-10-18 13:35:35'),
(167, 267, 'stardom.png', 'uploads/commercial-product/267-stardom.png', '2018-10-22 13:00:57', '2018-10-22 13:00:57'),
(168, 268, 'buyer-banner.jpg', 'uploads/commercial-product/268-buyer-banner.jpg', '2018-10-22 13:21:34', '2018-10-22 13:21:34'),
(169, 269, 'Screenshot_2018-10-15-12-28-30-646_com.android.chrome.png', 'uploads/commercial-product/269-Screenshot_2018-10-15-12-28-30-646_com.android.chrome.png', '2018-11-11 10:04:53', '2018-11-11 10:04:53'),
(170, 270, 'Learn the secrets to making a fortune from your own ( Home Based Business).mp4', 'uploads/commercial-product/270-Learn the secrets to making a fortune from your own ( Home Based Business).mp4', '2018-11-12 00:08:55', '2018-11-12 00:08:55'),
(171, 271, 'Fs.PNG', 'uploads/commercial-product/271-Fs.PNG', '2018-11-12 00:12:54', '2018-11-12 00:12:54'),
(172, 272, 'banner.jpg', 'uploads/commercial-product/272-banner.jpg', '2018-11-14 13:38:42', '2018-11-14 13:38:42'),
(173, 273, 'imglogo-1539846524854.png', 'uploads/commercial-product/273-imglogo-1539846524854.png', '2018-11-14 14:25:36', '2018-11-14 14:25:36'),
(174, 274, 'SampleVideo_1280x720_2mb.mp4', 'uploads/commercial-product/274-SampleVideo_1280x720_2mb.mp4', '2019-03-26 06:39:26', '2019-03-26 06:39:26'),
(175, 275, 'pumpkin-1768857__340.jpg', 'uploads/commercial-product/275-pumpkin-1768857__340.jpg', '2019-03-26 07:05:51', '2019-03-26 07:05:51'),
(176, 276, 'bread-2796393__340.jpg', 'uploads/commercial-product/276-bread-2796393__340.jpg', '2019-03-26 07:29:58', '2019-03-26 07:29:58'),
(177, 277, 'orange-1995056__340.jpg', 'uploads/commercial-product/277-orange-1995056__340.jpg', '2019-03-26 08:45:55', '2019-03-26 08:45:55'),
(178, 278, 'images (1).jpg', 'uploads/commercial-product/278-images (1).jpg', '2019-03-26 09:39:16', '2019-03-26 09:39:16'),
(179, 279, 'download (1).png', 'uploads/commercial-product/279-download (1).png', '2019-03-26 09:41:18', '2019-03-26 09:41:18'),
(180, 280, 'pumpkin-1768857__340.jpg', 'uploads/commercial-product/280-pumpkin-1768857__340.jpg', '2019-03-26 10:03:20', '2019-03-26 10:03:20'),
(181, 281, 'images (1).jpg', 'uploads/commercial-product/281-images (1).jpg', '2019-03-26 10:25:59', '2019-03-26 10:25:59'),
(182, 282, 'images (2).jpg', 'uploads/commercial-product/282-images (2).jpg', '2019-03-26 11:26:02', '2019-03-26 11:26:02'),
(183, 284, 'salad-2068220__340.jpg', 'uploads/commercial-product/284-salad-2068220__340.jpg', '2019-03-26 11:32:27', '2019-03-26 11:32:27'),
(184, 285, 'input-onlinejpgtools.jpg', 'uploads/commercial-product/285-input-onlinejpgtools.jpg', '2019-04-09 10:00:33', '2019-04-09 10:00:33'),
(185, 286, 'Wildlife-1.jpg', 'uploads/commercial-product/286-Wildlife-1.jpg', '2019-04-10 18:03:55', '2019-04-10 18:03:55'),
(186, 287, 'Biguine-Make-Up-13-1024x767.jpg', 'uploads/commercial-product/287-Biguine-Make-Up-13-1024x767.jpg', '2019-04-21 12:26:04', '2019-04-21 12:26:04'),
(187, 288, 'download.jpeg', 'uploads/commercial-product/288-download.jpeg', '2019-04-21 19:05:16', '2019-04-21 19:05:16'),
(188, 289, 'CL-MS-02_ml.jpg', 'uploads/commercial-product/289-CL-MS-02_ml.jpg', '2019-04-21 19:43:34', '2019-04-21 19:43:34'),
(189, 290, 'CL-MS-08-V2_ml.jpg', 'uploads/commercial-product/290-CL-MS-08-V2_ml.jpg', '2019-04-21 19:57:11', '2019-04-21 19:57:11'),
(190, 291, 'videoplayback.mp4', 'uploads/commercial-product/291-videoplayback.mp4', '2019-04-23 17:18:11', '2019-04-23 17:18:11');

-- --------------------------------------------------------

--
-- Table structure for table `contactuses`
--

CREATE TABLE `contactuses` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `message` varchar(1020) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contactuses`
--

INSERT INTO `contactuses` (`id`, `name`, `message`, `phone`, `email`, `created_at`, `updated_at`) VALUES
(1, 'Piyush Rathod', 'test', '8871171815', 'pshrathod@gmail.com', '2018-04-03 09:00:05', '2018-04-03 09:00:05'),
(2, 'Piyush', 'testing', '8871171815', 'pshrathod96@gmail.com', '2018-04-03 09:03:13', '2018-04-03 09:03:13'),
(3, 'John Watson', 'testing', '123456789', 'jwatson@gmail.com', '2018-04-03 22:59:36', '2018-04-03 22:59:36'),
(4, 'Zehntech Technologies', 'test', '88888', 'ms0028@zehntech.com', '2018-04-16 07:21:48', '2018-04-16 07:21:48'),
(5, 'chetan chouhan', 'please contact us', '7898816885', 'chetan.chouhan@5exceptions.com', '2018-07-27 07:58:57', '2018-07-27 07:58:57'),
(6, 'testing', 'asdasdad', '4561365', 'test@test.com', '2018-08-11 12:55:54', '2018-08-11 12:55:54'),
(7, 'tyuj', '[', '0298746563211', 'tester.apk42@yahoo.com', '2018-08-11 14:16:17', '2018-08-11 14:16:17'),
(8, 'five', 'verify_userverify_userverify_userverify_userverify_user', '4561365', 'five@fiveexceptions.com', '2018-09-05 06:51:35', '2018-09-05 06:51:35'),
(9, 'praveen', 'test message', '9876543210', 'praveen.patel@5exceptions.com', '2018-09-05 06:53:04', '2018-09-05 06:53:04'),
(10, 'test name', 'testing for contact us check', '9876543210', 'praveen.5exceptions@gmail.com', '2018-09-06 05:50:31', '2018-09-06 05:50:31'),
(11, 'teste', 'test message', '02987654321', 'tester.apk15@gmail.com', '2018-09-08 12:15:30', '2018-09-08 12:15:30'),
(12, 'praveen', 'something', '9874102563', 'praveen.patel@5exceptions.com', '2018-09-08 12:17:51', '2018-09-08 12:17:51'),
(13, 'tester', 'Test data demo data', '02987654321', 'tester.apk15@gmail.com', '2018-09-11 14:06:07', '2018-09-11 14:06:07'),
(14, 'praveen', 'Check mail template', '9876543210', 'praveen.patel@exceptions.com', '2018-09-13 12:54:42', '2018-09-13 12:54:42'),
(15, 'praveen', 'Mail to check template', '9876543210', 'praveen.patel@exceptions.com', '2018-09-13 12:55:38', '2018-09-13 12:55:38'),
(16, 'ds', 'sdfsdf', '9874102563', 'tester.apk3@yahoo.com', '2018-09-13 12:56:30', '2018-09-13 12:56:30'),
(17, 'praveen', 'check mail', '9874102563', 'praveen.5exceptions@gmail.com', '2018-09-13 12:59:04', '2018-09-13 12:59:04'),
(18, 'praveen', 'lets begin', '9874563210', 'praveen.5exceptions@gmail.com', '2018-09-13 13:01:17', '2018-09-13 13:01:17'),
(19, 'five', 'Thank You for showing interest in the Futurestarr. Your query has been successfully submitted. Our representative will contact you soon!', '4561365', 'praveen.5exceptions@gmail.com', '2018-09-13 13:04:05', '2018-09-13 13:04:05'),
(20, 'five', 'Thank You for showing interest in the Futurestarr. Your query has been successfully submitted. Our representative will contact you soon!', '4561365', 'praveen.5exceptions@gmail.com', '2018-09-13 13:04:33', '2018-09-13 13:04:33'),
(21, 'aaa', '21212', '8484', 'a@gmail.com', '2018-10-17 06:59:49', '2018-10-17 06:59:49'),
(22, 'aa', 'hell', '8817034140', 'a@gmail.com', '2018-10-17 09:42:53', '2018-10-17 09:42:53'),
(23, 'a', '123', '12345', 'a@gmail.com', '2018-10-17 09:49:36', '2018-10-17 09:49:36'),
(24, 'a', '1123', '456566665', 'a@g.com', '2018-10-17 09:55:36', '2018-10-17 09:55:36'),
(25, 'a', '5656', '8817034140', 'a@gmail.com', '2018-10-17 09:56:08', '2018-10-17 09:56:08'),
(26, 'a', '121', '1212', 'a@gmail.com', '2018-10-17 09:57:13', '2018-10-17 09:57:13'),
(27, 'a', '122', '1', 'a@gmail.com', '2018-10-17 09:58:34', '2018-10-17 09:58:34'),
(28, 'a', '131321', '121336', 'b@gmail.com', '2018-10-17 10:23:15', '2018-10-17 10:23:15'),
(29, 'AA', '13213', '1212', 'a@gmail.com', '2018-10-17 10:35:17', '2018-10-17 10:35:17'),
(30, 'abhi', '131', '112', 'a@gmail.com', '2018-10-17 10:35:53', '2018-10-17 10:35:53'),
(31, 'abhishek', '2332', '8817031', 'abhishek@g.com', '2018-10-17 11:59:11', '2018-10-17 11:59:11'),
(32, 'a', '323', '23', 'a@gmail.com', '2018-10-17 12:00:20', '2018-10-17 12:00:20'),
(33, 'a', 'a', '999', 'a@gmail.com', '2018-10-18 06:22:07', '2018-10-18 06:22:07'),
(34, 'a', '554', '55', 'a@a.com', '2018-10-18 11:40:55', '2018-10-18 11:40:55'),
(35, 'divy', 'hello test', '881', 'a@ga.com', '2018-10-24 06:45:33', '2018-10-24 06:45:33'),
(36, 'a', '555', '55', 'abhiadr143@gmail.com', '2018-10-24 13:33:39', '2018-10-24 13:33:39'),
(37, 'a', '656', '2', 'abhiadr143@gmail.com', '2018-10-24 13:34:20', '2018-10-24 13:34:20'),
(38, 'rajesh', 'Hii...\n\nTest', '9872467665', 'sharma.rajesh933@gmail.com', '2018-10-25 20:27:48', '2018-10-25 20:27:48'),
(39, 'Monisha', 'I’ve tried three times, the minute I register and I receive the email to confirm my address, I click the link right away to confirm and it says oops links expired. I’ll try again for more times after that and the same thing happens. I open on many different browsers I’ve even tried it on my phone as well as my laptop it’s the same thing that keeps happening. And this is the very same minute that I register and I receive the email and I click that link right away it’s under 10 seconds . And I keep saying ups link expired I don’t know why it’s saying that. Due to this I’m unable to register and start using the platform. I need your help to sort this out immediately as I would like to get on this right away thanks. Monisha', '85292676977', 'mbymonisha@gmail.com', '2018-10-26 08:38:31', '2018-10-26 08:38:31'),
(40, 'abhishek', 'asd', '8817034140', 'a@gmail.com', '2018-10-27 12:15:19', '2018-10-27 12:15:19'),
(41, 'Test', 'test', '4705915037', 'estandley2013@yahoo.com', '2018-11-02 02:32:38', '2018-11-02 02:32:38'),
(42, 'Ashish Sharma', 'ravi test mail', '8890420420', 'ashish244268@gmail.com', '2018-11-02 08:08:18', '2018-11-02 08:08:18'),
(43, 'Ravi', 'Thanks edward.. it test msg from contact us page', '9893031365', 'ravimayuri@gmail.com', '2018-11-02 08:14:45', '2018-11-02 08:14:45'),
(44, 'Testing Roshan', 'asdasdasd asdsadasd', '7697307036', 'roshanparmar6778@gmail.com', '2018-11-02 08:19:08', '2018-11-02 08:19:08'),
(45, 'asdsd as', 'asadasdasdsd', '23333', 'admin@gmail.com', '2018-11-02 08:20:30', '2018-11-02 08:20:30'),
(46, 'abhishek', 'hello world', '8817034140', 'akkira576@gmail.com', '2018-11-02 13:01:35', '2018-11-02 13:01:35'),
(47, 'abhishek', 'hello', '881700', 'abhishekar81@yahoo.com', '2018-11-02 13:02:43', '2018-11-02 13:02:43'),
(48, 'abhishek', 'hello i', '8817034140', 'abhishekar81@yahoo.com', '2018-11-02 13:14:22', '2018-11-02 13:14:22'),
(49, 'abhishek', 'aa', '881700', 'akkira576@gmail.com', '2018-11-02 13:29:34', '2018-11-02 13:29:34'),
(50, 'abhishek', '112', '8170', 'akkira576@gmail.com', '2018-11-02 13:36:34', '2018-11-02 13:36:34'),
(51, 'abhishek', 'aaaa', '881700', 'akkira576@gmail.com', '2018-11-02 13:42:35', '2018-11-02 13:42:35'),
(52, 'aa', '45455', '88170', 'akkira576@gmail.com', '2018-11-02 13:52:50', '2018-11-02 13:52:50'),
(53, 'Testing Roshan', 'Hi Roshan ..testing please check if you receive this email or not.', '7697307036', 'roshanparmar6778@gmail.com', '2018-11-02 14:00:59', '2018-11-02 14:00:59'),
(54, 'Testing Roshan', 'asdasdasdasd', '7697307036', 'admin@gmail.com', '2018-11-02 14:11:12', '2018-11-02 14:11:12'),
(55, 'test', 'test', '4705915037', 'estandley2013@yahoo.com', '2018-11-02 14:12:26', '2018-11-02 14:12:26'),
(56, 'Hi', 'asdasdasd adsadsadsdd sdsd', '7697307036', 'roshanparmar6778@gmail.com', '2018-11-02 14:13:42', '2018-11-02 14:13:42'),
(57, 'Singgellos Jean', 'Hello Dear Sir\n\nI hope you are well........ \n\nI am contacting you regarding Luca Tarqua, a young talent that we are managing.\n\nHe is 20 years old and plays Guitar and sings, he also composes  or co-composes all of his songs.\n\nWith the Link below, you can hear some of his songs, see some Photos and get the feel of who he is.\n\nHe is free to sign worldwide and we are looking for a partner to release his material, either worldwide or just for Asia.\n\nIf you have any questions, please do not hesitate to contact me.\n\n\nhttps://www.dropbox.com/sh/6w72ey558ovw5zq/AADndJKwMC0U63DC8ZAbgYpha?dl=0\n\n\nhttps://www.dropbox.com/sh/kk6bgjy2117cadm/AAAodY-4WhvJyPPtrfxyuCD5a?dl=0\n\nBest regards\nJean Singgellos / Dancing City Switzerland\ninfo@dancing-city.com', '0041448701606', 'info@dancing-city.com', '2018-11-10 05:17:38', '2018-11-10 05:17:38'),
(58, 'Farukh', 'Hi,\nAre you guys like or similar to MOBSTAR?\n\nWe are planning to hold a peace pageant and would like to ask our contestants to upload onto a \'talent search\' platform and then their fans can vote for them thru the internet for a number of categories.\n\nWe are just bouncing off this idea on the creativity wall and are looking for a platform to serve our NGO objectives.\n\nAwaiting your response.\n\nMr. Farukh', '60196667988', 'farukh.express@gmail.com', '2019-01-09 13:14:15', '2019-01-09 13:14:15'),
(59, 'parag', 'test', '111', 'parag.k@tridhya.com', '2019-01-16 11:31:30', '2019-01-16 11:31:30'),
(60, 'Travis Cole', 'I came across your website after searching for agencies on yelp.com.\n\nAnd I was wondering if you would like to partner up? \n\nBasically what we do is make animated videos that are designed to promote your service online and increase your website conversion rate.\n\nSo I wanted to offer you a 30 second animated explainer video for your service for just $197. (including script/voiceover)\n\nAll I ask in return is a quick testimonial if you like the video!\n\nIf you are interested in this offer, you can find out more and get started at www.30secondexplainervideos.com/explainer-promo \n\nOr can you shoot me a quick email for a brief discussion!\n\nCheers, \nTravis\nwww.30secondexplainervideos.com/explainer-promo', '3478095956', 'studio@30secondexplainervideos.com', '2019-01-17 01:54:25', '2019-01-17 01:54:25'),
(61, 'Test Name', 'This is the test message.', '123456789', 'test@test.com', '2019-02-12 05:28:17', '2019-02-12 05:28:17'),
(62, 'Protien Kp Fitness', 'this is test', '87987999', 'kirtan.p@tridhya.com', '2019-02-12 09:50:14', '2019-02-12 09:50:14'),
(63, 'd', 'dd', '9722101534', 'admin@admin.com', '2019-02-15 06:17:54', '2019-02-15 06:17:54'),
(64, 'pravi', 'something', '9897896545', 'pravi@gmail.com', '2019-02-27 14:01:12', '2019-02-27 14:01:12'),
(65, 'Ed Standley', 'Just checking to say hello', '4705915037', 'jrayray2@gmail.com', '2019-03-18 06:47:43', '2019-03-18 06:47:43'),
(66, 'QA T', 'nice project', '03336398967', 'qablackbox@gmail.com', '2019-03-18 13:49:50', '2019-03-18 13:49:50'),
(67, 'QA T', 'nice project', '03336398967', 'qablackbox@gmail.com', '2019-03-18 13:50:14', '2019-03-18 13:50:14'),
(68, 'QA T', 'nice project', '03336398967', 'qablackbox@gmail.com', '2019-03-18 13:50:39', '2019-03-18 13:50:39'),
(69, 'asd', 'asd', '1111', 'arpit_00016@yahoo.co.in', '2019-03-22 07:04:14', '2019-03-22 07:04:14'),
(70, 'test', 'test', '000000', 'test@yopmail.com', '2019-03-22 08:03:30', '2019-03-22 08:03:30'),
(71, 'utuuy', 'bnmbnmbnm', '456546456', 'ytyu@fghg.ghjhg', '2019-03-24 19:09:22', '2019-03-24 19:09:22'),
(72, 'anjali', 'testing', '8872228074', 'anjali.tanwar@walkwel.in', '2019-03-26 05:36:31', '2019-03-26 05:36:31'),
(73, 'Vinit Verma', 'Hello there!\n\nHope you are having a great day!\n\nI\'m the founder and the CEO of CreateMyContent.Co.Uk. \n\nWe have developed an absolutely FREE platform for digital marketers. The site has resources from marketing thought leaders. And the content gets updated every few hours.\n\nConsidering your focus into Digital, I feel that you might find these resources helpful:\n\n1. Free course on Content Marketing by Hubspot\n\nhttps://www.createmycontent.co.uk/2019/04/free-certification-course-on-content.html\n\n2. Latest (FREE) videos on Content Marketing\n\nhttps://www.createmycontent.co.uk/2019/04/latest-videos-on-content-marketing.html\n\n3. Latest (FREE) posts on Content Marketing\n\nhttps://www.createmycontent.co.uk/2019/03/content-marketing.html\n\nIf you find these relevant and helpful, could you please share my message with your team members?\n\nThank you, once again, for reading my message.\n\n\nKind Regards,\nVinit Verma\nFounder, Create My Content\nwww.createmycontent.co.uk', '7178897717', 'vinit.verma@createmycontent.co.uk', '2019-05-01 14:05:57', '2019-05-01 14:05:57');

-- --------------------------------------------------------

--
-- Table structure for table `custom_plans`
--

CREATE TABLE `custom_plans` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `custom_plan` varchar(2042) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `custom_plans`
--

INSERT INTO `custom_plans` (`id`, `user_id`, `custom_plan`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 1, 'fgdfg', '2018-05-02 10:37:07', '2018-05-02 10:37:07', 1, 1),
(2, 1, 'dfgdf', '2018-05-02 10:38:02', '2018-05-02 10:38:02', 1, 1),
(3, 1, 'hello', '2018-05-02 11:06:42', '2018-05-02 11:06:42', 1, 1),
(4, 1, 'hi', '2018-05-02 11:13:20', '2018-05-02 11:13:20', 1, 1),
(5, 1, 'hi', '2018-05-02 11:14:10', '2018-05-02 11:14:10', 1, 1),
(6, 1, 'gggg', '2018-05-02 11:15:44', '2018-05-02 11:15:44', 1, 1),
(7, 1, 'eeee', '2018-05-02 11:16:41', '2018-05-02 11:16:41', 1, 1),
(8, 70, 'Test message', '2018-08-22 13:18:14', '2018-08-22 13:18:14', 70, 70),
(9, 80, 'qwe', '2018-08-29 10:01:54', '2018-08-29 10:01:54', 80, 80),
(10, 91, 'Done TDone T', '2018-09-05 06:59:42', '2018-09-05 06:59:42', 91, 91),
(11, 69, 'praveen71praveen71praveen71praveen71praveen71praveen71', '2018-09-05 07:02:11', '2018-09-05 07:02:11', 69, 69),
(12, 69, 'testing for custom commercial plan', '2018-09-06 05:49:48', '2018-09-06 05:49:48', 69, 69),
(13, 69, 'lets begin button check in mail', '2018-09-06 05:56:54', '2018-09-06 05:56:54', 69, 69),
(14, 69, 'beginbeginbeginbeginbeginbegin', '2018-09-06 05:58:46', '2018-09-06 05:58:46', 69, 69),
(15, 69, 'begin', '2018-09-06 05:58:51', '2018-09-06 05:58:51', 69, 69),
(16, 69, 'sendCustomPlanEmailtoAdmin', '2018-09-06 05:59:34', '2018-09-06 05:59:34', 69, 69),
(17, 69, 'ssssss', '2018-09-06 06:00:05', '2018-09-06 06:00:05', 69, 69),
(18, 69, 'sendCustomPlanEmailtoAdmin sendCustomPlanEmailtoAdmin', '2018-09-06 06:02:01', '2018-09-06 06:02:01', 69, 69),
(19, 69, 'http://futurestarr.com/future_star/', '2018-09-06 06:04:36', '2018-09-06 06:04:36', 69, 69),
(20, 80, 'custom package', '2018-09-10 09:18:59', '2018-09-10 09:18:59', 80, 80),
(21, 70, 'custom ads', '2018-09-10 09:54:02', '2018-09-10 09:54:02', 70, 70);

-- --------------------------------------------------------

--
-- Table structure for table `favriote_user`
--

CREATE TABLE `favriote_user` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `fav_user_id` int(11) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `favriote_user`
--

INSERT INTO `favriote_user` (`id`, `user_id`, `fav_user_id`, `date`) VALUES
(27, 1, 63, '2018-08-10 07:02:07'),
(37, 1, 1, '2018-08-10 11:15:34'),
(39, 1, 45, '2018-08-10 11:59:24'),
(53, 81, 77, '2018-08-20 14:04:07'),
(54, 70, 77, '2018-08-29 06:11:43'),
(55, 70, 83, '2018-08-29 06:11:51'),
(57, 70, 82, '2018-08-29 06:11:58'),
(71, 45, 83, '2018-08-29 07:06:32'),
(72, 45, 82, '2018-08-29 07:06:36'),
(74, 45, 80, '2018-08-29 07:06:44'),
(75, 45, 76, '2018-08-29 07:06:54'),
(76, 45, 68, '2018-08-29 07:07:00'),
(77, 70, 85, '2018-08-29 13:23:31'),
(79, 70, 84, '2018-08-29 13:23:43'),
(80, 70, 68, '2018-08-29 13:23:49'),
(82, 92, 91, '2018-09-03 12:58:47'),
(87, 92, 88, '2018-09-03 13:17:15'),
(89, 91, 92, '2018-09-04 06:57:57'),
(90, 1, 0, '2018-09-04 15:36:57'),
(91, 1, 89, '2018-09-06 14:45:50'),
(92, 80, 81, '2018-09-08 07:35:24'),
(98, 45, 81, '2018-10-13 13:09:07'),
(104, 113, 112, '2018-10-15 10:15:23'),
(105, 45, 104, '2018-10-16 05:39:06'),
(106, 172, 0, '2018-11-11 07:23:44'),
(108, 168, 125, '2018-11-11 10:12:25'),
(109, 168, 172, '2019-01-09 06:30:21'),
(113, 183, 192, '2019-03-15 06:36:59'),
(114, 211, 0, '2019-03-26 05:50:17'),
(115, 212, 208, '2019-03-26 06:51:47'),
(116, 212, 211, '2019-03-26 06:52:05'),
(117, 211, 195, '2019-03-26 06:56:43'),
(118, 211, 198, '2019-03-26 06:56:47'),
(119, 211, 200, '2019-03-26 06:56:50'),
(120, 211, 199, '2019-03-26 06:56:59'),
(121, 211, 196, '2019-03-26 06:57:09'),
(122, 211, 191, '2019-03-26 06:57:14'),
(123, 211, 190, '2019-03-26 06:57:19'),
(124, 211, 212, '2019-03-26 10:49:07'),
(125, 212, 191, '2019-03-26 11:44:41');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `sent_by` int(11) NOT NULL,
  `received_by` int(11) NOT NULL,
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  `read_flag` int(11) NOT NULL DEFAULT '0',
  `active_flag` int(11) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(16, '2014_10_12_100000_create_password_resets_table', 1),
(17, '2017_11_07_051750_create_roles_table', 1),
(18, '2017_11_07_051804_create_users_table', 1),
(31, '2017_11_07_051849_create_talent_categories_table', 2),
(32, '2017_11_07_051937_create_talents_table', 2),
(33, '2017_11_07_052015_create_talent_multimedia_table', 2),
(34, '2017_11_07_052149_create_social_accounts_table', 2),
(35, '2017_11_07_052233_create_seller_availabilities_table', 2),
(36, '2017_11_07_052305_create_seller_certifications_table', 2),
(37, '2017_11_07_052332_create_seller_educations_table', 2),
(38, '2017_11_07_052348_create_seller_languages_table', 2),
(39, '2017_11_07_052402_create_seller_skills_table', 2),
(40, '2017_11_07_052424_create_shipping_addresses_table', 2),
(41, '2017_11_07_052517_create_payment_histories_table', 2),
(42, '2017_11_07_052549_create_talent_orders_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_histories`
--

CREATE TABLE `payment_histories` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `transaction_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `payment_histories`
--

INSERT INTO `payment_histories` (`id`, `user_id`, `transaction_id`, `amount`, `status`, `created_at`, `updated_at`) VALUES
(2, 197, 'txn_1EjNJbGoy9vhCpJJ6tWbIMvt', '550', '', '2019-06-08 23:57:55', '2019-06-08 23:57:55'),
(3, 197, 'txn_1EjNVZGoy9vhCpJJZsP6EoWu', '1', '', '2019-06-09 00:10:16', '2019-06-09 00:10:16'),
(4, 197, 'txn_1EjTXLGoy9vhCpJJaHQfRs9U', '23', '', '2019-06-09 06:36:30', '2019-06-09 06:36:30'),
(5, 197, 'txn_1EkERWGoy9vhCpJJJzQJacG5', '10', '', '2019-06-11 12:41:36', '2019-06-11 12:41:36');

-- --------------------------------------------------------

--
-- Table structure for table `payment_history`
--

CREATE TABLE `payment_history` (
  `id` int(11) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `transaction_id` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `PayerID` varchar(255) DEFAULT NULL,
  `stripe_acc` varchar(250) DEFAULT NULL,
  `talent_id` varchar(255) DEFAULT NULL,
  `plan_id` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `description` longtext,
  `transaction_payload` longtext,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payment_history`
--

INSERT INTO `payment_history` (`id`, `user_id`, `transaction_id`, `token`, `PayerID`, `stripe_acc`, `talent_id`, `plan_id`, `amount`, `description`, `transaction_payload`, `created_at`) VALUES
(1, '79', '08V72862P36461240', 'EC-8HM54195PM1682608', 'MM2WMKR2AGSRL', NULL, NULL, '1', '20', 'Plan Purchase', NULL, '2018-09-07 09:53:51'),
(2, '79', '9NS49386YU199835E', 'EC-8TC17871883639435', 'MM2WMKR2AGSRL', NULL, NULL, '2', '20', 'Plan Upgrade', NULL, '2018-09-07 09:54:47'),
(3, '79', '7X002041UK784462E', 'EC-5A6181745A072400B', 'MM2WMKR2AGSRL', NULL, NULL, '3', '135', 'Plan Upgrade', '{"TOKEN":"EC-5A6181745A072400B","SUCCESSPAGEREDIRECTREQUESTED":"false","TIMESTAMP":"2018-09-07T10:05:30Z","CORRELATIONID":"b9ba7f2613ec","ACK":"Success","VERSION":"113","BUILD":"48666477","INSURANCEOPTIONSELECTED":"false","SHIPPINGOPTIONISDEFAULT":"false","PAYMENTINFO_0_TRANSACTIONID":"7X002041UK784462E","PAYMENTINFO_0_TRANSACTIONTYPE":"expresscheckout","PAYMENTINFO_0_PAYMENTTYPE":"instant","PAYMENTINFO_0_ORDERTIME":"2018-09-07T10:05:29Z","PAYMENTINFO_0_AMT":"135.00","PAYMENTINFO_0_FEEAMT":"6.57","PAYMENTINFO_0_TAXAMT":"0.00","PAYMENTINFO_0_CURRENCYCODE":"USD","PAYMENTINFO_0_PAYMENTSTATUS":"Pending","PAYMENTINFO_0_PENDINGREASON":"paymentreview","PAYMENTINFO_0_REASONCODE":"None","PAYMENTINFO_0_PROTECTIONELIGIBILITY":"Ineligible","PAYMENTINFO_0_PROTECTIONELIGIBILITYTYPE":"None","PAYMENTINFO_0_SELLERPAYPALACCOUNTID":"tester.apk15-facilitator@gmail.com","PAYMENTINFO_0_SECUREMERCHANTACCOUNTID":"TAMBPFC8RCBXL","PAYMENTINFO_0_ERRORCODE":"0","PAYMENTINFO_0_ACK":"Success"}', '2018-09-07 10:05:30'),
(4, '45', 'AP-8CD8001900917111K', NULL, NULL, NULL, '180', NULL, '45', 'Talent Purchase', '[true]', '2018-09-07 11:38:11'),
(5, '45', 'AP-6T970199TG995771E', NULL, NULL, NULL, '180,174', NULL, '85', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-07T04:40:57.223-07:00","ack":"Success","correlationId":"9735dcf452ed5","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"transactionId":"45236615SP0326050","transactionStatus":"COMPLETED","receiver":{"amount":"31.50","email":"Kelly@paypal.com","primary":"false","paymentType":"SERVICE","accountId":"Z6NWQ93EYZ5EL"},"refundedAmount":"0.00","pendingRefund":"false","senderTransactionId":"3UX02574B8182113S","senderTransactionStatus":"COMPLETED"},{"receiver":{"amount":"28.00","email":"meghana.singhal@zehntech.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"87742853CH8275744","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"25.50","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"9D1758617N1568516","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":180,"users_id":45},{"talents_id":174,"users_id":45}]&amount=85","senderEmail":"tester.apk17@gmail.com","status":"COMPLETED","payKey":"AP-6T970199TG995771E","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk17@gmail.com","accountId":"MM2WMKR2AGSRL","useCredentials":"false"}}', '2018-09-07 11:40:57'),
(6, '85', '2E752677XR450494P', 'EC-2ME67854B3701182T', 'MM2WMKR2AGSRL', NULL, NULL, '1', '20', 'Plan Purchase', '{"TOKEN":"EC-2ME67854B3701182T","SUCCESSPAGEREDIRECTREQUESTED":"false","TIMESTAMP":"2018-09-08T11:44:38Z","CORRELATIONID":"f3edb2fb9db94","ACK":"Success","VERSION":"113","BUILD":"48666477","INSURANCEOPTIONSELECTED":"false","SHIPPINGOPTIONISDEFAULT":"false","PAYMENTINFO_0_TRANSACTIONID":"2E752677XR450494P","PAYMENTINFO_0_TRANSACTIONTYPE":"expresscheckout","PAYMENTINFO_0_PAYMENTTYPE":"instant","PAYMENTINFO_0_ORDERTIME":"2018-09-08T11:44:37Z","PAYMENTINFO_0_AMT":"20.00","PAYMENTINFO_0_FEEAMT":"1.27","PAYMENTINFO_0_TAXAMT":"0.00","PAYMENTINFO_0_CURRENCYCODE":"USD","PAYMENTINFO_0_PAYMENTSTATUS":"Pending","PAYMENTINFO_0_PENDINGREASON":"paymentreview","PAYMENTINFO_0_REASONCODE":"None","PAYMENTINFO_0_PROTECTIONELIGIBILITY":"Ineligible","PAYMENTINFO_0_PROTECTIONELIGIBILITYTYPE":"None","PAYMENTINFO_0_SELLERPAYPALACCOUNTID":"tester.apk15-facilitator@gmail.com","PAYMENTINFO_0_SECUREMERCHANTACCOUNTID":"TAMBPFC8RCBXL","PAYMENTINFO_0_ERRORCODE":"0","PAYMENTINFO_0_ACK":"Success"}', '2018-09-08 11:44:38'),
(7, '85', '73C28540BP075772W', 'EC-1WM45004WA942154M', 'MM2WMKR2AGSRL', NULL, NULL, '2', '20', 'Plan Upgrade', '{"TOKEN":"EC-1WM45004WA942154M","SUCCESSPAGEREDIRECTREQUESTED":"false","TIMESTAMP":"2018-09-08T12:06:21Z","CORRELATIONID":"3b4b1e6689c02","ACK":"Success","VERSION":"113","BUILD":"48666477","INSURANCEOPTIONSELECTED":"false","SHIPPINGOPTIONISDEFAULT":"false","PAYMENTINFO_0_TRANSACTIONID":"73C28540BP075772W","PAYMENTINFO_0_TRANSACTIONTYPE":"expresscheckout","PAYMENTINFO_0_PAYMENTTYPE":"instant","PAYMENTINFO_0_ORDERTIME":"2018-09-08T12:06:20Z","PAYMENTINFO_0_AMT":"20.00","PAYMENTINFO_0_FEEAMT":"1.27","PAYMENTINFO_0_TAXAMT":"0.00","PAYMENTINFO_0_CURRENCYCODE":"USD","PAYMENTINFO_0_PAYMENTSTATUS":"Pending","PAYMENTINFO_0_PENDINGREASON":"paymentreview","PAYMENTINFO_0_REASONCODE":"None","PAYMENTINFO_0_PROTECTIONELIGIBILITY":"Ineligible","PAYMENTINFO_0_PROTECTIONELIGIBILITYTYPE":"None","PAYMENTINFO_0_SELLERPAYPALACCOUNTID":"tester.apk15-facilitator@gmail.com","PAYMENTINFO_0_SECUREMERCHANTACCOUNTID":"TAMBPFC8RCBXL","PAYMENTINFO_0_ERRORCODE":"0","PAYMENTINFO_0_ACK":"Success"}', '2018-09-08 12:06:21'),
(8, '96', 'AP-27W68464YJ754832R', NULL, NULL, NULL, '212', NULL, '125', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-10T02:27:45.382-07:00","ack":"Success","correlationId":"1a9291962fea","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"87.50","email":"Tester.apk50@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"3X140338PL8594805","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"37.50","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"49H535601M3839158","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":212,"users_id":96}]&amount=125","senderEmail":"tester.apk17@gmail.com","status":"COMPLETED","payKey":"AP-27W68464YJ754832R","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk17@gmail.com","accountId":"MM2WMKR2AGSRL","useCredentials":"false"}}', '2018-09-10 09:27:45'),
(9, '81', 'AP-7TD8648471656614M', NULL, NULL, NULL, '256', NULL, '111', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-10T02:30:25.907-07:00","ack":"Success","correlationId":"d88eb7dedeba","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"33.30","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"5DT532227L304443R","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":256,"users_id":81}]&amount=111","senderEmail":"tester.apk17@gmail.com","status":"COMPLETED","payKey":"AP-7TD8648471656614M","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk17@gmail.com","accountId":"MM2WMKR2AGSRL","useCredentials":"false"}}', '2018-09-10 09:30:26'),
(10, '96', 'AP-3ND13589AH6819945', NULL, NULL, NULL, '213', NULL, '1000', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-10T02:42:31.189-07:00","ack":"Success","correlationId":"bdc1c28549c2","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"700.00","email":"tester.apk42@yahoo.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"2KY5104894328384K","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"300.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"45W54815FJ801744V","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":213,"users_id":96}]&amount=1000","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-3ND13589AH6819945","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-10 09:42:31'),
(11, '70', '1TC62414JY3787833', 'EC-7Y307780UD024273N', 'MM2WMKR2AGSRL', NULL, NULL, '1', '20', 'Plan Purchase', '{"TOKEN":"EC-7Y307780UD024273N","SUCCESSPAGEREDIRECTREQUESTED":"false","TIMESTAMP":"2018-09-10T09:59:17Z","CORRELATIONID":"75e29f4d4471a","ACK":"Success","VERSION":"113","BUILD":"48666477","INSURANCEOPTIONSELECTED":"false","SHIPPINGOPTIONISDEFAULT":"false","PAYMENTINFO_0_TRANSACTIONID":"1TC62414JY3787833","PAYMENTINFO_0_TRANSACTIONTYPE":"expresscheckout","PAYMENTINFO_0_PAYMENTTYPE":"instant","PAYMENTINFO_0_ORDERTIME":"2018-09-10T09:59:16Z","PAYMENTINFO_0_AMT":"20.00","PAYMENTINFO_0_FEEAMT":"1.27","PAYMENTINFO_0_TAXAMT":"0.00","PAYMENTINFO_0_CURRENCYCODE":"USD","PAYMENTINFO_0_PAYMENTSTATUS":"Pending","PAYMENTINFO_0_PENDINGREASON":"paymentreview","PAYMENTINFO_0_REASONCODE":"None","PAYMENTINFO_0_PROTECTIONELIGIBILITY":"Ineligible","PAYMENTINFO_0_PROTECTIONELIGIBILITYTYPE":"None","PAYMENTINFO_0_SELLERPAYPALACCOUNTID":"tester.apk15-facilitator@gmail.com","PAYMENTINFO_0_SECUREMERCHANTACCOUNTID":"TAMBPFC8RCBXL","PAYMENTINFO_0_ERRORCODE":"0","PAYMENTINFO_0_ACK":"Success"}', '2018-09-10 09:59:17'),
(12, '45', 'AP-60461095U5633350L', NULL, NULL, NULL, '254', NULL, '1212', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-10T03:13:31.670-07:00","ack":"Success","correlationId":"fc5f48e511aa9","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"transactionId":"8JN65663JS550430A","transactionStatus":"COMPLETED","receiver":{"amount":"848.40","email":"Kelly@paypal.com","primary":"false","paymentType":"SERVICE","accountId":"Z6NWQ93EYZ5EL"},"refundedAmount":"0.00","pendingRefund":"false","senderTransactionId":"3CE101190A040181D","senderTransactionStatus":"COMPLETED"},{"receiver":{"amount":"363.60","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"8YT58607V1217392X","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":254,"users_id":45}]&amount=1212","senderEmail":"tester.apk17@gmail.com","status":"COMPLETED","payKey":"AP-60461095U5633350L","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk17@gmail.com","accountId":"MM2WMKR2AGSRL","useCredentials":"false"}}', '2018-09-10 10:13:31'),
(13, '1', '4CB62682UK438135A', 'EC-87004065FV796740U', 'MM2WMKR2AGSRL', NULL, NULL, '3', '135', 'Plan Upgrade', '{"TOKEN":"EC-87004065FV796740U","SUCCESSPAGEREDIRECTREQUESTED":"false","TIMESTAMP":"2018-09-10T12:23:28Z","CORRELATIONID":"746bca8abef99","ACK":"Success","VERSION":"113","BUILD":"48666477","INSURANCEOPTIONSELECTED":"false","SHIPPINGOPTIONISDEFAULT":"false","PAYMENTINFO_0_TRANSACTIONID":"4CB62682UK438135A","PAYMENTINFO_0_TRANSACTIONTYPE":"expresscheckout","PAYMENTINFO_0_PAYMENTTYPE":"instant","PAYMENTINFO_0_ORDERTIME":"2018-09-10T12:23:27Z","PAYMENTINFO_0_AMT":"135.00","PAYMENTINFO_0_FEEAMT":"6.57","PAYMENTINFO_0_TAXAMT":"0.00","PAYMENTINFO_0_CURRENCYCODE":"USD","PAYMENTINFO_0_PAYMENTSTATUS":"Pending","PAYMENTINFO_0_PENDINGREASON":"paymentreview","PAYMENTINFO_0_REASONCODE":"None","PAYMENTINFO_0_PROTECTIONELIGIBILITY":"Ineligible","PAYMENTINFO_0_PROTECTIONELIGIBILITYTYPE":"None","PAYMENTINFO_0_SELLERPAYPALACCOUNTID":"tester.apk15-facilitator@gmail.com","PAYMENTINFO_0_SECUREMERCHANTACCOUNTID":"TAMBPFC8RCBXL","PAYMENTINFO_0_ERRORCODE":"0","PAYMENTINFO_0_ACK":"Success"}', '2018-09-10 12:23:28'),
(14, '98', '3DN866820N254472H', 'EC-9TS33022AW569200G', 'MM2WMKR2AGSRL', NULL, NULL, '1', '20', 'Plan Purchase', '{"TOKEN":"EC-9TS33022AW569200G","SUCCESSPAGEREDIRECTREQUESTED":"false","TIMESTAMP":"2018-09-11T05:37:07Z","CORRELATIONID":"6cc577a7d5125","ACK":"Success","VERSION":"113","BUILD":"48666477","INSURANCEOPTIONSELECTED":"false","SHIPPINGOPTIONISDEFAULT":"false","PAYMENTINFO_0_TRANSACTIONID":"3DN866820N254472H","PAYMENTINFO_0_TRANSACTIONTYPE":"expresscheckout","PAYMENTINFO_0_PAYMENTTYPE":"instant","PAYMENTINFO_0_ORDERTIME":"2018-09-11T05:37:06Z","PAYMENTINFO_0_AMT":"20.00","PAYMENTINFO_0_FEEAMT":"1.27","PAYMENTINFO_0_TAXAMT":"0.00","PAYMENTINFO_0_CURRENCYCODE":"USD","PAYMENTINFO_0_PAYMENTSTATUS":"Pending","PAYMENTINFO_0_PENDINGREASON":"paymentreview","PAYMENTINFO_0_REASONCODE":"None","PAYMENTINFO_0_PROTECTIONELIGIBILITY":"Ineligible","PAYMENTINFO_0_PROTECTIONELIGIBILITYTYPE":"None","PAYMENTINFO_0_SELLERPAYPALACCOUNTID":"tester.apk15-facilitator@gmail.com","PAYMENTINFO_0_SECUREMERCHANTACCOUNTID":"TAMBPFC8RCBXL","PAYMENTINFO_0_ERRORCODE":"0","PAYMENTINFO_0_ACK":"Success"}', '2018-09-11 05:37:07'),
(15, '99', 'AP-1HS486584X213392K', NULL, NULL, NULL, '243', NULL, '5000', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-11T04:20:46.087-07:00","ack":"Success","correlationId":"b11a8f7acef7d","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"3500.00","email":"tester.apk15@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"0B578085DK5976127","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"1500.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"3AK14505KJ148481K","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":243,"users_id":99}]&amount=5000","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-1HS486584X213392K","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-11 11:20:46'),
(16, '99', 'AP-6G139967S1176164P', NULL, NULL, NULL, '260', NULL, '1200', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-11T05:41:50.247-07:00","ack":"Success","correlationId":"f159bda6ba762","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"840.00","email":"sellerads@mailinator.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"9LY67862SP247381B","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"360.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"1JY4369214637362C","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":260,"users_id":99}]&amount=1200","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-6G139967S1176164P","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-11 12:41:50'),
(17, '81', 'AP-2A6963934W7093616', NULL, NULL, NULL, '260', NULL, '1200', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-11T06:28:45.427-07:00","ack":"Success","correlationId":"399589fa14d1a","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"840.00","email":"sellerads@mailinator.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"93B43246NB192531Y","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"360.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"4SR987108R425134N","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":260,"users_id":81}]&amount=1200","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-2A6963934W7093616","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-11 13:28:45'),
(18, '99', 'AP-90648915UF1627123', NULL, NULL, NULL, '260', NULL, '1200', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-13T23:35:25.766-07:00","ack":"Success","correlationId":"fb6144078ad3","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"840.00","email":"sellerads@mailinator.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"5GY30264Y3082621T","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"360.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"0TD66185HX169993M","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":260,"users_id":99}]&amount=1200","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-90648915UF1627123","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-14 06:35:25'),
(19, '99', 'AP-90648915UF1627123', NULL, NULL, NULL, '260', NULL, '1200', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-13T23:35:27.281-07:00","ack":"Success","correlationId":"adf64cbb23381","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"840.00","email":"sellerads@mailinator.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"5GY30264Y3082621T","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"360.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"0TD66185HX169993M","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":260,"users_id":99}]&amount=1200","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-90648915UF1627123","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-14 06:35:27'),
(20, '99', 'AP-44C16244BM169845L', NULL, NULL, NULL, '259', NULL, '10000', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T02:39:42.834-07:00","ack":"Success","correlationId":"1c8ec627af918","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"7000.00","email":"sellerads@mailinator.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"099933647C015080L","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"3000.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"4NS706299C765703G","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":259,"users_id":99}]&amount=10000","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-44C16244BM169845L","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-14 09:39:42'),
(21, '99', 'AP-44C16244BM169845L', NULL, NULL, NULL, '259', NULL, '10000', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T02:39:44.722-07:00","ack":"Success","correlationId":"c95375559d152","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"7000.00","email":"sellerads@mailinator.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"099933647C015080L","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"3000.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"4NS706299C765703G","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":259,"users_id":99}]&amount=10000","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-44C16244BM169845L","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-14 09:39:44'),
(22, '81', 'AP-2JW931995L1669904', NULL, NULL, NULL, '218', NULL, '1230', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T02:43:30.013-07:00","ack":"Success","correlationId":"1b3524691946a","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"861.00","email":"Tester.apk50@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"3MF16503NU7389142","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"369.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"8R1707165G232902D","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":218,"users_id":81}]&amount=1230","senderEmail":"tester.apk17@gmail.com","status":"COMPLETED","payKey":"AP-2JW931995L1669904","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk17@gmail.com","accountId":"MM2WMKR2AGSRL","useCredentials":"false"}}', '2018-09-14 09:43:30'),
(23, '81', 'AP-2A400078CE713215B', NULL, NULL, NULL, '250', NULL, '1000', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T02:45:48.609-07:00","ack":"Success","correlationId":"8b855c8517b08","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"transactionId":"8V8389587M430642T","transactionStatus":"COMPLETED","receiver":{"amount":"700.00","email":"Kelly@paypal.com","primary":"false","paymentType":"SERVICE","accountId":"Z6NWQ93EYZ5EL"},"refundedAmount":"0.00","pendingRefund":"false","senderTransactionId":"2DW603994S2996111","senderTransactionStatus":"COMPLETED"},{"receiver":{"amount":"300.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"2DR54874N9815084G","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":250,"users_id":81}]&amount=1000","senderEmail":"tester.apk17@gmail.com","status":"COMPLETED","payKey":"AP-2A400078CE713215B","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk17@gmail.com","accountId":"MM2WMKR2AGSRL","useCredentials":"false"}}', '2018-09-14 09:45:48'),
(24, '99', 'AP-12B09882EE992770H', NULL, NULL, NULL, '250', NULL, '1000', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T02:48:18.116-07:00","ack":"Success","correlationId":"e26437b084518","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"transactionId":"1NB602587T266691W","transactionStatus":"COMPLETED","receiver":{"amount":"700.00","email":"Kelly@paypal.com","primary":"false","paymentType":"SERVICE","accountId":"Z6NWQ93EYZ5EL"},"refundedAmount":"0.00","pendingRefund":"false","senderTransactionId":"5HF52989K6879924X","senderTransactionStatus":"COMPLETED"},{"receiver":{"amount":"300.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"9X426571E72817004","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":250,"users_id":99}]&amount=1000","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-12B09882EE992770H","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-14 09:48:18'),
(25, '99', 'AP-47423076G0675620L', NULL, NULL, NULL, '262', NULL, '90000', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T02:49:54.762-07:00","ack":"Success","correlationId":"d32622fcade52","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"63000.00","email":"sellerads@mailinator.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"9EA05607U0100134G","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"27000.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"6A23786800626123C","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":262,"users_id":99}]&amount=90000","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-47423076G0675620L","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-14 09:49:54'),
(26, '99', 'AP-47423076G0675620L', NULL, NULL, NULL, '262', NULL, '90000', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T02:49:56.695-07:00","ack":"Success","correlationId":"300675c2623f3","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"63000.00","email":"sellerads@mailinator.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"9EA05607U0100134G","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"27000.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"6A23786800626123C","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":262,"users_id":99}]&amount=90000","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-47423076G0675620L","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-14 09:49:56'),
(27, '99', 'AP-1AM85598BR4110907', NULL, NULL, NULL, '261', NULL, '1100', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T02:52:39.892-07:00","ack":"Success","correlationId":"ca9cf2a984514","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"770.00","email":"sellerads@mailinator.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"3LU88533BM5451219","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"330.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"4H119392V44895622","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":261,"users_id":99}]&amount=1100","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-1AM85598BR4110907","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-14 09:52:39'),
(28, '81', 'AP-0M11320057559040P', NULL, NULL, NULL, '262', NULL, '90000', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T02:58:28.169-07:00","ack":"Success","correlationId":"5e95e5b3e96f0","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"63000.00","email":"sellerads@mailinator.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"99U31650PJ615135P","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"27000.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"7KL92670RW300173L","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":262,"users_id":81}]&amount=90000","senderEmail":"tester.apk17@gmail.com","status":"COMPLETED","payKey":"AP-0M11320057559040P","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk17@gmail.com","accountId":"MM2WMKR2AGSRL","useCredentials":"false"}}', '2018-09-14 09:58:28'),
(29, '99', 'AP-8PV70207G2879433A', NULL, NULL, NULL, '261', NULL, '1100', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T02:59:39.901-07:00","ack":"Success","correlationId":"18a2c73cc0acb","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"770.00","email":"sellerads@mailinator.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"5FU49521AB579323L","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"330.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"6WL14467HP7145742","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":261,"users_id":99}]&amount=1100","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-8PV70207G2879433A","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-14 09:59:39'),
(30, '99', 'AP-8PV70207G2879433A', NULL, NULL, NULL, '261', NULL, '1100', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T02:59:41.861-07:00","ack":"Success","correlationId":"f91ae0f4d4573","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"770.00","email":"sellerads@mailinator.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"5FU49521AB579323L","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"330.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"6WL14467HP7145742","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":261,"users_id":99}]&amount=1100","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-8PV70207G2879433A","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-14 09:59:41'),
(31, '81', 'AP-6XP35092MM022224R', NULL, NULL, NULL, '217', NULL, '1230', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T02:59:50.117-07:00","ack":"Success","correlationId":"92af8cb14e93","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"861.00","email":"Tester.apk50@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"6FN14261FC611125N","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"369.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"1E754039YV9879059","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":217,"users_id":81}]&amount=1230","senderEmail":"tester.apk17@gmail.com","status":"COMPLETED","payKey":"AP-6XP35092MM022224R","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk17@gmail.com","accountId":"MM2WMKR2AGSRL","useCredentials":"false"}}', '2018-09-14 09:59:50'),
(32, '81', 'AP-8SR12280Y0333041V', NULL, NULL, NULL, '261', NULL, '1100', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T03:03:20.969-07:00","ack":"Success","correlationId":"7e79228de52b","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"770.00","email":"sellerads@mailinator.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"2K0269005U057705Y","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"330.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"7RJ92698AS695332G","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":261,"users_id":81}]&amount=1100","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-8SR12280Y0333041V","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-14 10:03:21'),
(33, '99', 'AP-5L231745XY149400H', NULL, NULL, NULL, '235', NULL, '23', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T03:06:08.249-07:00","ack":"Success","correlationId":"9ec23db685ff","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"transactionId":"19475088Y21208103","transactionStatus":"COMPLETED","receiver":{"amount":"16.10","email":"Kelly@paypal.com","primary":"false","paymentType":"SERVICE","accountId":"Z6NWQ93EYZ5EL"},"refundedAmount":"0.00","pendingRefund":"false","senderTransactionId":"2XF42396EK887703P","senderTransactionStatus":"COMPLETED"},{"receiver":{"amount":"6.90","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"9L619492297363340","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":235,"users_id":99}]&amount=23","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-5L231745XY149400H","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-14 10:06:08'),
(34, '99', 'AP-5L231745XY149400H', NULL, NULL, NULL, '235', NULL, '23', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T03:06:10.723-07:00","ack":"Success","correlationId":"25f646e793e76","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"transactionId":"19475088Y21208103","transactionStatus":"COMPLETED","receiver":{"amount":"16.10","email":"Kelly@paypal.com","primary":"false","paymentType":"SERVICE","accountId":"Z6NWQ93EYZ5EL"},"refundedAmount":"0.00","pendingRefund":"false","senderTransactionId":"2XF42396EK887703P","senderTransactionStatus":"COMPLETED"},{"receiver":{"amount":"6.90","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"9L619492297363340","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":235,"users_id":99}]&amount=23","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-5L231745XY149400H","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-14 10:06:10'),
(35, '99', 'AP-59G80740NR207144B', NULL, NULL, NULL, '210', NULL, '1000', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T03:07:23.440-07:00","ack":"Success","correlationId":"41078d73d028","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"700.00","email":"Tester.apk50@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"3L627453T2102730U","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"300.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"4FM16214MP4544016","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":210,"users_id":99}]&amount=1000","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-59G80740NR207144B","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-14 10:07:23'),
(36, '99', 'AP-39K5177080551182B', NULL, NULL, NULL, '240', NULL, '20000', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T03:10:17.504-07:00","ack":"Success","correlationId":"45b36c912d13e","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"14000.00","email":"tester.apk15@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"0NB761585Y8793120","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"6000.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"5KX11821RD5097207","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":240,"users_id":99}]&amount=20000","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-39K5177080551182B","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-14 10:10:17'),
(37, '99', 'AP-39K5177080551182B', NULL, NULL, NULL, '240', NULL, '20000', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T03:10:20.758-07:00","ack":"Success","correlationId":"6edaf3b533da8","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"14000.00","email":"tester.apk15@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"0NB761585Y8793120","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"6000.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"5KX11821RD5097207","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":240,"users_id":99}]&amount=20000","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-39K5177080551182B","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-14 10:10:20'),
(38, '99', 'AP-15F80413LA711384X', NULL, NULL, NULL, '219', NULL, '1220', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T03:14:45.432-07:00","ack":"Success","correlationId":"3f207b5144752","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"854.00","email":"Tester.apk50@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"04C13730U30062402","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"366.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"0BK05626KK304914J","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":219,"users_id":99}]&amount=1220","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-15F80413LA711384X","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-14 10:14:45'),
(39, '99', 'AP-15F80413LA711384X', NULL, NULL, NULL, '219', NULL, '1220', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T03:14:47.552-07:00","ack":"Success","correlationId":"aceb27315f2f2","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"854.00","email":"Tester.apk50@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"04C13730U30062402","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"366.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"0BK05626KK304914J","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":219,"users_id":99}]&amount=1220","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-15F80413LA711384X","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-14 10:14:47'),
(40, '92', 'AP-3HA79036ND457302D', NULL, NULL, NULL, '261', NULL, '1100', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T03:16:26.950-07:00","ack":"Success","correlationId":"e3a62d13cf811","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"770.00","email":"sellerads@mailinator.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"5RA84129PS489342A","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"330.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"5SE24897T42516117","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":261,"users_id":92}]&amount=1100","senderEmail":"tester.apk17@gmail.com","status":"COMPLETED","payKey":"AP-3HA79036ND457302D","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk17@gmail.com","accountId":"MM2WMKR2AGSRL","useCredentials":"false"}}', '2018-09-14 10:16:27'),
(41, '92', 'AP-3HA79036ND457302D', NULL, NULL, NULL, '261', NULL, '1100', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T03:16:29.231-07:00","ack":"Success","correlationId":"2b90dd43a5fd2","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"770.00","email":"sellerads@mailinator.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"5RA84129PS489342A","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"330.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"5SE24897T42516117","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":261,"users_id":92}]&amount=1100","senderEmail":"tester.apk17@gmail.com","status":"COMPLETED","payKey":"AP-3HA79036ND457302D","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk17@gmail.com","accountId":"MM2WMKR2AGSRL","useCredentials":"false"}}', '2018-09-14 10:16:29');
INSERT INTO `payment_history` (`id`, `user_id`, `transaction_id`, `token`, `PayerID`, `stripe_acc`, `talent_id`, `plan_id`, `amount`, `description`, `transaction_payload`, `created_at`) VALUES
(42, '92', 'AP-5LG396084M327382B', NULL, NULL, NULL, '239', NULL, '1000', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T03:18:36.839-07:00","ack":"Success","correlationId":"e47c83f09aa9c","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"700.00","email":"Tester.apk50@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"33F91398MN617184R","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"300.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"3S259218YS552550R","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":239,"users_id":92}]&amount=1000","senderEmail":"tester.apk17@gmail.com","status":"COMPLETED","payKey":"AP-5LG396084M327382B","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk17@gmail.com","accountId":"MM2WMKR2AGSRL","useCredentials":"false"}}', '2018-09-14 10:18:36'),
(43, '99', 'AP-5RJ82929EC8989033', NULL, NULL, NULL, '236', NULL, '44', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T03:18:54.482-07:00","ack":"Success","correlationId":"e7d46e872b5fa","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"transactionId":"8EV22850VG7611618","transactionStatus":"COMPLETED","receiver":{"amount":"30.80","email":"Kelly@paypal.com","primary":"false","paymentType":"SERVICE","accountId":"Z6NWQ93EYZ5EL"},"refundedAmount":"0.00","pendingRefund":"false","senderTransactionId":"3P6391586D291040F","senderTransactionStatus":"COMPLETED"},{"receiver":{"amount":"13.20","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"5M977098SD1783019","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":236,"users_id":99}]&amount=44","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-5RJ82929EC8989033","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-14 10:18:54'),
(44, '92', 'AP-9JS982098B2749627', NULL, NULL, NULL, '236', NULL, '44', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T03:20:34.817-07:00","ack":"Success","correlationId":"1d13992cc9da5","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"transactionId":"10C79897LK060940C","transactionStatus":"COMPLETED","receiver":{"amount":"30.80","email":"Kelly@paypal.com","primary":"false","paymentType":"SERVICE","accountId":"Z6NWQ93EYZ5EL"},"refundedAmount":"0.00","pendingRefund":"false","senderTransactionId":"6LJ046806W331862F","senderTransactionStatus":"COMPLETED"},{"receiver":{"amount":"13.20","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"13A63029T31172719","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":236,"users_id":92}]&amount=44","senderEmail":"tester.apk17@gmail.com","status":"COMPLETED","payKey":"AP-9JS982098B2749627","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk17@gmail.com","accountId":"MM2WMKR2AGSRL","useCredentials":"false"}}', '2018-09-14 10:20:34'),
(45, '92', 'AP-9JS982098B2749627', NULL, NULL, NULL, '236', NULL, '44', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T03:20:37.108-07:00","ack":"Success","correlationId":"290973e9294e8","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"transactionId":"10C79897LK060940C","transactionStatus":"COMPLETED","receiver":{"amount":"30.80","email":"Kelly@paypal.com","primary":"false","paymentType":"SERVICE","accountId":"Z6NWQ93EYZ5EL"},"refundedAmount":"0.00","pendingRefund":"false","senderTransactionId":"6LJ046806W331862F","senderTransactionStatus":"COMPLETED"},{"receiver":{"amount":"13.20","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"13A63029T31172719","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":236,"users_id":92}]&amount=44","senderEmail":"tester.apk17@gmail.com","status":"COMPLETED","payKey":"AP-9JS982098B2749627","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk17@gmail.com","accountId":"MM2WMKR2AGSRL","useCredentials":"false"}}', '2018-09-14 10:20:37'),
(46, '92', 'AP-70B571743U2407744', NULL, NULL, NULL, '216', NULL, '1200', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T04:16:08.349-07:00","ack":"Success","correlationId":"4b0adfc23a2b1","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"840.00","email":"Tester.apk50@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"3L207454AU0333647","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"360.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"0AS839747C155192X","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":216,"users_id":92}]&amount=1200","senderEmail":"tester.apk17@gmail.com","status":"COMPLETED","payKey":"AP-70B571743U2407744","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk17@gmail.com","accountId":"MM2WMKR2AGSRL","useCredentials":"false"}}', '2018-09-14 11:16:08'),
(47, '92', 'AP-70B571743U2407744', NULL, NULL, NULL, '216', NULL, '1200', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T04:16:10.129-07:00","ack":"Success","correlationId":"1057b06b014c","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"840.00","email":"Tester.apk50@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"3L207454AU0333647","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"360.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"0AS839747C155192X","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":216,"users_id":92}]&amount=1200","senderEmail":"tester.apk17@gmail.com","status":"COMPLETED","payKey":"AP-70B571743U2407744","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk17@gmail.com","accountId":"MM2WMKR2AGSRL","useCredentials":"false"}}', '2018-09-14 11:16:10'),
(48, '99', 'AP-87730238KA239235Y', NULL, NULL, NULL, '260', NULL, '1200', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T04:19:06.606-07:00","ack":"Success","correlationId":"e7de271d4d560","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"840.00","email":"sellerads@mailinator.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"6DT19649W02446938","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"360.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"2AV83088C28238320","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":260,"users_id":99}]&amount=1200","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-87730238KA239235Y","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-14 11:19:06'),
(49, '99', 'AP-87730238KA239235Y', NULL, NULL, NULL, '260', NULL, '1200', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T04:19:08.697-07:00","ack":"Success","correlationId":"a9890c13ea6b8","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"840.00","email":"sellerads@mailinator.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"6DT19649W02446938","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"360.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"2AV83088C28238320","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":260,"users_id":99}]&amount=1200","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-87730238KA239235Y","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-14 11:19:08'),
(50, '99', 'AP-9M916178KV6492722', NULL, NULL, NULL, '218,216', NULL, '2430', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T04:21:03.763-07:00","ack":"Success","correlationId":"b7aa9f002d27b","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"1701.00","email":"Tester.apk50@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"9FW47208T84427452","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"729.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"6MW93406RB527220P","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":218,"users_id":99},{"talents_id":216,"users_id":99}]&amount=2430","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-9M916178KV6492722","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-14 11:21:03'),
(51, '99', 'AP-78455963492472015', NULL, NULL, NULL, '211', NULL, '1000', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T04:23:09.506-07:00","ack":"Success","correlationId":"78729120f21ee","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"700.00","email":"Tester.apk50@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"98E09481VF6323059","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"300.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"1TD31299T8009911M","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":211,"users_id":99}]&amount=1000","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-78455963492472015","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-14 11:23:09'),
(52, '99', 'AP-78455963492472015', NULL, NULL, NULL, '211', NULL, '1000', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T04:23:11.847-07:00","ack":"Success","correlationId":"c0ba49fb7450b","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"700.00","email":"Tester.apk50@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"98E09481VF6323059","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"300.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"1TD31299T8009911M","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":211,"users_id":99}]&amount=1000","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-78455963492472015","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-14 11:23:11'),
(53, '92', 'AP-5VU12898E0087235H', NULL, NULL, NULL, '212', NULL, '125', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T04:29:02.490-07:00","ack":"Success","correlationId":"f60fb7b73a0f1","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"87.50","email":"Tester.apk50@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"5R236716KB064111R","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"37.50","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"7M671800PD341112M","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":212,"users_id":92}]&amount=125","senderEmail":"tester.apk17@gmail.com","status":"COMPLETED","payKey":"AP-5VU12898E0087235H","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk17@gmail.com","accountId":"MM2WMKR2AGSRL","useCredentials":"false"}}', '2018-09-14 11:29:02'),
(54, '81', 'AP-5WR24144DK075671A', NULL, NULL, NULL, '260', NULL, '1200', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T04:51:15.305-07:00","ack":"Success","correlationId":"341730556059","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"840.00","email":"sellerads@mailinator.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"5NP11425P4779662J","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"360.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"2B9930728V8567714","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":260,"users_id":81}]&amount=1200","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-5WR24144DK075671A","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-14 11:51:15'),
(55, '81', 'AP-8T7464243K7582919', NULL, NULL, NULL, '260', NULL, '1200', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T04:56:21.974-07:00","ack":"Success","correlationId":"c1e6344bd8196","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"840.00","email":"sellerads@mailinator.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"3PH14522J5066140L","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"360.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"52D450640D963303N","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":260,"users_id":81}]&amount=1200","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-8T7464243K7582919","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-14 11:56:22'),
(56, '81', 'AP-8T7464243K7582919', NULL, NULL, NULL, '260', NULL, '1200', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T04:56:23.728-07:00","ack":"Success","correlationId":"570b8b006b907","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"840.00","email":"sellerads@mailinator.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"3PH14522J5066140L","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"360.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"52D450640D963303N","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":260,"users_id":81}]&amount=1200","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-8T7464243K7582919","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-14 11:56:23'),
(57, '45', 'AP-3T871889TT492961B', NULL, NULL, NULL, '234,203,262,213,204,259,218,173,261,217,250,216,212,246,257', NULL, '12439710', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T05:23:19.158-07:00","ack":"Success","correlationId":"9efe88ee8d985","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"transactionId":"00405687214991339","transactionStatus":"COMPLETED","receiver":{"amount":"1050.70","email":"Kelly@paypal.com","primary":"false","paymentType":"SERVICE","accountId":"Z6NWQ93EYZ5EL"},"refundedAmount":"0.00","pendingRefund":"false","senderTransactionId":"2RS71598PW416963W","senderTransactionStatus":"COMPLETED"},{"receiver":{"amount":"70770.00","email":"sellerads@mailinator.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"5TW99239KB299153A","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"700.00","email":"tester.apk42@yahoo.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"7KU47940P4317653A","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"2649.50","email":"Tester.apk50@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"94P731200H930782V","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"700.00","email":"mondayseller@mailinator.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"65285728ML977770H","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"3731913.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"7K1481779M778234A","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":234,"users_id":45},{"talents_id":203,"users_id":45},{"talents_id":262,"users_id":45},{"talents_id":213,"users_id":45},{"talents_id":204,"users_id":45},{"talents_id":259,"users_id":45},{"talents_id":218,"users_id":45},{"talents_id":173,"users_id":45},{"talents_id":261,"users_id":45},{"talents_id":217,"users_id":45},{"talents_id":250,"users_id":45},{"talents_id":216,"users_id":45},{"talents_id":212,"users_id":45},{"talents_id":246,"users_id":45},{"talents_id":257,"users_id":45}]&amount=12439710","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-3T871889TT492961B","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-14 12:23:19'),
(58, '81', 'AP-0FK48828LY797545A', NULL, NULL, NULL, '260', NULL, '1200', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T05:39:59.724-07:00","ack":"Success","correlationId":"cb5ddf6979d96","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"840.00","email":"sellerads@mailinator.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"22188156035576608","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"360.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"2KB60312CB7941027","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":260,"users_id":81}]&amount=1200","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-0FK48828LY797545A","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-14 12:39:59'),
(59, '81', 'AP-0FK48828LY797545A', NULL, NULL, NULL, '260', NULL, '1200', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T05:40:01.355-07:00","ack":"Success","correlationId":"c55c33e63e119","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"840.00","email":"sellerads@mailinator.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"22188156035576608","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"360.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"2KB60312CB7941027","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":260,"users_id":81}]&amount=1200","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-0FK48828LY797545A","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-14 12:40:01'),
(60, '81', 'AP-9RP79443EN4576100', NULL, NULL, NULL, '260,213', NULL, '2200', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T05:43:18.028-07:00","ack":"Success","correlationId":"2352644bb931b","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"840.00","email":"sellerads@mailinator.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"94S26704WV676210C","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"700.00","email":"tester.apk42@yahoo.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"81W77300GA3217351","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"660.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"68858003M7348221S","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":260,"users_id":81},{"talents_id":213,"users_id":81}]&amount=2200","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-9RP79443EN4576100","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-14 12:43:18'),
(61, '81', 'AP-9RP79443EN4576100', NULL, NULL, NULL, '260,213', NULL, '2200', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T05:43:19.725-07:00","ack":"Success","correlationId":"875565c75442e","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"840.00","email":"sellerads@mailinator.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"94S26704WV676210C","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"700.00","email":"tester.apk42@yahoo.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"81W77300GA3217351","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"660.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"68858003M7348221S","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":260,"users_id":81},{"talents_id":213,"users_id":81}]&amount=2200","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-9RP79443EN4576100","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-14 12:43:19'),
(62, '1', '0B519168WA722310C', 'EC-0NU317892X022020B', 'MM2WMKR2AGSRL', NULL, NULL, '2', '16', 'Plan Upgrade', '{"TOKEN":"EC-0NU317892X022020B","SUCCESSPAGEREDIRECTREQUESTED":"false","TIMESTAMP":"2018-09-14T13:21:06Z","CORRELATIONID":"633e61b0930","ACK":"Success","VERSION":"113","BUILD":"49181869","INSURANCEOPTIONSELECTED":"false","SHIPPINGOPTIONISDEFAULT":"false","PAYMENTINFO_0_TRANSACTIONID":"0B519168WA722310C","PAYMENTINFO_0_TRANSACTIONTYPE":"expresscheckout","PAYMENTINFO_0_PAYMENTTYPE":"instant","PAYMENTINFO_0_ORDERTIME":"2018-09-14T13:21:05Z","PAYMENTINFO_0_AMT":"16.00","PAYMENTINFO_0_FEEAMT":"1.09","PAYMENTINFO_0_TAXAMT":"0.00","PAYMENTINFO_0_CURRENCYCODE":"USD","PAYMENTINFO_0_PAYMENTSTATUS":"Pending","PAYMENTINFO_0_PENDINGREASON":"paymentreview","PAYMENTINFO_0_REASONCODE":"None","PAYMENTINFO_0_PROTECTIONELIGIBILITY":"Ineligible","PAYMENTINFO_0_PROTECTIONELIGIBILITYTYPE":"None","PAYMENTINFO_0_SELLERPAYPALACCOUNTID":"tester.apk15-facilitator@gmail.com","PAYMENTINFO_0_SECUREMERCHANTACCOUNTID":"TAMBPFC8RCBXL","PAYMENTINFO_0_ERRORCODE":"0","PAYMENTINFO_0_ACK":"Success"}', '2018-09-14 13:21:06'),
(63, '100', '9WX74618W20137422', 'EC-7N943040B5660044V', 'MM2WMKR2AGSRL', NULL, NULL, '1', '20', 'Plan Purchase', '{"TOKEN":"EC-7N943040B5660044V","SUCCESSPAGEREDIRECTREQUESTED":"false","TIMESTAMP":"2018-09-14T13:44:54Z","CORRELATIONID":"5a81e16a28474","ACK":"Success","VERSION":"113","BUILD":"49181869","INSURANCEOPTIONSELECTED":"false","SHIPPINGOPTIONISDEFAULT":"false","PAYMENTINFO_0_TRANSACTIONID":"9WX74618W20137422","PAYMENTINFO_0_TRANSACTIONTYPE":"expresscheckout","PAYMENTINFO_0_PAYMENTTYPE":"instant","PAYMENTINFO_0_ORDERTIME":"2018-09-14T13:44:53Z","PAYMENTINFO_0_AMT":"20.00","PAYMENTINFO_0_FEEAMT":"1.27","PAYMENTINFO_0_TAXAMT":"0.00","PAYMENTINFO_0_CURRENCYCODE":"USD","PAYMENTINFO_0_PAYMENTSTATUS":"Pending","PAYMENTINFO_0_PENDINGREASON":"paymentreview","PAYMENTINFO_0_REASONCODE":"None","PAYMENTINFO_0_PROTECTIONELIGIBILITY":"Ineligible","PAYMENTINFO_0_PROTECTIONELIGIBILITYTYPE":"None","PAYMENTINFO_0_SELLERPAYPALACCOUNTID":"tester.apk15-facilitator@gmail.com","PAYMENTINFO_0_SECUREMERCHANTACCOUNTID":"TAMBPFC8RCBXL","PAYMENTINFO_0_ERRORCODE":"0","PAYMENTINFO_0_ACK":"Success"}', '2018-09-14 13:44:54'),
(64, '92', NULL, 'AP-9VH19238YU174223U', NULL, NULL, '204', NULL, '12321324', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-14T23:40:43.193-07:00","ack":"Success","correlationId":"53e5e6845d1b9","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"3696397.20","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"9YM209855L569245W","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":204,"users_id":92}]&amount=12321324","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-9VH19238YU174223U","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-15 06:40:43'),
(65, '98', '74X972647D4802508', 'EC-7AX47919LF8335717', 'MM2WMKR2AGSRL', NULL, NULL, '2', '17', 'Plan Upgrade', '{"TOKEN":"EC-7AX47919LF8335717","SUCCESSPAGEREDIRECTREQUESTED":"false","TIMESTAMP":"2018-09-15T08:21:06Z","CORRELATIONID":"c27699f9d67de","ACK":"Success","VERSION":"113","BUILD":"49181869","INSURANCEOPTIONSELECTED":"false","SHIPPINGOPTIONISDEFAULT":"false","PAYMENTINFO_0_TRANSACTIONID":"74X972647D4802508","PAYMENTINFO_0_TRANSACTIONTYPE":"expresscheckout","PAYMENTINFO_0_PAYMENTTYPE":"instant","PAYMENTINFO_0_ORDERTIME":"2018-09-15T08:21:05Z","PAYMENTINFO_0_AMT":"17.00","PAYMENTINFO_0_FEEAMT":"1.13","PAYMENTINFO_0_TAXAMT":"0.00","PAYMENTINFO_0_CURRENCYCODE":"USD","PAYMENTINFO_0_PAYMENTSTATUS":"Pending","PAYMENTINFO_0_PENDINGREASON":"paymentreview","PAYMENTINFO_0_REASONCODE":"None","PAYMENTINFO_0_PROTECTIONELIGIBILITY":"Ineligible","PAYMENTINFO_0_PROTECTIONELIGIBILITYTYPE":"None","PAYMENTINFO_0_SELLERPAYPALACCOUNTID":"tester.apk15-facilitator@gmail.com","PAYMENTINFO_0_SECUREMERCHANTACCOUNTID":"TAMBPFC8RCBXL","PAYMENTINFO_0_ERRORCODE":"0","PAYMENTINFO_0_ACK":"Success"}', '2018-09-15 08:21:06'),
(66, '81', NULL, 'AP-6M136756LT3229931', NULL, NULL, '260,261', NULL, '2300', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-15T01:29:02.321-07:00","ack":"Success","correlationId":"c7b7d0a647a93","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"1610.00","email":"sellerads@mailinator.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"4YS64045DY302542T","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"690.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"5W990916G9948533N","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":260,"users_id":81},{"talents_id":261,"users_id":81}]&amount=2300","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-6M136756LT3229931","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-15 08:29:02'),
(67, '81', NULL, 'AP-1GT36138VM450005E', NULL, NULL, '262', NULL, '90000', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-15T01:30:14.688-07:00","ack":"Success","correlationId":"955a0bf6baaa6","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"63000.00","email":"sellerads@mailinator.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"00J76767E56923603","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"27000.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"7S990579FV677501Y","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":262,"users_id":81}]&amount=90000","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-1GT36138VM450005E","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-15 08:30:14'),
(68, '81', NULL, 'AP-1GT36138VM450005E', NULL, NULL, '262', NULL, '90000', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-15T01:30:16.098-07:00","ack":"Success","correlationId":"591c88f2dca0","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"63000.00","email":"sellerads@mailinator.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"00J76767E56923603","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"27000.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"7S990579FV677501Y","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":262,"users_id":81}]&amount=90000","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-1GT36138VM450005E","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-15 08:30:16'),
(69, '81', NULL, 'AP-0N8823880N5651922', NULL, NULL, '262', NULL, '90000', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-15T01:39:57.524-07:00","ack":"Success","correlationId":"187acd508885b","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"63000.00","email":"sellerads@mailinator.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"3TX120351P317661N","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"27000.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"9LJ41405K6743544Y","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":262,"users_id":81}]&amount=90000","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-0N8823880N5651922","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-15 08:39:57'),
(70, '81', NULL, 'AP-0N8823880N5651922', NULL, NULL, '262', NULL, '90000', 'Talent Purchase', '{"responseEnvelope":{"timestamp":"2018-09-15T01:39:59.360-07:00","ack":"Success","correlationId":"dba5a36e6aa6d","build":"48631621"},"cancelUrl":"http://futurestarr.com/future_star/","currencyCode":"USD","memo":"Commercial Ad Package 3","paymentInfoList":{"paymentInfo":[{"receiver":{"amount":"63000.00","email":"sellerads@mailinator.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"3TX120351P317661N","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"},{"receiver":{"amount":"27000.00","email":"admin_price@gmail.com","primary":"false","paymentType":"SERVICE","accountId":"0"},"pendingRefund":"false","senderTransactionId":"9LJ41405K6743544Y","senderTransactionStatus":"PENDING","pendingReason":"UNILATERAL"}]},"returnUrl":"http://futurestarr.com/paypal?data_for=[{"talents_id":262,"users_id":81}]&amount=90000","senderEmail":"tester.apk15-buyer@gmail.com","status":"COMPLETED","payKey":"AP-0N8823880N5651922","actionType":"PAY","feesPayer":"EACHRECEIVER","reverseAllParallelPaymentsOnError":"false","sender":{"email":"tester.apk15-buyer@gmail.com","accountId":"K46NZTHFRDLDY","useCredentials":"false"}}', '2018-09-15 08:39:59'),
(71, '227', '7X3457369S270561R', 'EC-8FN20807RB307542Y', '97SXYNCZD9HW2', NULL, '272', NULL, '10', 'Talent Purchase', '{"TOKEN":"EC-8FN20807RB307542Y","BILLINGAGREEMENTACCEPTEDSTATUS":"0","CHECKOUTSTATUS":"PaymentActionCompleted","TIMESTAMP":"2019-04-12T17:35:26Z","CORRELATIONID":"46ae90cfd0d80","ACK":"Success","VERSION":"113","BUILD":"52475881","EMAIL":"rahul.botad-buyer@gmail.com","PAYERID":"97SXYNCZD9HW2","PAYERSTATUS":"verified","FIRSTNAME":"test","LASTNAME":"buyer","COUNTRYCODE":"IN","SHIPTONAME":"test buyer","SHIPTOSTREET":"Flat no. 507 Wing A Raheja Residency","SHIPTOSTREET2":"Film City Road, Goregaon East","SHIPTOCITY":"Mumbai","SHIPTOSTATE":"Maharashtra","SHIPTOZIP":"400097","SHIPTOCOUNTRYCODE":"IN","SHIPTOCOUNTRYNAME":"India","ADDRESSSTATUS":"Confirmed","CURRENCYCODE":"USD","AMT":"7.00","SHIPPINGAMT":"0.00","HANDLINGAMT":"0.00","TAXAMT":"0.00","INSURANCEAMT":"0.00","SHIPDISCAMT":"0.00","TRANSACTIONID":"7X3457369S270561R","INSURANCEOPTIONOFFERED":"false","PAYMENTREQUEST_0_CURRENCYCODE":"USD","PAYMENTREQUEST_0_AMT":"7.00","PAYMENTREQUEST_0_SHIPPINGAMT":"0.00","PAYMENTREQUEST_0_HANDLINGAMT":"0.00","PAYMENTREQUEST_0_TAXAMT":"0.00","PAYMENTREQUEST_0_INSURANCEAMT":"0.00","PAYMENTREQUEST_0_SHIPDISCAMT":"0.00","PAYMENTREQUEST_0_TRANSACTIONID":"7X3457369S270561R","PAYMENTREQUEST_0_SELLERPAYPALACCOUNTID":"qysen@wmail.club","PAYMENTREQUEST_0_INSURANCEOPTIONOFFERED":"false","PAYMENTREQUEST_0_PAYMENTREQUESTID":"talent_sale_168","PAYMENTREQUEST_0_SOFTDESCRIPTOR":"PAYPAL","PAYMENTREQUEST_0_SHIPTONAME":"test buyer","PAYMENTREQUEST_0_SHIPTOSTREET":"Flat no. 507 Wing A Raheja Residency","PAYMENTREQUEST_0_SHIPTOSTREET2":"Film City Road, Goregaon East","PAYMENTREQUEST_0_SHIPTOCITY":"Mumbai","PAYMENTREQUEST_0_SHIPTOSTATE":"Maharashtra","PAYMENTREQUEST_0_SHIPTOZIP":"400097","PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE":"IN","PAYMENTREQUEST_0_SHIPTOCOUNTRYNAME":"India","PAYMENTREQUEST_0_ADDRESSSTATUS":"Confirmed","PAYMENTREQUEST_1_CURRENCYCODE":"USD","PAYMENTREQUEST_1_AMT":"3.00","PAYMENTREQUEST_1_SHIPPINGAMT":"0.00","PAYMENTREQUEST_1_HANDLINGAMT":"0.00","PAYMENTREQUEST_1_TAXAMT":"0.00","PAYMENTREQUEST_1_INSURANCEAMT":"0.00","PAYMENTREQUEST_1_SHIPDISCAMT":"0.00","PAYMENTREQUEST_1_TRANSACTIONID":"69C69460B3509782R","PAYMENTREQUEST_1_SELLERPAYPALACCOUNTID":"futurestarr2012@yahoo.com","PAYMENTREQUEST_1_INSURANCEOPTIONOFFERED":"false","PAYMENTREQUEST_1_PAYMENTREQUESTID":"talent_sale_admin","PAYMENTREQUEST_1_SOFTDESCRIPTOR":"PAYPAL","PAYMENTREQUEST_1_SHIPTONAME":"test buyer","PAYMENTREQUEST_1_SHIPTOSTREET":"Flat no. 507 Wing A Raheja Residency","PAYMENTREQUEST_1_SHIPTOSTREET2":"Film City Road, Goregaon East","PAYMENTREQUEST_1_SHIPTOCITY":"Mumbai","PAYMENTREQUEST_1_SHIPTOSTATE":"Maharashtra","PAYMENTREQUEST_1_SHIPTOZIP":"400097","PAYMENTREQUEST_1_SHIPTOCOUNTRYCODE":"IN","PAYMENTREQUEST_1_SHIPTOCOUNTRYNAME":"India","PAYMENTREQUEST_1_ADDRESSSTATUS":"Confirmed","PAYMENTREQUESTINFO_0_TRANSACTIONID":"7X3457369S270561R","PAYMENTREQUESTINFO_0_PAYMENTREQUESTID":"talent_sale_168","PAYMENTREQUESTINFO_1_TRANSACTIONID":"69C69460B3509782R","PAYMENTREQUESTINFO_1_PAYMENTREQUESTID":"talent_sale_admin"}', '2019-04-12 17:35:26'),
(72, '227', '06782074D4469340H', 'EC-22A37029U6779881S', '97SXYNCZD9HW2', NULL, '250', NULL, '1000', 'Talent Purchase', '{"TOKEN":"EC-22A37029U6779881S","BILLINGAGREEMENTACCEPTEDSTATUS":"0","CHECKOUTSTATUS":"PaymentActionCompleted","TIMESTAMP":"2019-04-12T18:00:35Z","CORRELATIONID":"f22387f892e70","ACK":"Success","VERSION":"113","BUILD":"52475881","EMAIL":"rahul.botad-buyer@gmail.com","PAYERID":"97SXYNCZD9HW2","PAYERSTATUS":"verified","FIRSTNAME":"test","LASTNAME":"buyer","COUNTRYCODE":"IN","SHIPTONAME":"test buyer","SHIPTOSTREET":"Flat no. 507 Wing A Raheja Residency","SHIPTOSTREET2":"Film City Road, Goregaon East","SHIPTOCITY":"Mumbai","SHIPTOSTATE":"Maharashtra","SHIPTOZIP":"400097","SHIPTOCOUNTRYCODE":"IN","SHIPTOCOUNTRYNAME":"India","ADDRESSSTATUS":"Confirmed","CURRENCYCODE":"USD","AMT":"700.00","SHIPPINGAMT":"0.00","HANDLINGAMT":"0.00","TAXAMT":"0.00","INSURANCEAMT":"0.00","SHIPDISCAMT":"0.00","TRANSACTIONID":"06782074D4469340H","INSURANCEOPTIONOFFERED":"false","PAYMENTREQUEST_0_CURRENCYCODE":"USD","PAYMENTREQUEST_0_AMT":"700.00","PAYMENTREQUEST_0_SHIPPINGAMT":"0.00","PAYMENTREQUEST_0_HANDLINGAMT":"0.00","PAYMENTREQUEST_0_TAXAMT":"0.00","PAYMENTREQUEST_0_INSURANCEAMT":"0.00","PAYMENTREQUEST_0_SHIPDISCAMT":"0.00","PAYMENTREQUEST_0_TRANSACTIONID":"06782074D4469340H","PAYMENTREQUEST_0_SELLERPAYPALACCOUNTID":"kelly@paypal.com","PAYMENTREQUEST_0_INSURANCEOPTIONOFFERED":"false","PAYMENTREQUEST_0_PAYMENTREQUESTID":"talent_sale_1","PAYMENTREQUEST_0_SOFTDESCRIPTOR":"PAYPAL *KELLY","PAYMENTREQUEST_0_SHIPTONAME":"test buyer","PAYMENTREQUEST_0_SHIPTOSTREET":"Flat no. 507 Wing A Raheja Residency","PAYMENTREQUEST_0_SHIPTOSTREET2":"Film City Road, Goregaon East","PAYMENTREQUEST_0_SHIPTOCITY":"Mumbai","PAYMENTREQUEST_0_SHIPTOSTATE":"Maharashtra","PAYMENTREQUEST_0_SHIPTOZIP":"400097","PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE":"IN","PAYMENTREQUEST_0_SHIPTOCOUNTRYNAME":"India","PAYMENTREQUEST_0_ADDRESSSTATUS":"Confirmed","PAYMENTREQUEST_1_CURRENCYCODE":"USD","PAYMENTREQUEST_1_AMT":"300.00","PAYMENTREQUEST_1_SHIPPINGAMT":"0.00","PAYMENTREQUEST_1_HANDLINGAMT":"0.00","PAYMENTREQUEST_1_TAXAMT":"0.00","PAYMENTREQUEST_1_INSURANCEAMT":"0.00","PAYMENTREQUEST_1_SHIPDISCAMT":"0.00","PAYMENTREQUEST_1_TRANSACTIONID":"2VV07463LY196781J","PAYMENTREQUEST_1_SELLERPAYPALACCOUNTID":"futurestarr2012@yahoo.com","PAYMENTREQUEST_1_INSURANCEOPTIONOFFERED":"false","PAYMENTREQUEST_1_PAYMENTREQUESTID":"talent_sale_admin","PAYMENTREQUEST_1_SOFTDESCRIPTOR":"PAYPAL","PAYMENTREQUEST_1_SHIPTONAME":"test buyer","PAYMENTREQUEST_1_SHIPTOSTREET":"Flat no. 507 Wing A Raheja Residency","PAYMENTREQUEST_1_SHIPTOSTREET2":"Film City Road, Goregaon East","PAYMENTREQUEST_1_SHIPTOCITY":"Mumbai","PAYMENTREQUEST_1_SHIPTOSTATE":"Maharashtra","PAYMENTREQUEST_1_SHIPTOZIP":"400097","PAYMENTREQUEST_1_SHIPTOCOUNTRYCODE":"IN","PAYMENTREQUEST_1_SHIPTOCOUNTRYNAME":"India","PAYMENTREQUEST_1_ADDRESSSTATUS":"Confirmed","PAYMENTREQUESTINFO_0_TRANSACTIONID":"06782074D4469340H","PAYMENTREQUESTINFO_0_PAYMENTREQUESTID":"talent_sale_1","PAYMENTREQUESTINFO_1_TRANSACTIONID":"2VV07463LY196781J","PAYMENTREQUESTINFO_1_PAYMENTREQUESTID":"talent_sale_admin"}', '2019-04-12 18:00:35'),
(73, '197', '4XH290079T4588709', 'EC-26F15932AB219415J', 'ZHGFZJMCVA2X4', NULL, '284,234,273', NULL, '152', 'Talent Purchase', '{"TOKEN":"EC-26F15932AB219415J","BILLINGAGREEMENTACCEPTEDSTATUS":"0","CHECKOUTSTATUS":"PaymentActionCompleted","TIMESTAMP":"2019-04-12T18:40:39Z","CORRELATIONID":"c016357df327","ACK":"Success","VERSION":"113","BUILD":"52475881","EMAIL":"bhaskerbhatt1234@gmail.com","PAYERID":"ZHGFZJMCVA2X4","PAYERSTATUS":"verified","FIRSTNAME":"Bhasker","LASTNAME":"Bhatt","COUNTRYCODE":"US","SHIPTONAME":"Bhasker Bhatt","SHIPTOSTREET":"1 Main St","SHIPTOCITY":"San Jose","SHIPTOSTATE":"CA","SHIPTOZIP":"95131","SHIPTOCOUNTRYCODE":"US","SHIPTOCOUNTRYNAME":"United States","ADDRESSSTATUS":"Confirmed","CURRENCYCODE":"USD","AMT":"28.00","SHIPPINGAMT":"0.00","HANDLINGAMT":"0.00","TAXAMT":"0.00","INSURANCEAMT":"0.00","SHIPDISCAMT":"0.00","TRANSACTIONID":"4XH290079T4588709","INSURANCEOPTIONOFFERED":"false","PAYMENTREQUEST_0_CURRENCYCODE":"USD","PAYMENTREQUEST_0_AMT":"28.00","PAYMENTREQUEST_0_SHIPPINGAMT":"0.00","PAYMENTREQUEST_0_HANDLINGAMT":"0.00","PAYMENTREQUEST_0_TAXAMT":"0.00","PAYMENTREQUEST_0_INSURANCEAMT":"0.00","PAYMENTREQUEST_0_SHIPDISCAMT":"0.00","PAYMENTREQUEST_0_TRANSACTIONID":"4XH290079T4588709","PAYMENTREQUEST_0_SELLERPAYPALACCOUNTID":"developer.walkwel@gmail.com","PAYMENTREQUEST_0_INSURANCEOPTIONOFFERED":"false","PAYMENTREQUEST_0_PAYMENTREQUESTID":"talent_sale_212","PAYMENTREQUEST_0_SHIPTONAME":"Bhasker Bhatt","PAYMENTREQUEST_0_SHIPTOSTREET":"1 Main St","PAYMENTREQUEST_0_SHIPTOCITY":"San Jose","PAYMENTREQUEST_0_SHIPTOSTATE":"CA","PAYMENTREQUEST_0_SHIPTOZIP":"95131","PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE":"US","PAYMENTREQUEST_0_SHIPTOCOUNTRYNAME":"United States","PAYMENTREQUEST_0_ADDRESSSTATUS":"Confirmed","PAYMENTREQUEST_1_CURRENCYCODE":"USD","PAYMENTREQUEST_1_AMT":"8.40","PAYMENTREQUEST_1_SHIPPINGAMT":"0.00","PAYMENTREQUEST_1_HANDLINGAMT":"0.00","PAYMENTREQUEST_1_TAXAMT":"0.00","PAYMENTREQUEST_1_INSURANCEAMT":"0.00","PAYMENTREQUEST_1_SHIPDISCAMT":"0.00","PAYMENTREQUEST_1_SELLERPAYPALACCOUNTID":"kelly@paypal.com","PAYMENTREQUEST_1_INSURANCEOPTIONOFFERED":"false","PAYMENTREQUEST_1_PAYMENTREQUESTID":"talent_sale_1","PAYMENTREQUEST_1_SHIPTONAME":"Bhasker Bhatt","PAYMENTREQUEST_1_SHIPTOSTREET":"1 Main St","PAYMENTREQUEST_1_SHIPTOCITY":"San Jose","PAYMENTREQUEST_1_SHIPTOSTATE":"CA","PAYMENTREQUEST_1_SHIPTOZIP":"95131","PAYMENTREQUEST_1_SHIPTOCOUNTRYCODE":"US","PAYMENTREQUEST_1_SHIPTOCOUNTRYNAME":"United States","PAYMENTREQUEST_1_ADDRESSSTATUS":"Confirmed","PAYMENTREQUEST_2_CURRENCYCODE":"USD","PAYMENTREQUEST_2_AMT":"70.00","PAYMENTREQUEST_2_SHIPPINGAMT":"0.00","PAYMENTREQUEST_2_HANDLINGAMT":"0.00","PAYMENTREQUEST_2_TAXAMT":"0.00","PAYMENTREQUEST_2_INSURANCEAMT":"0.00","PAYMENTREQUEST_2_SHIPDISCAMT":"0.00","PAYMENTREQUEST_2_SELLERPAYPALACCOUNTID":"qysen@wmail.club","PAYMENTREQUEST_2_INSURANCEOPTIONOFFERED":"false","PAYMENTREQUEST_2_PAYMENTREQUESTID":"talent_sale_168","PAYMENTREQUEST_2_SHIPTONAME":"Bhasker Bhatt","PAYMENTREQUEST_2_SHIPTOSTREET":"1 Main St","PAYMENTREQUEST_2_SHIPTOCITY":"San Jose","PAYMENTREQUEST_2_SHIPTOSTATE":"CA","PAYMENTREQUEST_2_SHIPTOZIP":"95131","PAYMENTREQUEST_2_SHIPTOCOUNTRYCODE":"US","PAYMENTREQUEST_2_SHIPTOCOUNTRYNAME":"United States","PAYMENTREQUEST_2_ADDRESSSTATUS":"Confirmed","PAYMENTREQUEST_3_CURRENCYCODE":"USD","PAYMENTREQUEST_3_AMT":"45.60","PAYMENTREQUEST_3_SHIPPINGAMT":"0.00","PAYMENTREQUEST_3_HANDLINGAMT":"0.00","PAYMENTREQUEST_3_TAXAMT":"0.00","PAYMENTREQUEST_3_INSURANCEAMT":"0.00","PAYMENTREQUEST_3_SHIPDISCAMT":"0.00","PAYMENTREQUEST_3_SELLERPAYPALACCOUNTID":"futurestarr2012@yahoo.com","PAYMENTREQUEST_3_INSURANCEOPTIONOFFERED":"false","PAYMENTREQUEST_3_PAYMENTREQUESTID":"talent_sale_admin","PAYMENTREQUEST_3_SHIPTONAME":"Bhasker Bhatt","PAYMENTREQUEST_3_SHIPTOSTREET":"1 Main St","PAYMENTREQUEST_3_SHIPTOCITY":"San Jose","PAYMENTREQUEST_3_SHIPTOSTATE":"CA","PAYMENTREQUEST_3_SHIPTOZIP":"95131","PAYMENTREQUEST_3_SHIPTOCOUNTRYCODE":"US","PAYMENTREQUEST_3_SHIPTOCOUNTRYNAME":"United States","PAYMENTREQUEST_3_ADDRESSSTATUS":"Confirmed","PAYMENTREQUESTINFO_0_TRANSACTIONID":"4XH290079T4588709","PAYMENTREQUESTINFO_0_PAYMENTREQUESTID":"talent_sale_212","PAYMENTREQUESTINFO_1_PAYMENTREQUESTID":"talent_sale_1","PAYMENTREQUESTINFO_2_PAYMENTREQUESTID":"talent_sale_168","PAYMENTREQUESTINFO_3_PAYMENTREQUESTID":"talent_sale_admin"}', '2019-04-12 18:40:39');
INSERT INTO `payment_history` (`id`, `user_id`, `transaction_id`, `token`, `PayerID`, `stripe_acc`, `talent_id`, `plan_id`, `amount`, `description`, `transaction_payload`, `created_at`) VALUES
(74, '214', '9WX79861YM637231X', 'EC-09B467423N981831C', 'GC5SCKF5T5KY8', NULL, '274,276', NULL, '550', 'Talent Purchase', '{"TOKEN":"EC-09B467423N981831C","BILLINGAGREEMENTACCEPTEDSTATUS":"0","CHECKOUTSTATUS":"PaymentActionCompleted","TIMESTAMP":"2019-04-13T11:06:46Z","CORRELATIONID":"44036618f2d4f","ACK":"Success","VERSION":"113","BUILD":"52475881","EMAIL":"praveen.patel@5exceptions.com","PAYERID":"GC5SCKF5T5KY8","PAYERSTATUS":"unverified","FIRSTNAME":"praveen","LASTNAME":"patel","COUNTRYCODE":"IN","SHIPTONAME":"praveen patel","SHIPTOSTREET":"Flat no. 507 Wing A Raheja Residency","SHIPTOSTREET2":"Film City Road, Goregaon East","SHIPTOCITY":"Mumbai","SHIPTOSTATE":"Maharashtra","SHIPTOZIP":"400097","SHIPTOCOUNTRYCODE":"IN","SHIPTOCOUNTRYNAME":"India","ADDRESSSTATUS":"Confirmed","CURRENCYCODE":"USD","AMT":"385.00","SHIPPINGAMT":"0.00","HANDLINGAMT":"0.00","TAXAMT":"0.00","INSURANCEAMT":"0.00","SHIPDISCAMT":"0.00","TRANSACTIONID":"9WX79861YM637231X","INSURANCEOPTIONOFFERED":"false","PAYMENTREQUEST_0_CURRENCYCODE":"USD","PAYMENTREQUEST_0_AMT":"385.00","PAYMENTREQUEST_0_SHIPPINGAMT":"0.00","PAYMENTREQUEST_0_HANDLINGAMT":"0.00","PAYMENTREQUEST_0_TAXAMT":"0.00","PAYMENTREQUEST_0_INSURANCEAMT":"0.00","PAYMENTREQUEST_0_SHIPDISCAMT":"0.00","PAYMENTREQUEST_0_TRANSACTIONID":"9WX79861YM637231X","PAYMENTREQUEST_0_SELLERPAYPALACCOUNTID":"developer.walkwel@gmail.com","PAYMENTREQUEST_0_INSURANCEOPTIONOFFERED":"false","PAYMENTREQUEST_0_PAYMENTREQUESTID":"talent_sale_212","PAYMENTREQUEST_0_SOFTDESCRIPTOR":"PAYPAL","PAYMENTREQUEST_0_SHIPTONAME":"praveen patel","PAYMENTREQUEST_0_SHIPTOSTREET":"Flat no. 507 Wing A Raheja Residency","PAYMENTREQUEST_0_SHIPTOSTREET2":"Film City Road, Goregaon East","PAYMENTREQUEST_0_SHIPTOCITY":"Mumbai","PAYMENTREQUEST_0_SHIPTOSTATE":"Maharashtra","PAYMENTREQUEST_0_SHIPTOZIP":"400097","PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE":"IN","PAYMENTREQUEST_0_SHIPTOCOUNTRYNAME":"India","PAYMENTREQUEST_0_ADDRESSSTATUS":"Confirmed","PAYMENTREQUEST_1_CURRENCYCODE":"USD","PAYMENTREQUEST_1_AMT":"165.00","PAYMENTREQUEST_1_SHIPPINGAMT":"0.00","PAYMENTREQUEST_1_HANDLINGAMT":"0.00","PAYMENTREQUEST_1_TAXAMT":"0.00","PAYMENTREQUEST_1_INSURANCEAMT":"0.00","PAYMENTREQUEST_1_SHIPDISCAMT":"0.00","PAYMENTREQUEST_1_TRANSACTIONID":"2C68886895323903B","PAYMENTREQUEST_1_SELLERPAYPALACCOUNTID":"futurestarr2012@yahoo.com","PAYMENTREQUEST_1_INSURANCEOPTIONOFFERED":"false","PAYMENTREQUEST_1_PAYMENTREQUESTID":"talent_sale_admin","PAYMENTREQUEST_1_SOFTDESCRIPTOR":"PAYPAL","PAYMENTREQUEST_1_SHIPTONAME":"praveen patel","PAYMENTREQUEST_1_SHIPTOSTREET":"Flat no. 507 Wing A Raheja Residency","PAYMENTREQUEST_1_SHIPTOSTREET2":"Film City Road, Goregaon East","PAYMENTREQUEST_1_SHIPTOCITY":"Mumbai","PAYMENTREQUEST_1_SHIPTOSTATE":"Maharashtra","PAYMENTREQUEST_1_SHIPTOZIP":"400097","PAYMENTREQUEST_1_SHIPTOCOUNTRYCODE":"IN","PAYMENTREQUEST_1_SHIPTOCOUNTRYNAME":"India","PAYMENTREQUEST_1_ADDRESSSTATUS":"Confirmed","PAYMENTREQUESTINFO_0_TRANSACTIONID":"9WX79861YM637231X","PAYMENTREQUESTINFO_0_PAYMENTREQUESTID":"talent_sale_212","PAYMENTREQUESTINFO_1_TRANSACTIONID":"2C68886895323903B","PAYMENTREQUESTINFO_1_PAYMENTREQUESTID":"talent_sale_admin"}', '2019-04-13 11:06:46'),
(75, '217', '7L571283BR6877140', 'EC-6HF83874D4940722H', 'ZHGFZJMCVA2X4', NULL, '284', NULL, '40', 'Talent Purchase', '{"TOKEN":"EC-6HF83874D4940722H","BILLINGAGREEMENTACCEPTEDSTATUS":"0","CHECKOUTSTATUS":"PaymentActionCompleted","TIMESTAMP":"2019-04-14T12:19:55Z","CORRELATIONID":"8e5f299e3dea9","ACK":"Success","VERSION":"113","BUILD":"52475881","EMAIL":"bhaskerbhatt1234@gmail.com","PAYERID":"ZHGFZJMCVA2X4","PAYERSTATUS":"verified","FIRSTNAME":"Bhasker","LASTNAME":"Bhatt","COUNTRYCODE":"US","SHIPTONAME":"Bhasker Bhatt","SHIPTOSTREET":"1 Main St","SHIPTOCITY":"San Jose","SHIPTOSTATE":"CA","SHIPTOZIP":"95131","SHIPTOCOUNTRYCODE":"US","SHIPTOCOUNTRYNAME":"United States","ADDRESSSTATUS":"Confirmed","CURRENCYCODE":"USD","AMT":"28.00","SHIPPINGAMT":"0.00","HANDLINGAMT":"0.00","TAXAMT":"0.00","INSURANCEAMT":"0.00","SHIPDISCAMT":"0.00","TRANSACTIONID":"7L571283BR6877140","INSURANCEOPTIONOFFERED":"false","PAYMENTREQUEST_0_CURRENCYCODE":"USD","PAYMENTREQUEST_0_AMT":"28.00","PAYMENTREQUEST_0_SHIPPINGAMT":"0.00","PAYMENTREQUEST_0_HANDLINGAMT":"0.00","PAYMENTREQUEST_0_TAXAMT":"0.00","PAYMENTREQUEST_0_INSURANCEAMT":"0.00","PAYMENTREQUEST_0_SHIPDISCAMT":"0.00","PAYMENTREQUEST_0_TRANSACTIONID":"7L571283BR6877140","PAYMENTREQUEST_0_SELLERPAYPALACCOUNTID":"developer.walkwel@gmail.com","PAYMENTREQUEST_0_INSURANCEOPTIONOFFERED":"false","PAYMENTREQUEST_0_PAYMENTREQUESTID":"talent_sale_212","PAYMENTREQUEST_0_SHIPTONAME":"Bhasker Bhatt","PAYMENTREQUEST_0_SHIPTOSTREET":"1 Main St","PAYMENTREQUEST_0_SHIPTOCITY":"San Jose","PAYMENTREQUEST_0_SHIPTOSTATE":"CA","PAYMENTREQUEST_0_SHIPTOZIP":"95131","PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE":"US","PAYMENTREQUEST_0_SHIPTOCOUNTRYNAME":"United States","PAYMENTREQUEST_0_ADDRESSSTATUS":"Confirmed","PAYMENTREQUEST_1_CURRENCYCODE":"USD","PAYMENTREQUEST_1_AMT":"12.00","PAYMENTREQUEST_1_SHIPPINGAMT":"0.00","PAYMENTREQUEST_1_HANDLINGAMT":"0.00","PAYMENTREQUEST_1_TAXAMT":"0.00","PAYMENTREQUEST_1_INSURANCEAMT":"0.00","PAYMENTREQUEST_1_SHIPDISCAMT":"0.00","PAYMENTREQUEST_1_TRANSACTIONID":"3UN786012U6104936","PAYMENTREQUEST_1_SELLERPAYPALACCOUNTID":"futurestarr2012@yahoo.com","PAYMENTREQUEST_1_INSURANCEOPTIONOFFERED":"false","PAYMENTREQUEST_1_PAYMENTREQUESTID":"talent_sale_admin","PAYMENTREQUEST_1_SHIPTONAME":"Bhasker Bhatt","PAYMENTREQUEST_1_SHIPTOSTREET":"1 Main St","PAYMENTREQUEST_1_SHIPTOCITY":"San Jose","PAYMENTREQUEST_1_SHIPTOSTATE":"CA","PAYMENTREQUEST_1_SHIPTOZIP":"95131","PAYMENTREQUEST_1_SHIPTOCOUNTRYCODE":"US","PAYMENTREQUEST_1_SHIPTOCOUNTRYNAME":"United States","PAYMENTREQUEST_1_ADDRESSSTATUS":"Confirmed","PAYMENTREQUESTINFO_0_TRANSACTIONID":"7L571283BR6877140","PAYMENTREQUESTINFO_0_PAYMENTREQUESTID":"talent_sale_212","PAYMENTREQUESTINFO_1_TRANSACTIONID":"3UN786012U6104936","PAYMENTREQUESTINFO_1_PAYMENTREQUESTID":"talent_sale_admin"}', '2019-04-14 12:19:55');

-- --------------------------------------------------------

--
-- Table structure for table `payment_statuses`
--

CREATE TABLE `payment_statuses` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_statuses`
--

INSERT INTO `payment_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Inital', '2018-05-25 18:19:24', '2018-05-25 18:19:24'),
(2, 'Pennding', '2018-05-25 18:19:24', '2018-05-25 18:19:24'),
(3, 'Completed', '2018-05-25 18:19:33', '2018-05-25 18:19:33'),
(4, 'Failed', '2018-05-25 18:20:18', '2018-05-25 18:20:18');

-- --------------------------------------------------------

--
-- Table structure for table `plans`
--

CREATE TABLE `plans` (
  `id` int(11) NOT NULL,
  `plan_name` varchar(225) NOT NULL,
  `description` varchar(225) NOT NULL,
  `price` varchar(225) NOT NULL,
  `time_period` varchar(225) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `plans`
--

INSERT INTO `plans` (`id`, `plan_name`, `description`, `price`, `time_period`, `created_at`, `updated_at`) VALUES
(1, 'Basic Plan', '10 clicks per month', '20', '30', '2017-12-07 13:04:55', '2017-12-07 13:04:55'),
(2, 'Economy Plan', '25 clicks per month', '40', '30', '2017-12-07 13:04:55', '2017-12-07 13:04:55'),
(3, 'Ultimate Plan', '100 clicks per month', '175', '30', '2017-12-07 13:05:25', '2017-12-07 13:05:25');

-- --------------------------------------------------------

--
-- Table structure for table `product_media`
--

CREATE TABLE `product_media` (
  `id` int(11) NOT NULL,
  `talent_id` int(11) NOT NULL,
  `pdf_name` varchar(225) NOT NULL,
  `pdf_path` varchar(225) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_media`
--

INSERT INTO `product_media` (`id`, `talent_id`, `pdf_name`, `pdf_path`, `created_at`, `updated_at`) VALUES
(11, 170, 'pixar.mp4', 'uploads/170-seller-product-media/pixar.mp4', '2018-05-01 06:34:22', '2018-05-01 06:34:22'),
(12, 171, 'Timesheet Report - Zehntech Technologies Pvt. Ltd..pdf', 'uploads/seller-product-media/171-Timesheet Report - Zehntech Technologies Pvt. Ltd..pdf', '2018-05-01 08:37:59', '2018-05-01 08:37:59'),
(13, 176, 'atoo artist.jpg', 'uploads/seller-product-media/189-Tatoo artist.jpg', '2018-05-15 04:23:18', '2018-05-15 04:23:18'),
(14, 177, 'videoplayback.mp4', 'uploads/seller-product-media/177-videoplayback.mp4', '2018-06-04 10:12:46', '2018-06-04 10:12:46'),
(18, 173, 'mov_bbb.mp4', 'uploads/seller-product-media/180-mov_bbb.mp4', '2018-08-02 11:22:04', '2018-08-02 11:22:04'),
(19, 182, 's9qgz-g5flo.mp4', 'uploads/seller-product-media/182-s9qgz-g5flo.mp4', '2018-08-06 13:40:00', '2018-08-06 13:40:00'),
(20, 184, '500DD6F1-7EFB-4E7A-8EF5-50780FF797D8.jpg', 'uploads/seller-product-media/184-500DD6F1-7EFB-4E7A-8EF5-50780FF797D8.jpg', '2018-08-09 09:24:55', '2018-08-09 09:24:55'),
(21, 184, 'FS.pdf', 'uploads/seller-product-media/184-FS.pdf', '2018-08-09 09:25:35', '2018-08-09 09:25:35'),
(22, 185, 'FS.pdf', 'uploads/seller-product-media/185-FS.pdf', '2018-08-09 10:43:34', '2018-08-09 10:43:34'),
(23, 186, '15MG.mp4', 'uploads/seller-product-media/186-15MG.mp4', '2018-08-10 13:26:51', '2018-08-10 13:26:51'),
(24, 187, '2.jpg', 'uploads/seller-product-media/187-2.jpg', '2018-08-10 13:41:52', '2018-08-10 13:41:52'),
(25, 189, 'Tatoo artist.jpg', 'uploads/seller-product-media/189-Tatoo artist.jpg', '2018-08-11 10:13:44', '2018-08-11 10:13:44'),
(26, 190, 'cat 30 sec.mp4', 'uploads/seller-product-media/190-cat 30 sec.mp4', '2018-08-11 10:19:16', '2018-08-11 10:19:16'),
(27, 191, 'animation.mp4', 'uploads/seller-product-media/191-animation.mp4', '2018-08-11 10:38:15', '2018-08-11 10:38:15'),
(30, 194, 'mathematics.jpg', 'uploads/seller-product-media/194-mathematics.jpg', '2018-08-11 12:33:08', '2018-08-11 12:33:08'),
(31, 195, 'tester.jpg', 'uploads/seller-product-media/195-tester.jpg', '2018-08-11 12:35:20', '2018-08-11 12:35:20'),
(32, 196, 'animation.mp4', 'uploads/seller-product-media/196-animation.mp4', '2018-08-11 12:39:14', '2018-08-11 12:39:14'),
(34, 198, '15MG.mp4', 'uploads/seller-product-media/198-15MG.mp4', '2018-08-14 06:20:48', '2018-08-14 06:20:48'),
(35, 199, '15MG.mp4', 'uploads/seller-product-media/199-15MG.mp4', '2018-08-16 11:09:10', '2018-08-16 11:09:10'),
(36, 200, 'cat 30 sec.mp4', 'uploads/seller-product-media/200-cat 30 sec.mp4', '2018-08-16 11:19:02', '2018-08-16 11:19:02'),
(37, 201, '15MG.mp4', 'uploads/seller-product-media/201-15MG.mp4', '2018-08-16 11:28:37', '2018-08-16 11:28:37'),
(38, 202, 'animation.mp4', 'uploads/seller-product-media/202-animation.mp4', '2018-08-16 11:57:25', '2018-08-16 11:57:25'),
(39, 203, 'animation.mp4', 'uploads/seller-product-media/203-animation.mp4', '2018-08-16 11:59:10', '2018-08-16 11:59:10'),
(40, 204, '2.jpg', 'uploads/seller-product-media/204-2.jpg', '2018-08-16 12:42:46', '2018-08-16 12:42:46'),
(41, 205, '15MG.mp4', 'uploads/seller-product-media/205-15MG.mp4', '2018-08-16 13:40:56', '2018-08-16 13:40:56'),
(42, 206, '15MG.mp4', 'uploads/seller-product-media/206-15MG.mp4', '2018-08-16 13:43:30', '2018-08-16 13:43:30'),
(54, 207, '15MG.mp4', 'uploads/seller-product-media/207-15MG.mp4', '2018-08-17 09:08:35', '2018-08-17 09:08:35'),
(55, 207, '2.jpg', 'uploads/seller-product-media/207-2.jpg', '2018-08-17 09:08:35', '2018-08-17 09:08:35'),
(56, 207, 'cat 30 sec.mp4', 'uploads/seller-product-media/207-cat 30 sec.mp4', '2018-08-17 09:08:41', '2018-08-17 09:08:41'),
(57, 207, 'Tatoo artist.jpg', 'uploads/seller-product-media/207-Tatoo artist.jpg', '2018-08-17 09:08:41', '2018-08-17 09:08:41'),
(58, 208, 'Author.jpeg', 'uploads/seller-product-media/208-Author.jpeg', '2018-08-17 11:54:21', '2018-08-17 11:54:21'),
(59, 209, 'seller.jpg', '/uploads/seller-product-media/seller.jpg', '2018-08-20 09:26:46', '2018-08-20 09:26:46'),
(92, 210, 'terms-conditaions.jpg', '/uploads/seller-product-media/210-terms-conditaions.jpg', '2018-08-22 11:15:04', '2018-08-22 11:15:04'),
(93, 212, '182-s9qgz-g5flo.mp4', 'uploads/seller-product-media/212-182-s9qgz-g5flo.mp4', '2018-08-22 11:25:23', '2018-08-22 11:25:23'),
(94, 212, 'imag3.jpeg', 'uploads/seller-product-media/212-imag3.jpeg', '2018-08-22 11:25:38', '2018-08-22 11:25:38'),
(95, 212, 'img1.jpeg', 'uploads/seller-product-media/212-img1.jpeg', '2018-08-22 11:25:40', '2018-08-22 11:25:40'),
(96, 212, 'img2.jpg', 'uploads/seller-product-media/212-img2.jpg', '2018-08-22 11:25:42', '2018-08-22 11:25:42'),
(97, 212, 'pdf-sample.pdf', 'uploads/seller-product-media/212-pdf-sample.pdf', '2018-08-22 11:25:43', '2018-08-22 11:25:43'),
(98, 212, 'seller.jpg', 'uploads/seller-product-media/212-seller.jpg', '2018-08-22 11:25:49', '2018-08-22 11:25:49'),
(102, 214, 'imag3.jpeg', '/uploads/seller-product-media/214-imag3.jpeg', '2018-08-22 13:39:08', '2018-08-22 13:39:08'),
(103, 215, 'FD853843-1A70-4843-9953-ECD753427102.jpeg', 'uploads/seller-product-media/215-FD853843-1A70-4843-9953-ECD753427102.jpeg', '2018-08-22 13:50:27', '2018-08-22 13:50:27'),
(105, 221, 'pdf-sample.pdf', 'uploads/seller-product-media/221-pdf-sample.pdf', '2018-08-27 09:46:02', '2018-08-27 09:46:02'),
(106, 222, '182-s9qgz-g5flo.mp4', 'uploads/seller-product-media/222-182-s9qgz-g5flo.mp4', '2018-08-27 09:54:27', '2018-08-27 09:54:27'),
(107, 223, '55706153925__F2813E37-B69C-44C9-BDDB-ADC44C7D786B.MOV', 'uploads/seller-product-media/223-55706153925__F2813E37-B69C-44C9-BDDB-ADC44C7D786B.MOV', '2018-08-27 11:20:49', '2018-08-27 11:20:49'),
(108, 219, 'imag3.jpeg', '/uploads/seller-product-media/214-imag3.jpeg', '2018-08-22 13:39:08', '2018-08-22 13:39:08'),
(109, 224, 'National Geography.jpg', '/uploads/seller-product-media/224-National Geography.jpg', '2018-08-28 10:53:51', '2018-08-28 10:53:51'),
(111, 224, 'National Geography.jpg', '/uploads/seller-product-media/224-National Geography.jpg', '2018-08-28 10:55:03', '2018-08-28 10:55:03'),
(112, 224, 'seller.jpg', '/uploads/seller-product-media/224-seller.jpg', '2018-08-28 10:55:06', '2018-08-28 10:55:06'),
(113, 225, 'image.jpg', 'uploads/seller-product-media/225-image.jpg', '2018-08-28 11:17:27', '2018-08-28 11:17:27'),
(114, 225, '55714776401__B8B4CDBC-D876-48A7-AE74-04426EF52EBB.MOV', 'uploads/seller-product-media/225-55714776401__B8B4CDBC-D876-48A7-AE74-04426EF52EBB.MOV', '2018-08-28 11:17:28', '2018-08-28 11:17:28'),
(115, 225, '3C4C6188-88C0-4415-9AF6-44DB33E62291.jpeg', 'uploads/seller-product-media/225-3C4C6188-88C0-4415-9AF6-44DB33E62291.jpeg', '2018-08-28 11:22:33', '2018-08-28 11:22:33'),
(116, 225, '82BBE6BE-A813-450E-8B1D-76D80E6E4E73.png', 'uploads/seller-product-media/225-82BBE6BE-A813-450E-8B1D-76D80E6E4E73.png', '2018-08-28 11:22:36', '2018-08-28 11:22:36'),
(118, 232, 'step.mov', 'uploads/seller-product-media/232-step.mov', '2018-08-29 10:19:16', '2018-08-29 10:19:16'),
(119, 233, 'SampleVideo_1280x720_5mb.mkv', 'uploads/seller-product-media/233-SampleVideo_1280x720_5mb.mkv', '2018-08-29 10:23:52', '2018-08-29 10:23:52'),
(120, 234, 'step.mov', 'uploads/seller-product-media/234-step.mov', '2018-08-29 10:25:38', '2018-08-29 10:25:38'),
(121, 235, 'step.mov', 'uploads/seller-product-media/235-step.mov', '2018-08-29 10:35:15', '2018-08-29 10:35:15'),
(123, 181, 'step.mov', '/uploads/seller-product-media/181-step.mov', '2018-08-29 10:55:48', '2018-08-29 10:55:48'),
(124, 180, 'SampleVideo_1280x720_5mb.mkv', '/uploads/seller-product-media/180-SampleVideo_1280x720_5mb.mkv', '2018-08-29 10:59:35', '2018-08-29 10:59:35'),
(125, 183, 'step.mov', '/uploads/seller-product-media/183-step.mov', '2018-08-29 11:02:00', '2018-08-29 11:02:00'),
(126, 237, 'step.mov', 'uploads/seller-product-media/237-step.mov', '2018-08-29 11:11:22', '2018-08-29 11:11:22'),
(127, 238, 'SampleVideo_1280x720_5mb.mkv', 'uploads/seller-product-media/238-SampleVideo_1280x720_5mb.mkv', '2018-08-29 11:12:57', '2018-08-29 11:12:57'),
(128, 239, 'users.pdf', 'uploads/seller-product-media/239-users.pdf', '2018-08-30 13:51:15', '2018-08-30 13:51:15'),
(129, 240, '182-s9qgz-g5flo.mp4', 'uploads/seller-product-media/240-182-s9qgz-g5flo.mp4', '2018-08-31 09:37:22', '2018-08-31 09:37:22'),
(130, 241, '182-s9qgz-g5flo.mp4', 'uploads/seller-product-media/241-182-s9qgz-g5flo.mp4', '2018-08-31 10:26:00', '2018-08-31 10:26:00'),
(132, 242, 'SampleAudio_0.7mb.mp3', '/uploads/seller-product-media/242-SampleAudio_0.7mb.mp3', '2018-08-31 11:43:01', '2018-08-31 11:43:01'),
(135, 236, 'TM-T82II_trg_en_revC.pdf', '/uploads/seller-product-media/236-TM-T82II_trg_en_revC.pdf', '2018-08-31 11:52:06', '2018-08-31 11:52:06'),
(136, 244, 'video-player-benfryc.psd', 'uploads/seller-product-media/244-video-player-benfryc.psd', '2018-09-03 06:25:44', '2018-09-03 06:25:44'),
(137, 244, '14e58049-c617-4340-b67e-9eaf131c9e3f.jpg', 'uploads/seller-product-media/244-14e58049-c617-4340-b67e-9eaf131c9e3f.jpg', '2018-09-03 06:25:45', '2018-09-03 06:25:45'),
(138, 244, 'icon.png', 'uploads/seller-product-media/244-icon.png', '2018-09-03 06:25:45', '2018-09-03 06:25:45'),
(139, 244, 'users.pdf', 'uploads/seller-product-media/244-users.pdf', '2018-09-03 06:25:46', '2018-09-03 06:25:46'),
(140, 244, 'Statistics Request (2).docx', 'uploads/seller-product-media/244-Statistics Request (2).docx', '2018-09-03 06:25:46', '2018-09-03 06:25:46'),
(141, 245, '14e58049-c617-4340-b67e-9eaf131c9e3f.jpg', 'uploads/seller-product-media/245-14e58049-c617-4340-b67e-9eaf131c9e3f.jpg', '2018-09-03 08:51:48', '2018-09-03 08:51:48'),
(142, 245, 'users.pdf', 'uploads/seller-product-media/245-users.pdf', '2018-09-03 08:51:49', '2018-09-03 08:51:49'),
(143, 245, 'step.mov', 'uploads/seller-product-media/245-step.mov', '2018-09-03 08:51:58', '2018-09-03 08:51:58'),
(144, 245, 'icon.png', 'uploads/seller-product-media/245-icon.png', '2018-09-03 08:51:58', '2018-09-03 08:51:58'),
(145, 245, 'Statistics Request (2).docx', 'uploads/seller-product-media/245-Statistics Request (2).docx', '2018-09-03 08:51:59', '2018-09-03 08:51:59'),
(146, 245, 'FS_Privacy_stgIII-Final.docx', 'uploads/seller-product-media/245-FS_Privacy_stgIII-Final.docx', '2018-09-03 08:52:01', '2018-09-03 08:52:01'),
(147, 245, 'Statistics Request (1).docx', 'uploads/seller-product-media/245-Statistics Request (1).docx', '2018-09-03 08:52:02', '2018-09-03 08:52:02'),
(148, 245, 'privacy-banner.jpg', 'uploads/seller-product-media/245-privacy-banner.jpg', '2018-09-03 08:52:04', '2018-09-03 08:52:04'),
(149, 245, 'Refund-Policy.jpg', 'uploads/seller-product-media/245-Refund-Policy.jpg', '2018-09-03 08:52:05', '2018-09-03 08:52:05'),
(150, 245, 'terms-conditaions.jpg', 'uploads/seller-product-media/245-terms-conditaions.jpg', '2018-09-03 08:52:07', '2018-09-03 08:52:07'),
(151, 247, 'audio.mp3', 'uploads/seller-product-media/247-audio.mp3', '2018-09-03 11:34:24', '2018-09-03 11:34:24'),
(152, 247, 'audio2.mp3', 'uploads/seller-product-media/247-audio2.mp3', '2018-09-03 11:34:43', '2018-09-03 11:34:43'),
(153, 247, 'audio3.mp3', 'uploads/seller-product-media/247-audio3.mp3', '2018-09-03 11:34:58', '2018-09-03 11:34:58'),
(156, 220, 'create ads social buzz image.png', '/uploads/seller-product-media/220-create ads social buzz image.png', '2018-09-07 11:56:19', '2018-09-07 11:56:19'),
(158, 218, 'img2.jpg', '/uploads/seller-product-media/218-img2.jpg', '2018-09-07 11:58:32', '2018-09-07 11:58:32'),
(159, 216, 'seller.jpg', '/uploads/seller-product-media/216-seller.jpg', '2018-09-07 12:00:48', '2018-09-07 12:00:48'),
(160, 217, 'demo.jpg', '/uploads/seller-product-media/217-demo.jpg', '2018-09-07 12:04:05', '2018-09-07 12:04:05'),
(162, 249, 'user.png', '/uploads/seller-product-media/249-user.png', '2018-09-08 09:34:34', '2018-09-08 09:34:34'),
(164, 248, 'National Geography.jpg', '/uploads/seller-product-media/248-National Geography.jpg', '2018-09-08 09:43:28', '2018-09-08 09:43:28'),
(165, 230, 'entertainment2.jpg', '/uploads/seller-product-media/230-entertainment2.jpg', '2018-09-08 10:51:12', '2018-09-08 10:51:12'),
(166, 230, 'author.jpg', '/uploads/seller-product-media/230-author.jpg', '2018-09-08 10:51:15', '2018-09-08 10:51:15'),
(167, 230, 'fitness2.jpg', '/uploads/seller-product-media/230-fitness2.jpg', '2018-09-08 10:51:28', '2018-09-08 10:51:28'),
(168, 231, 'author.jpg', '/uploads/seller-product-media/231-author.jpg', '2018-09-08 10:54:36', '2018-09-08 10:54:36'),
(169, 231, 'science.png', '/uploads/seller-product-media/231-science.png', '2018-09-08 10:55:22', '2018-09-08 10:55:22'),
(170, 231, 'mathematics.jpg', '/uploads/seller-product-media/231-mathematics.jpg', '2018-09-08 10:55:24', '2018-09-08 10:55:24'),
(171, 213, 'flv.flv', '/uploads/seller-product-media/213-flv.flv', '2018-09-10 08:56:30', '2018-09-10 08:56:30'),
(172, 213, 'AVI.AVI', '/uploads/seller-product-media/213-AVI.AVI', '2018-09-10 09:00:48', '2018-09-10 09:00:48'),
(173, 213, 'Mov.mov', '/uploads/seller-product-media/213-Mov.mov', '2018-09-10 09:01:03', '2018-09-10 09:01:03'),
(174, 213, 'ogv.ogv', '/uploads/seller-product-media/213-ogv.ogv', '2018-09-10 09:01:14', '2018-09-10 09:01:14'),
(175, 213, 'mpeg.mp4', '/uploads/seller-product-media/213-mpeg.mp4', '2018-09-10 09:01:25', '2018-09-10 09:01:25'),
(176, 213, 'flv.flv', '/uploads/seller-product-media/213-flv.flv', '2018-09-10 09:01:29', '2018-09-10 09:01:29'),
(177, 259, 'Mov.mov', 'uploads/seller-product-media/259-Mov.mov', '2018-09-11 06:39:11', '2018-09-11 06:39:11'),
(178, 259, 'flv.flv', 'uploads/seller-product-media/259-flv.flv', '2018-09-11 06:39:15', '2018-09-11 06:39:15'),
(179, 259, 'ogv.ogv', 'uploads/seller-product-media/259-ogv.ogv', '2018-09-11 06:39:29', '2018-09-11 06:39:29'),
(180, 259, 'mpeg.mp4', 'uploads/seller-product-media/259-mpeg.mp4', '2018-09-11 06:39:46', '2018-09-11 06:39:46'),
(181, 259, 'AVI.AVI', 'uploads/seller-product-media/259-AVI.AVI', '2018-09-11 06:40:50', '2018-09-11 06:40:50'),
(182, 259, 'futurestar.odt', 'uploads/seller-product-media/259-futurestar.odt', '2018-09-11 06:40:53', '2018-09-11 06:40:53'),
(188, 261, 'seller.jpg', 'uploads/seller-product-media/261-seller.jpg', '2018-09-12 11:47:35', '2018-09-12 11:47:35'),
(189, 261, 'img1.jpeg', 'uploads/seller-product-media/261-img1.jpeg', '2018-09-12 11:47:37', '2018-09-12 11:47:37'),
(190, 261, 'imag3.jpeg', 'uploads/seller-product-media/261-imag3.jpeg', '2018-09-12 11:47:56', '2018-09-12 11:47:56'),
(191, 261, 'img2.jpg', 'uploads/seller-product-media/261-img2.jpg', '2018-09-12 11:47:58', '2018-09-12 11:47:58'),
(192, 261, 'National Geography.jpg', 'uploads/seller-product-media/261-National Geography.jpg', '2018-09-12 11:48:02', '2018-09-12 11:48:02'),
(193, 261, 'dna.jpg', 'uploads/seller-product-media/261-dna.jpg', '2018-09-12 11:48:07', '2018-09-12 11:48:07'),
(194, 261, 'nutrition.jpg', 'uploads/seller-product-media/261-nutrition.jpg', '2018-09-12 11:48:13', '2018-09-12 11:48:13'),
(195, 260, 'seller.jpg', '/uploads/seller-product-media/260-seller.jpg', '2018-09-14 06:31:58', '2018-09-14 06:31:58'),
(196, 260, 'img1.jpeg', '/uploads/seller-product-media/260-img1.jpeg', '2018-09-14 06:32:00', '2018-09-14 06:32:00'),
(197, 260, 'imag3.jpeg', '/uploads/seller-product-media/260-imag3.jpeg', '2018-09-14 06:32:12', '2018-09-14 06:32:12'),
(198, 260, 'img2.jpg', '/uploads/seller-product-media/260-img2.jpg', '2018-09-14 06:32:14', '2018-09-14 06:32:14'),
(199, 260, 'National Geography.jpg', '/uploads/seller-product-media/260-National Geography.jpg', '2018-09-14 06:32:17', '2018-09-14 06:32:17'),
(200, 260, 'dna.jpg', '/uploads/seller-product-media/260-dna.jpg', '2018-09-14 06:32:22', '2018-09-14 06:32:22'),
(201, 260, 'nutrition.jpg', '/uploads/seller-product-media/260-nutrition.jpg', '2018-09-14 06:32:27', '2018-09-14 06:32:27'),
(202, 260, 'fitness.jpg', '/uploads/seller-product-media/260-fitness.jpg', '2018-09-14 06:32:31', '2018-09-14 06:32:31'),
(203, 260, 'demo.jpg', '/uploads/seller-product-media/260-demo.jpg', '2018-09-14 06:32:37', '2018-09-14 06:32:37'),
(207, 263, 'Ranbuild22.mov', 'uploads/seller-product-media/263-Ranbuild22.mov', '2018-09-14 08:56:35', '2018-09-14 08:56:35'),
(208, 263, 'Mov.mov', 'uploads/seller-product-media/263-Mov.mov', '2018-09-14 08:56:59', '2018-09-14 08:56:59'),
(209, 263, 'seller.jpg', 'uploads/seller-product-media/263-seller.jpg', '2018-09-14 08:57:05', '2018-09-14 08:57:05'),
(210, 263, 'img1.jpeg', 'uploads/seller-product-media/263-img1.jpeg', '2018-09-14 08:57:09', '2018-09-14 08:57:09'),
(211, 263, 'imag3.jpeg', 'uploads/seller-product-media/263-imag3.jpeg', '2018-09-14 08:57:23', '2018-09-14 08:57:23'),
(212, 263, 'img2.jpg', 'uploads/seller-product-media/263-img2.jpg', '2018-09-14 08:57:24', '2018-09-14 08:57:24'),
(213, 262, 'pdf-sample.pdf', '/uploads/seller-product-media/262-pdf-sample.pdf', '2018-09-15 08:35:39', '2018-09-15 08:35:39'),
(214, 262, 'user.png', '/uploads/seller-product-media/262-user.png', '2018-09-15 08:35:41', '2018-09-15 08:35:41'),
(215, 262, 'seller.jpg', '/uploads/seller-product-media/262-seller.jpg', '2018-09-15 08:35:45', '2018-09-15 08:35:45'),
(216, 262, 'science fiction.jpeg', '/uploads/seller-product-media/262-science fiction.jpeg', '2018-09-15 08:35:50', '2018-09-15 08:35:50'),
(217, 262, 'science.png', '/uploads/seller-product-media/262-science.png', '2018-09-15 08:36:20', '2018-09-15 08:36:20'),
(218, 266, 'talent_management.jpg', 'uploads/seller-product-media/266-talent_management.jpg', '2018-10-18 13:35:35', '2018-10-18 13:35:35'),
(219, 270, 'ARTIC-TOP-SHELF-MASTER.mp3', 'uploads/seller-product-media/270-ARTIC-TOP-SHELF-MASTER.mp3', '2018-11-12 00:08:53', '2018-11-12 00:08:53'),
(220, 271, 'ARTIC-TOP-SHELF-MASTER.mp3', 'uploads/seller-product-media/271-ARTIC-TOP-SHELF-MASTER.mp3', '2018-11-12 00:12:57', '2018-11-12 00:12:57'),
(221, 272, 'background.png', 'uploads/seller-product-media/272-background.png', '2018-11-14 13:38:43', '2018-11-14 13:38:43'),
(222, 273, 'imglogo-1539846524854.png', 'uploads/seller-product-media/273-imglogo-1539846524854.png', '2018-11-14 14:25:36', '2018-11-14 14:25:36'),
(223, 274, 'images.jpg', 'uploads/seller-product-media/274-images.jpg', '2019-03-26 06:39:13', '2019-03-26 06:39:13'),
(224, 275, 'casserole-dish-2776735__340.jpg', 'uploads/seller-product-media/275-casserole-dish-2776735__340.jpg', '2019-03-26 07:05:52', '2019-03-26 07:05:52'),
(225, 276, 'salad-2068220__340.jpg', 'uploads/seller-product-media/276-salad-2068220__340.jpg', '2019-03-26 07:29:59', '2019-03-26 07:29:59'),
(226, 277, 'smoothie-3697014__340.jpg', 'uploads/seller-product-media/277-smoothie-3697014__340.jpg', '2019-03-26 08:45:55', '2019-03-26 08:45:55'),
(227, 278, 'images (2).jpg', 'uploads/seller-product-media/278-images (2).jpg', '2019-03-26 09:39:16', '2019-03-26 09:39:16'),
(228, 279, 'Music-Notes.jpg', 'uploads/seller-product-media/279-Music-Notes.jpg', '2019-03-26 09:41:19', '2019-03-26 09:41:19'),
(229, 280, 'images (2).jpg', 'uploads/seller-product-media/280-images (2).jpg', '2019-03-26 10:03:20', '2019-03-26 10:03:20'),
(230, 281, 'images (3).jpg', 'uploads/seller-product-media/281-images (3).jpg', '2019-03-26 10:25:59', '2019-03-26 10:25:59'),
(231, 282, 'images.png', 'uploads/seller-product-media/282-images.png', '2019-03-26 11:26:04', '2019-03-26 11:26:04'),
(232, 284, 'images (1).png', 'uploads/seller-product-media/284-images (1).png', '2019-03-26 11:33:21', '2019-03-26 11:33:21'),
(233, 286, 'Wildlife-1.jpg', 'uploads/seller-product-media/286-Wildlife-1.jpg', '2019-04-10 18:03:56', '2019-04-10 18:03:56'),
(235, 286, 'Wildlife-1.jpg', '/uploads/seller-product-media/286-Wildlife-1.jpg', '2019-04-21 12:15:23', '2019-04-21 12:15:23'),
(236, 287, 'Biguine-Make-Up-13-1024x767.jpg', 'uploads/seller-product-media/287-Biguine-Make-Up-13-1024x767.jpg', '2019-04-21 12:26:04', '2019-04-21 12:26:04'),
(237, 291, 'videohive-19115379-underground-rap-music-concert-crowd (online-video-cutter.com).mp4', 'uploads/seller-product-media/291-videohive-19115379-underground-rap-music-concert-crowd (online-video-cutter.com).mp4', '2019-04-23 17:17:46', '2019-04-23 17:17:46');

-- --------------------------------------------------------

--
-- Table structure for table `promote_products`
--

CREATE TABLE `promote_products` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `talent_id` int(11) DEFAULT NULL,
  `social_id` int(11) DEFAULT NULL,
  `title` varchar(1224) DEFAULT NULL,
  `message` varchar(1224) DEFAULT NULL,
  `active` varchar(225) NOT NULL DEFAULT 'true',
  `date` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `promote_products`
--

INSERT INTO `promote_products` (`id`, `user_id`, `talent_id`, `social_id`, `title`, `message`, `active`, `date`, `created_at`, `updated_at`) VALUES
(8, 1, 170, NULL, 'test', 'This is awesome.', 'true', '2018-05-02', '2018-05-02 00:52:08', '2018-05-02 00:52:08'),
(9, 1, 171, NULL, 'model', 'The Indefinite Article. The indefinite article takes two forms. It\'s the word a when it precedes a word that begins with a consonant', 'true', '2018-05-02', '2018-05-02 00:52:59', '2018-05-02 00:52:59'),
(10, 1, 170, NULL, NULL, NULL, 'true', '2018-05-03', '2018-05-03 04:58:37', '2018-05-03 04:58:37'),
(11, 1, 170, NULL, NULL, NULL, 'true', '2018-05-16', '2018-05-16 14:49:20', '2018-05-16 14:49:20'),
(12, 1, 176, NULL, 'hello', 'test', 'true', '2018-05-25', '2018-05-25 04:17:45', '2018-05-25 04:17:45'),
(13, 45, 174, NULL, 'dfgdfg', 'dfgdfg', 'true', '2018-07-13', '2018-07-13 08:32:34', '2018-07-13 08:32:34'),
(14, 45, 173, NULL, 'dfsdg', 'dfgfgfd', 'true', '2018-07-16', '2018-07-16 03:09:36', '2018-07-16 03:09:36'),
(15, 45, 173, NULL, 'about product', 'this product very useful', 'true', '2018-07-28', '2018-07-28 07:12:19', '2018-07-28 07:12:19'),
(16, 45, 173, NULL, 'responsive', 'Responsiveness', 'true', '2018-08-06', '2018-08-06 06:37:24', '2018-08-06 06:37:24'),
(17, 1, 183, NULL, 'test', 'promote', 'true', '2018-08-09', '2018-08-09 21:56:06', '2018-08-09 21:56:06'),
(18, 70, 199, NULL, 'Tattoo Artist', '123', 'false', '2018-08-16', '2018-08-16 11:12:09', '2018-08-16 11:12:09'),
(19, 70, 197, NULL, 'delete', 'delete', 'true', '2018-08-16', '2018-08-16 11:12:56', '2018-08-16 11:12:56'),
(20, 79, 204, NULL, 'qqqq', 'qqqqq', 'false', '2018-08-16', '2018-08-16 12:43:05', '2018-08-16 12:43:05'),
(21, 81, 209, NULL, NULL, NULL, 'true', '2018-08-20', '2018-08-20 10:15:08', '2018-08-20 10:15:08'),
(22, 81, 209, NULL, 'Test', 'Test', 'true', '2018-08-20', '2018-08-20 10:16:48', '2018-08-20 10:16:48'),
(23, 70, 196, NULL, 'qqqq', 'qqqqqqq', 'true', '2018-08-21', '2018-08-21 13:08:04', '2018-08-21 13:08:04'),
(24, 80, 211, NULL, 'console.log(this.repoUrl);', 'console.log(this.repoUrl);', 'true', '2018-08-21', '2018-08-21 13:18:09', '2018-08-21 13:18:09'),
(25, 80, 210, NULL, 'console.log(this.repoUrl);', 'console.log(this.repoUrl);', 'true', '2018-08-21', '2018-08-21 13:18:13', '2018-08-21 13:18:13'),
(26, 81, 209, NULL, 'title', 'title message', 'true', '2018-08-27', '2018-08-27 09:30:11', '2018-08-27 09:30:11'),
(27, 81, 209, NULL, 'Message', 'Message', 'true', '2018-08-27', '2018-08-27 09:34:17', '2018-08-27 09:34:17'),
(28, 45, 173, NULL, 'ghjk', 'gijkgj', 'true', '2018-08-28', '2018-08-28 10:22:28', '2018-08-28 10:22:28'),
(29, 84, 226, NULL, 'Promote', 'These is ID dna id product', 'true', '2018-08-29', '2018-08-29 09:12:39', '2018-08-29 09:12:39'),
(30, 80, 225, NULL, 'dont write irrelevent', 'MESSAGE', 'true', '2018-08-29', '2018-08-29 10:03:29', '2018-08-29 10:03:29'),
(31, 92, 212, NULL, 'share', 'From buyer', 'true', '2018-09-03', '2018-09-03 10:26:45', '2018-09-03 10:26:45'),
(32, 92, 240, NULL, 'Share', 'share', 'true', '2018-09-03', '2018-09-03 13:20:12', '2018-09-03 13:20:12'),
(33, 91, 245, NULL, '1', 'First promotion of product', 'true', '2018-09-04', '2018-09-04 10:09:31', '2018-09-04 10:09:31'),
(34, 91, 247, NULL, 'Hi', 'Hi', 'true', '2018-09-04', '2018-09-04 10:15:44', '2018-09-04 10:15:44'),
(35, 1, 257, NULL, 'For promote', 'promotiona message', 'true', '2018-09-13', '2018-09-13 10:44:35', '2018-09-13 10:44:35'),
(36, 1, 257, NULL, 'this.loading = false;', 'this.loading = false;', 'true', '2018-09-13', '2018-09-13 10:45:40', '2018-09-13 10:45:40'),
(37, 1, 183, NULL, 'video', 'video preview', 'true', '2018-09-13', '2018-09-13 10:56:17', '2018-09-14 02:43:05'),
(38, 168, 269, NULL, 'asd', 'sadasdasdsd', 'true', '2018-11-11', '2018-11-11 10:05:31', '2018-11-11 10:05:31'),
(39, 1, 268, NULL, 'test', 'test', 'true', '2018-12-27', '2018-12-27 06:04:12', '2018-12-27 06:04:12'),
(40, 212, 276, NULL, 'Food', 'If more of us valued food and cheer and song above hoarded gold, it would be a merrier world', 'true', '2019-03-26', '2019-03-26 07:30:32', '2019-03-26 07:30:32'),
(41, 212, 277, NULL, 'Kids_love', 'After a good dinner one can forgive anybody, even one\'s own relations.', 'true', '2019-03-26', '2019-03-26 08:49:13', '2019-03-26 08:49:13'),
(42, 212, 279, NULL, 'entertainment', 'your message is entertainment', 'true', '2019-03-26', '2019-03-26 09:52:13', '2019-03-26 09:52:13'),
(43, 212, 280, NULL, 'science', 'science informations', 'true', '2019-03-26', '2019-03-26 10:15:59', '2019-03-26 10:15:59'),
(44, 212, 281, NULL, 'cosematics', 'product informations', 'true', '2019-03-26', '2019-03-26 10:26:25', '2019-03-26 10:26:25'),
(45, 220, 287, NULL, 'Cosmetic HD Bridal Mackup', 'Products Used :\nFace:\nMakeupRevolution Ultra Sculpt & Contour Kit ( Ultra Fair C01)\n\nMusic : Official by YOUTUBE\n??? THANK YOU SO MUCH FOR WATCHING ???\nthe Balm Balm Voyage Vol. 2 pallete\ntheBalm Mary Lou Manizer Luminizer', 'true', '2019-04-21', '2019-04-21 12:27:07', '2019-04-21 12:27:07'),
(46, 220, 288, NULL, 'Pop Rock Band', 'Pop rock band details', 'true', '2019-04-21', '2019-04-21 19:05:49', '2019-04-21 19:05:49');

-- --------------------------------------------------------

--
-- Table structure for table `purchased_products`
--

CREATE TABLE `purchased_products` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `cart_id` int(11) NOT NULL,
  `talent_id` int(11) NOT NULL,
  `title` varchar(225) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `unit_price` varchar(225) NOT NULL,
  `total_amount` int(11) DEFAULT NULL,
  `delete_flag` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchased_products`
--

INSERT INTO `purchased_products` (`id`, `user_id`, `cart_id`, `talent_id`, `title`, `quantity`, `unit_price`, `total_amount`, `delete_flag`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(25, 45, 2, 178, 'hindi song', 1, '21', 21, 1, NULL, NULL, '2018-07-11 05:41:38', '2018-07-11 05:41:38'),
(26, 45, 2, 173, 'dcd', 1, '33', 33, 1, NULL, NULL, '2018-07-11 05:41:48', '2018-07-11 05:41:48'),
(28, 45, 2, 173, 'dcd', 1, '33', 33, 1, NULL, NULL, '2018-07-13 13:23:58', '2018-07-13 13:23:58'),
(29, 45, 2, 178, 'hindi song', 1, '21', 21, 1, NULL, NULL, '2018-07-13 13:24:08', '2018-07-13 13:24:08'),
(30, 1, 3, 174, 'author gallery', 1, '40', 40, 0, NULL, NULL, '2018-07-27 13:17:12', '2018-07-27 13:17:12'),
(32, 45, 2, 178, 'hindi song', 1, '21', 21, 1, NULL, NULL, '2018-08-09 06:22:47', '2018-08-09 06:22:47'),
(33, 45, 2, 176, 'test-05', 1, '305', 305, 1, NULL, NULL, '2018-08-09 06:26:08', '2018-08-09 06:26:08'),
(34, 45, 2, 178, 'hindi song', 1, '21', 21, 1, NULL, NULL, '2018-08-09 06:26:29', '2018-08-09 06:26:29'),
(35, 45, 2, 176, 'test-05', 1, '305', 305, 1, NULL, NULL, '2018-08-09 06:27:25', '2018-08-09 06:27:25'),
(36, 63, 4, 180, 'bfbdfb', 1, '10', 10, 1, NULL, NULL, '2018-08-09 06:34:15', '2018-08-09 06:34:15'),
(37, 63, 4, 173, 'dcd', 1, '33', 33, 1, NULL, NULL, '2018-08-09 06:34:20', '2018-08-09 06:34:20'),
(38, 45, 2, 186, 'Entermaintent Test product 1', 3, '50', 150, 1, NULL, NULL, '2018-08-11 12:42:09', '2018-08-11 12:47:36'),
(39, 45, 2, 178, 'hindi song', 1, '21', 21, 1, NULL, NULL, '2018-08-14 05:49:10', '2018-08-14 05:49:10'),
(40, 45, 2, 178, 'hindi song', 1, '21', 21, 1, NULL, NULL, '2018-08-14 06:21:35', '2018-08-14 06:21:35'),
(42, 45, 2, 178, 'hindi song', 1, '21', 21, 1, NULL, NULL, '2018-08-14 10:34:22', '2018-08-14 10:34:22'),
(43, 45, 2, 187, 'Music File', 1, '100', 100, 1, NULL, NULL, '2018-08-14 10:35:00', '2018-08-14 10:35:00'),
(44, 45, 2, 176, 'test-05', 1, '305', 305, 1, NULL, NULL, '2018-08-14 10:37:26', '2018-08-14 10:37:26'),
(46, 63, 4, 198, 'Added by buyer', 1, '50', 50, 1, NULL, NULL, '2018-08-14 12:04:53', '2018-08-16 06:00:11'),
(47, 45, 2, 183, 'VIDEO PREVIEW', 1, '99', 99, 1, NULL, NULL, '2018-08-14 18:39:52', '2018-08-14 18:39:52'),
(49, 45, 2, 186, 'Entermaintent Test product 1', 1, '50', 50, 1, NULL, NULL, '2018-08-14 18:44:53', '2018-08-14 18:44:53'),
(52, 63, 4, 199, 'Science 2', 1, '1000000', 1000000, 0, NULL, NULL, '2018-08-16 13:37:13', '2018-08-16 13:37:13'),
(53, 63, 4, 205, 'author2', 1, '124654', 124654, 0, NULL, NULL, '2018-08-16 13:41:23', '2018-08-16 13:41:23'),
(54, 63, 4, 206, 'music5', 1, '45555', 45555, 0, NULL, NULL, '2018-08-16 13:43:49', '2018-08-16 13:43:49'),
(56, 63, 4, 207, 'Entertainment 2', 1, '10000', 10000, 0, NULL, NULL, '2018-08-17 09:09:25', '2018-08-17 09:09:25'),
(65, 45, 2, 197, 'Fitness', 1, '1', 1, 1, NULL, NULL, '2018-08-21 02:01:59', '2018-08-21 02:01:59'),
(69, 63, 4, 176, 'test-05', 1, '305', 305, 1, NULL, NULL, '2018-08-22 06:06:22', '2018-08-22 06:06:29'),
(80, 81, 5, 174, 'author gallery', 2, '40', 80, 1, NULL, NULL, '2018-08-30 09:14:15', '2018-08-30 09:45:55'),
(86, 90, 6, 212, 'Ipad Fitness', 1, '125', 125, 0, NULL, NULL, '2018-08-31 13:38:51', '2018-08-31 13:38:51'),
(87, 81, 5, 174, 'author gallery', 1, '40', 40, 1, NULL, NULL, '2018-09-03 09:42:01', '2018-09-03 09:42:01'),
(92, 92, 7, 176, 'test-05', 1, '305', 305, 1, NULL, NULL, '2018-09-03 12:50:39', '2018-09-03 12:50:39'),
(93, 81, 5, 240, 'Mailinator Fitness', 1, '20000', 20000, 1, NULL, NULL, '2018-09-07 10:09:43', '2018-09-07 10:09:43'),
(94, 45, 2, 235, 'My Music', 1, '23', 23, 1, NULL, NULL, '2018-09-09 02:59:04', '2018-09-09 02:59:04'),
(96, 96, 8, 203, 'aa', 1, '10000', 10000, 1, NULL, NULL, '2018-09-10 09:24:37', '2018-09-10 09:24:37'),
(100, 81, 5, 256, 'Science', 1, '111', 111, 1, NULL, NULL, '2018-09-10 09:47:28', '2018-09-10 09:47:28'),
(104, 81, 5, 243, 'Cosmetics Multiple items', 1, '-5000', -5000, 1, NULL, NULL, '2018-09-11 11:11:45', '2018-09-11 11:11:45'),
(106, 99, 9, 170, 'test1', 1, '454', 454, 0, NULL, NULL, '2018-09-11 12:46:33', '2018-09-11 12:46:33'),
(109, 45, 2, 235, 'My Music', 1, '23', 23, 1, NULL, NULL, '2018-09-13 09:06:17', '2018-09-13 09:06:17'),
(110, 81, 5, 203, 'aa', 1, '10000', 10000, 1, NULL, NULL, '2018-09-14 05:37:47', '2018-09-14 05:37:47'),
(112, 81, 5, 260, 'Music Ads', 1, '1200', 1200, 1, NULL, NULL, '2018-09-14 06:38:32', '2018-09-14 06:38:32'),
(137, 99, 9, 216, 'Food items', 1, '1200', 1200, 1, NULL, NULL, '2018-09-14 11:21:40', '2018-09-14 11:21:40'),
(138, 99, 9, 212, 'Mathematics', 1, '125', 125, 1, NULL, NULL, '2018-09-14 11:21:53', '2018-09-14 11:21:53'),
(140, 99, 9, 213, 'My Entertainment', 1, '1000', 1000, 1, NULL, NULL, '2018-09-14 11:22:08', '2018-09-14 11:22:08'),
(142, 81, 5, 260, 'Music Ads', 1, '1200', 1200, 1, NULL, NULL, '2018-09-14 11:45:37', '2018-09-14 11:45:37'),
(149, 45, 2, 173, 'dcd', 1, '33', 33, 0, NULL, NULL, '2018-09-14 12:20:51', '2018-09-14 12:20:51'),
(158, 45, 2, 204, 'aaaaa', 1, '12321324', 12321324, 1, NULL, NULL, '2018-09-14 12:51:03', '2018-09-14 12:51:03'),
(161, 92, 7, 203, 'aa', 1, '10000', 10000, 0, NULL, NULL, '2018-09-15 06:41:31', '2018-09-15 06:41:31'),
(165, 104, 10, 204, 'aaaaa', 1, '1232', 1232, 1, NULL, NULL, '2018-10-12 13:21:21', '2018-10-12 13:21:21'),
(166, 104, 10, 218, 'Entertainment 2', 1, '1230', 1230, 0, NULL, NULL, '2018-10-12 13:22:01', '2018-10-12 13:22:01'),
(167, 104, 10, 224, 'Fitness', 1, '12050', 12050, 1, NULL, NULL, '2018-10-12 13:24:43', '2018-10-12 13:24:43'),
(168, 104, 10, 204, 'aaaaa', 1, '1232', 1232, 0, NULL, NULL, '2018-10-12 13:34:43', '2018-10-12 13:34:43'),
(169, 113, 11, 234, 'test', 1, '12', 12, 0, NULL, NULL, '2018-10-15 10:19:52', '2018-10-15 10:19:52'),
(170, 113, 11, 173, 'dcd', 1, '33', 33, 0, NULL, NULL, '2018-10-15 10:20:28', '2018-10-15 10:20:28'),
(171, 104, 10, 234, 'test', 1, '12', 12, 1, NULL, NULL, '2018-10-22 10:18:11', '2018-10-22 10:18:11'),
(172, 104, 10, 235, 'My Music', 1, '23', 23, 0, NULL, NULL, '2018-10-22 10:19:21', '2018-10-22 10:19:21'),
(173, 181, 12, 250, 'I can make you laugh', 1, '1000', 1000, 1, NULL, NULL, '2018-12-04 14:15:02', '2018-12-04 14:15:02'),
(174, 181, 12, 173, 'dcd', 1, '33', 33, 1, NULL, NULL, '2018-12-04 14:42:57', '2018-12-04 14:42:57'),
(175, 184, 13, 235, 'My Music', 1, '23', 23, 1, NULL, NULL, '2018-12-15 14:13:30', '2018-12-15 14:13:30'),
(176, 185, 14, 234, 'test', 1, '12', 12, 0, NULL, NULL, '2018-12-27 05:37:54', '2018-12-27 05:37:54'),
(177, 183, 15, 235, 'My Music', 1, '23', 23, 1, NULL, NULL, '2019-01-16 11:17:10', '2019-01-16 11:17:10'),
(178, 183, 15, 235, 'My Music', 1, '23', 23, 1, NULL, NULL, '2019-01-16 11:19:13', '2019-01-16 11:19:13'),
(179, 189, 16, 272, 'music system', 1, '10', 10, 1, NULL, NULL, '2019-01-22 08:49:46', '2019-01-22 08:49:46'),
(180, 183, 15, 234, 'test', 1, '12', 12, 1, NULL, NULL, '2019-02-08 05:47:58', '2019-02-08 05:47:58'),
(181, 183, 15, 273, 'aa', 1, '100', 100, 1, NULL, NULL, '2019-02-08 06:21:46', '2019-02-08 06:21:46'),
(182, 183, 15, 250, 'I can make you laugh', 1, '1000', 1000, 1, NULL, NULL, '2019-03-15 12:33:33', '2019-03-15 12:33:33'),
(185, 183, 15, 173, 'dcd', 1, '33', 33, 1, NULL, NULL, '2019-03-22 06:08:03', '2019-03-22 06:08:03'),
(186, 183, 15, 234, 'test', 1, '12', 12, 0, NULL, NULL, '2019-03-22 07:32:52', '2019-03-22 07:32:52'),
(187, 202, 18, 234, 'test', 1, '12', 12, 0, NULL, NULL, '2019-03-23 06:23:44', '2019-03-23 06:23:44'),
(188, 198, 19, 173, 'dcd', 1, '33', 33, 0, NULL, NULL, '2019-03-23 07:07:41', '2019-03-23 07:07:41'),
(189, 204, 20, 236, 'wild life photography', 1, '44', 44, 0, NULL, NULL, '2019-03-23 19:07:18', '2019-03-23 19:07:18'),
(190, 205, 21, 272, 'music system', 1, '10', 10, 0, NULL, NULL, '2019-03-24 18:04:55', '2019-03-24 18:04:55'),
(191, 211, 22, 234, 'test', 1, '12', 12, 0, NULL, NULL, '2019-03-26 06:59:13', '2019-03-26 06:59:13'),
(192, 211, 22, 173, 'dcd', 1, '33', 33, 0, NULL, NULL, '2019-03-26 09:05:53', '2019-03-26 09:05:53'),
(193, 211, 22, 276, 'food is first love', 1, '300', 300, 0, NULL, NULL, '2019-03-26 09:27:12', '2019-03-26 09:27:12'),
(194, 211, 22, 274, 'Music is "love" is music', 1, '250', 250, 0, NULL, NULL, '2019-03-26 09:30:02', '2019-03-26 09:30:02'),
(195, 214, 23, 234, 'test', 1, '12', 12, 1, NULL, NULL, '2019-03-26 09:44:57', '2019-03-26 09:44:57'),
(196, 211, 22, 236, 'wild life photography', 1, '44', 44, 0, NULL, NULL, '2019-03-26 09:58:52', '2019-03-26 09:58:52'),
(197, 216, 24, 273, 'aa', 1, '100', 100, 0, NULL, NULL, '2019-03-26 11:07:40', '2019-03-26 11:07:40'),
(198, 216, 24, 274, 'Music is "love" is music', 1, '250', 250, 1, NULL, NULL, '2019-03-26 11:11:37', '2019-03-26 11:11:37'),
(199, 217, 25, 173, 'dcd', 1, '33', 33, 1, NULL, NULL, '2019-03-26 19:50:19', '2019-03-26 19:50:19'),
(200, 214, 23, 272, 'music system', 1, '10', 10, 1, NULL, NULL, '2019-03-27 05:17:46', '2019-03-27 05:17:46'),
(201, 181, 12, 173, 'dcd', 1, '33', 33, 1, NULL, NULL, '2019-03-27 07:47:52', '2019-03-27 07:47:52'),
(202, 125, 26, 272, 'music system', 1, '10', 10, 0, NULL, NULL, '2019-03-29 06:50:39', '2019-03-29 06:50:39'),
(205, 217, 25, 234, 'test', 1, '12', 12, 1, NULL, NULL, '2019-04-04 17:41:11', '2019-04-04 17:41:11'),
(206, 217, 25, 234, 'test', 1, '12', 12, 1, NULL, NULL, '2019-04-04 17:54:05', '2019-04-04 17:54:05'),
(207, 181, 12, 272, 'music system', 1, '10', 10, 0, NULL, NULL, '2019-04-05 06:59:10', '2019-04-05 06:59:10'),
(210, 227, 27, 234, 'test', 1, '12', 12, 1, NULL, NULL, '2019-04-11 18:40:49', '2019-04-11 18:40:49'),
(219, 217, 25, 234, 'test', 1, '12', 12, 1, NULL, NULL, '2019-04-14 17:52:38', '2019-04-14 17:52:38'),
(221, 197, 17, 288, 'Pop Music Band', 1, '500', 500, 0, NULL, NULL, '2019-06-11 18:13:38', '2019-06-11 18:13:38');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permission` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `permission`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'all', '2017-11-07 03:05:01', '2017-11-07 03:05:01'),
(2, 'manager', 'read,write', '2017-11-07 03:05:01', '2017-11-07 03:05:01'),
(3, 'buyer', 'read,write', '2017-11-07 03:05:01', '2017-11-07 03:05:01'),
(4, 'seller', 'read,write', '2017-11-07 03:05:01', '2017-11-07 03:05:01');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` int(11) NOT NULL,
  `talent_id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sale_statuses`
--

CREATE TABLE `sale_statuses` (
  `id` int(11) NOT NULL,
  `status` varchar(225) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sample_media`
--

CREATE TABLE `sample_media` (
  `id` int(11) NOT NULL,
  `talent_id` int(11) NOT NULL,
  `video_name` varchar(225) NOT NULL,
  `path_name` varchar(225) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sample_media`
--

INSERT INTO `sample_media` (`id`, `talent_id`, `video_name`, `path_name`, `created_at`, `updated_at`) VALUES
(1, 180, 'step.mov', 'uploads/seller-video/180-step.mov', '2018-08-02 16:52:03', '2018-08-02 16:52:03'),
(2, 182, 'mov_bbb.mp4', 'uploads/seller-video/182-mov_bbb.mp4', '2018-08-06 13:39:01', '2018-08-06 13:39:01'),
(3, 182, 's9qgz-g5flo.mp4', 'uploads/seller-video/182-s9qgz-g5flo.mp4', '2018-08-06 13:41:51', '2018-08-06 13:41:51'),
(4, 184, '15MG.mp4', 'uploads/seller-video/184-15MG.mp4', '2018-08-09 09:23:53', '2018-08-09 09:23:53'),
(5, 185, '15MG.mp4', 'uploads/seller-video/185-15MG.mp4', '2018-08-09 10:43:53', '2018-08-09 10:43:53'),
(6, 186, '15MG.mp4', 'uploads/seller-video/186-15MG.mp4', '2018-08-10 13:26:49', '2018-08-10 13:26:49'),
(7, 187, '15MG.mp4', 'uploads/seller-video/187-15MG.mp4', '2018-08-10 13:41:55', '2018-08-10 13:41:55'),
(8, 188, '15MG.mp4', 'uploads/seller-video/188-15MG.mp4', '2018-08-10 14:15:56', '2018-08-10 14:15:56'),
(9, 189, 'cat 30 sec.mp4', 'uploads/seller-video/189-cat 30 sec.mp4', '2018-08-11 10:14:13', '2018-08-11 10:14:13'),
(10, 190, 'cat 30 sec.mp4', 'uploads/seller-video/190-cat 30 sec.mp4', '2018-08-11 10:19:16', '2018-08-11 10:19:16'),
(11, 191, 'animation.mp4', 'uploads/seller-video/191-animation.mp4', '2018-08-11 10:38:11', '2018-08-11 10:38:11'),
(12, 192, 'animation.mp4', 'uploads/seller-video/192-animation.mp4', '2018-08-11 12:31:50', '2018-08-11 12:31:50'),
(13, 193, 'cat 30 sec.mp4', 'uploads/seller-video/193-cat 30 sec.mp4', '2018-08-11 12:32:34', '2018-08-11 12:32:34'),
(14, 194, 'cat 30 sec.mp4', 'uploads/seller-video/194-cat 30 sec.mp4', '2018-08-11 12:33:11', '2018-08-11 12:33:11'),
(15, 195, '15MG.mp4', 'uploads/seller-video/195-15MG.mp4', '2018-08-11 12:35:24', '2018-08-11 12:35:24'),
(16, 196, '15MG.mp4', 'uploads/seller-video/196-15MG.mp4', '2018-08-11 12:39:09', '2018-08-11 12:39:09'),
(17, 197, 'cat 30 sec.mp4', 'uploads/seller-video/197-cat 30 sec.mp4', '2018-08-11 12:42:08', '2018-08-11 12:42:08'),
(18, 198, 'cat 30 sec.mp4', 'uploads/seller-video/198-cat 30 sec.mp4', '2018-08-14 06:20:46', '2018-08-14 06:20:46'),
(19, 199, 'cat 30 sec.mp4', 'uploads/seller-video/199-cat 30 sec.mp4', '2018-08-16 11:09:07', '2018-08-16 11:09:07'),
(20, 200, '15MG.mp4', 'uploads/seller-video/200-15MG.mp4', '2018-08-16 11:19:13', '2018-08-16 11:19:13'),
(21, 202, '15MG.mp4', 'uploads/seller-video/202-15MG.mp4', '2018-08-16 11:57:28', '2018-08-16 11:57:28'),
(22, 203, 'animation.mp4', 'uploads/seller-video/203-animation.mp4', '2018-08-16 11:59:14', '2018-08-16 11:59:14'),
(23, 204, 'animation.mp4', 'uploads/seller-video/204-animation.mp4', '2018-08-16 12:43:05', '2018-08-16 12:43:05'),
(24, 205, '15MG.mp4', 'uploads/seller-video/205-15MG.mp4', '2018-08-16 13:40:51', '2018-08-16 13:40:51'),
(25, 206, '15MG.mp4', 'uploads/seller-video/206-15MG.mp4', '2018-08-16 13:43:30', '2018-08-16 13:43:30'),
(26, 207, '15MG.mp4', 'uploads/seller-video/207-15MG.mp4', '2018-08-17 09:08:33', '2018-08-17 09:08:33'),
(27, 208, '15MG.mp4', 'uploads/seller-video/208-15MG.mp4', '2018-08-17 11:54:26', '2018-08-17 11:54:26'),
(28, 209, '182-s9qgz-g5flo.mp4', 'uploads/seller-video/209-182-s9qgz-g5flo.mp4', '2018-08-22 07:27:18', '2018-08-22 07:27:18'),
(29, 212, '182-s9qgz-g5flo.mp4', 'uploads/seller-video/212-182-s9qgz-g5flo.mp4', '2018-08-22 11:25:20', '2018-08-22 11:25:20'),
(30, 222, '182-s9qgz-g5flo.mp4', 'uploads/seller-video/222-182-s9qgz-g5flo.mp4', '2018-08-27 09:54:34', '2018-08-27 09:54:34'),
(31, 224, '55714760752__58E58D5B-1B83-4477-8DBA-730482DEEE9F.MOV', 'uploads/seller-video/224-55714760752__58E58D5B-1B83-4477-8DBA-730482DEEE9F.MOV', '2018-08-28 11:14:21', '2018-08-28 11:14:21'),
(32, 225, '55714772948__73FB8023-8987-47B0-8312-FA4FBBD22EAB.MOV', 'uploads/seller-video/225-55714772948__73FB8023-8987-47B0-8312-FA4FBBD22EAB.MOV', '2018-08-28 11:17:21', '2018-08-28 11:17:21'),
(33, 230, 'SampleVideo_1280x720_5mb.mkv', 'uploads/seller-video/230-SampleVideo_1280x720_5mb.mkv', '2018-08-29 10:12:40', '2018-08-29 10:12:40'),
(34, 231, 'SampleVideo_1280x720_5mb.mkv', 'uploads/seller-video/231-SampleVideo_1280x720_5mb.mkv', '2018-08-29 10:16:03', '2018-08-29 10:16:03'),
(35, 232, 'SampleVideo_1280x720_5mb.mkv', 'uploads/seller-video/232-SampleVideo_1280x720_5mb.mkv', '2018-08-29 10:19:26', '2018-08-29 10:19:26'),
(36, 233, 'step.mov', 'uploads/seller-video/233-step.mov', '2018-08-29 10:23:42', '2018-08-29 10:23:42'),
(37, 234, 'SampleVideo_1280x720_5mb.mkv', 'uploads/seller-video/234-SampleVideo_1280x720_5mb.mkv', '2018-08-29 10:25:52', '2018-08-29 10:25:52'),
(38, 235, '202-file_example_MP3_700KB.mp3', 'uploads/seller-video/235-202-file_example_MP3_700KB.mp3', '2018-08-29 10:35:26', '2018-08-29 10:35:26'),
(39, 236, 'SampleVideo_1280x720_5mb.mkv', 'uploads/seller-video/236-SampleVideo_1280x720_5mb.mkv', '2018-08-29 10:38:14', '2018-08-29 10:38:14'),
(40, 181, 'SampleVideo_1280x720_5mb.mkv', 'uploads/seller-video/181-SampleVideo_1280x720_5mb.mkv', '2018-08-29 10:55:59', '2018-08-29 10:55:59'),
(41, 183, 'SampleVideo_1280x720_5mb.mkv', 'uploads/seller-video/183-SampleVideo_1280x720_5mb.mkv', '2018-08-29 11:02:11', '2018-08-29 11:02:11'),
(42, 237, 'SampleVideo_1280x720_5mb.mkv', 'uploads/seller-video/237-SampleVideo_1280x720_5mb.mkv', '2018-08-29 11:11:35', '2018-08-29 11:11:35'),
(43, 238, 'step.mov', 'uploads/seller-video/238-step.mov', '2018-08-29 11:12:45', '2018-08-29 11:12:45'),
(44, 240, 'SampleAudio_0.4mb.mp3', 'uploads/seller-video/240-SampleAudio_0.4mb.mp3', '2018-08-31 09:36:45', '2018-08-31 09:36:45'),
(45, 242, '182-s9qgz-g5flo.mp4', 'uploads/seller-video/242-182-s9qgz-g5flo.mp4', '2018-08-31 11:41:57', '2018-08-31 11:41:57'),
(46, 243, '182-s9qgz-g5flo.mp4', 'uploads/seller-video/243-182-s9qgz-g5flo.mp4', '2018-09-03 06:02:51', '2018-09-03 06:02:51'),
(47, 244, 'ogv.ogv', 'uploads/seller-video/244-ogv.ogv', '2018-09-03 06:25:30', '2018-09-03 06:25:30'),
(48, 245, '202-file_example_MP3_700KB.mp3', 'uploads/seller-video/245-202-file_example_MP3_700KB.mp3', '2018-09-03 08:52:08', '2018-09-03 08:52:08'),
(49, 246, 'video2.mp4', 'uploads/seller-video/246-video2.mp4', '2018-09-03 11:17:09', '2018-09-03 11:17:09'),
(50, 249, 'SampleAudio_0.7mb.mp3', 'uploads/seller-video/249-SampleAudio_0.7mb.mp3', '2018-09-07 11:45:14', '2018-09-07 11:45:14'),
(51, 248, '182-s9qgz-g5flo.mp4', 'uploads/seller-video/248-182-s9qgz-g5flo.mp4', '2018-09-08 09:37:22', '2018-09-08 09:37:22'),
(52, 223, '182-s9qgz-g5flo.mp4', 'uploads/seller-video/223-182-s9qgz-g5flo.mp4', '2018-09-08 09:52:22', '2018-09-08 09:52:22'),
(53, 218, 'SampleAudio_0.4mb.mp3', 'uploads/seller-video/218-SampleAudio_0.4mb.mp3', '2018-09-08 10:17:17', '2018-09-08 10:17:17'),
(54, 250, 'Where promote your new song or talent- - Go To www.FutureStarr.com ---- FutureStarr----.mp4', 'uploads/seller-video/250-Where promote your new song or talent- - Go To www.FutureStarr.com ---- FutureStarr----.mp4', '2018-09-09 03:05:12', '2018-09-09 03:05:12'),
(55, 256, 'SampleVideo_1280x720_5mb.mkv', 'uploads/seller-video/256-SampleVideo_1280x720_5mb.mkv', '2018-09-10 07:09:38', '2018-09-10 07:09:38'),
(56, 254, 'Mov.mov', 'uploads/seller-video/254-Mov.mov', '2018-09-10 07:14:53', '2018-09-10 07:14:53'),
(57, 213, 'flv.flv', 'uploads/seller-video/213-flv.flv', '2018-09-10 09:00:45', '2018-09-10 09:00:45'),
(58, 257, 'flv.flv', 'uploads/seller-video/257-flv.flv', '2018-09-10 10:36:54', '2018-09-10 10:36:54'),
(59, 259, 'mpeg.mp4', 'uploads/seller-video/259-mpeg.mp4', '2018-09-11 06:39:08', '2018-09-11 06:39:08'),
(60, 260, 'mpeg.mp4', 'uploads/seller-video/260-mpeg.mp4', '2018-09-11 12:18:15', '2018-09-11 12:18:15'),
(61, 261, 'Mov.mov', 'uploads/seller-video/261-Mov.mov', '2018-09-12 11:47:48', '2018-09-12 11:47:48'),
(62, 262, 'Mov.mov', 'uploads/seller-video/262-Mov.mov', '2018-09-14 06:50:03', '2018-09-14 06:50:03'),
(63, 263, 'Ranbuild22.mov', 'uploads/seller-video/263-Ranbuild22.mov', '2018-09-14 08:57:19', '2018-09-14 08:57:19'),
(64, 271, 'ARTIC-TOP-SHELF-MASTER.mp3', 'uploads/seller-video/271-ARTIC-TOP-SHELF-MASTER.mp3', '2018-11-12 00:12:59', '2018-11-12 00:12:59'),
(65, 274, 'SampleAudio_0.4mb.mp3', 'uploads/seller-video/274-SampleAudio_0.4mb.mp3', '2019-03-26 06:39:15', '2019-03-26 06:39:15'),
(66, 275, 'SampleVideo_1280x720_5mb.mp4', 'uploads/seller-video/275-SampleVideo_1280x720_5mb.mp4', '2019-03-26 07:06:09', '2019-03-26 07:06:09'),
(67, 276, 'SampleVideo_1280x720_2mb.mp4', 'uploads/seller-video/276-SampleVideo_1280x720_2mb.mp4', '2019-03-26 07:30:08', '2019-03-26 07:30:08'),
(68, 277, 'SampleAudio_0.7mb.mp3', 'uploads/seller-video/277-SampleAudio_0.7mb.mp3', '2019-03-26 08:45:59', '2019-03-26 08:45:59'),
(69, 278, 'SampleVideo_1280x720_2mb.mp4', 'uploads/seller-video/278-SampleVideo_1280x720_2mb.mp4', '2019-03-26 09:39:23', '2019-03-26 09:39:23'),
(70, 279, 'SampleVideo_1280x720_2mb.mp4', 'uploads/seller-video/279-SampleVideo_1280x720_2mb.mp4', '2019-03-26 09:41:26', '2019-03-26 09:41:26'),
(71, 280, 'SampleVideo_1280x720_2mb.mp4', 'uploads/seller-video/280-SampleVideo_1280x720_2mb.mp4', '2019-03-26 10:03:33', '2019-03-26 10:03:33'),
(72, 281, 'SampleVideo_1280x720_2mb.mp4', 'uploads/seller-video/281-SampleVideo_1280x720_2mb.mp4', '2019-03-26 10:26:09', '2019-03-26 10:26:09'),
(73, 284, 'SampleVideo_1280x720_2mb.mp4', 'uploads/seller-video/284-SampleVideo_1280x720_2mb.mp4', '2019-03-26 11:33:52', '2019-03-26 11:33:52'),
(74, 286, 'hale_bopp_1.mpg', 'uploads/seller-video/286-hale_bopp_1.mpg', '2019-04-10 18:22:34', '2019-04-10 18:22:34'),
(75, 287, 'Smokey Eye Makeup Video Free Download.mp4', 'uploads/seller-video/287-Smokey Eye Makeup Video Free Download.mp4', '2019-04-21 12:26:10', '2019-04-21 12:26:10'),
(76, 291, 'videohive-10668306-super-stylish-model-dancing-and-posing-6 (online-video-cutter.com).mp4', 'uploads/seller-video/291-videohive-10668306-super-stylish-model-dancing-and-posing-6 (online-video-cutter.com).mp4', '2019-04-23 17:17:48', '2019-04-23 17:17:48');

-- --------------------------------------------------------

--
-- Table structure for table `seller_audios`
--

CREATE TABLE `seller_audios` (
  `id` int(11) NOT NULL,
  `talent_id` int(11) NOT NULL,
  `audio_name` varchar(225) NOT NULL,
  `audio_path` varchar(225) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `seller_audios`
--

INSERT INTO `seller_audios` (`id`, `talent_id`, `audio_name`, `audio_path`, `created_at`, `updated_at`) VALUES
(2, 71, 'dart.video-3.mp3', 'uploads/seller-audio/dart.video-3.mp3', '2018-01-01 08:10:08', '2018-01-01 08:10:08'),
(3, 75, 'dart.video-3.mp3', 'uploads/seller-audio/dart.video-3.mp3', '2018-01-01 08:17:07', '2018-01-01 08:17:07'),
(4, 89, 'dart.video-3.mp3', 'uploads/seller-audio/dart.video-3.mp3', '2018-01-12 15:05:51', '2018-01-12 15:05:51'),
(5, 97, 'dart.video-3.mp3', 'http://192.168.1.194/futurestarr-backend/storage/uploads/seller-audio/dart.video-3.mp3', '2018-01-16 11:01:21', '2018-01-16 11:01:21');

-- --------------------------------------------------------

--
-- Table structure for table `seller_contacts`
--

CREATE TABLE `seller_contacts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `username` varchar(225) NOT NULL,
  `email` varchar(225) NOT NULL,
  `description` varchar(225) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `seller_inboxes`
--

CREATE TABLE `seller_inboxes` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `talent_id` int(11) NOT NULL,
  `name` varchar(225) DEFAULT NULL,
  `message` varchar(225) DEFAULT NULL,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `posted_by` int(11) NOT NULL,
  `active` varchar(225) NOT NULL DEFAULT 'true',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `seller_inboxes`
--

INSERT INTO `seller_inboxes` (`id`, `user_id`, `talent_id`, `name`, `message`, `time`, `posted_by`, `active`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Gslimz Dalink', 'Lorem Ipsum is simply dummy text of the printing and type setting', '2017-12-08 10:10:00', 1, 'true', '2017-12-07 18:30:00', '2017-12-08 10:01:35'),
(2, 1, 1, 'Anurag', 'Hi, This is test message', '2018-01-12 11:08:34', 1, 'true', '2018-01-12 05:38:34', '2018-01-12 05:38:34');

-- --------------------------------------------------------

--
-- Table structure for table `seller_likes`
--

CREATE TABLE `seller_likes` (
  `id` int(11) NOT NULL,
  `talent_id` int(11) NOT NULL,
  `liked_by` int(11) NOT NULL,
  `talent_like` varchar(225) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `upadted_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `seller_plans`
--

CREATE TABLE `seller_plans` (
  `id` int(11) NOT NULL,
  `talent_id` int(11) DEFAULT NULL,
  `plan_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `end_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `expired` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1=Expired, 0=Not expired'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `seller_plans`
--

INSERT INTO `seller_plans` (`id`, `talent_id`, `plan_id`, `user_id`, `start_date`, `end_date`, `created_by`, `updated_by`, `created_at`, `updated_at`, `expired`) VALUES
(1, NULL, 2, 1, '2018-09-10 12:26:04', '2018-10-09 12:26:04', 1, 1, '2018-09-04 13:30:03', '2018-09-14 13:21:06', 0),
(2, NULL, 2, 80, '2018-09-07 09:04:21', '2018-10-07 09:04:21', 80, 80, '2018-09-05 06:12:21', '2018-09-07 09:04:21', 0),
(3, NULL, 1, 91, '2018-09-05 06:36:27', '2018-10-05 06:36:27', 91, 91, '2018-09-05 06:36:27', '2018-09-05 06:36:27', 0),
(4, NULL, 3, 79, '2018-09-07 09:53:51', '2018-10-07 09:53:51', 79, 79, '2018-09-07 09:53:51', '2018-09-07 10:05:30', 0),
(5, NULL, 2, 85, '2018-09-08 11:44:38', '2018-10-08 11:44:38', 85, 85, '2018-09-08 11:44:38', '2018-09-08 12:06:21', 0),
(6, NULL, 1, 70, '2018-09-10 09:59:17', '2018-10-10 09:59:17', 70, 70, '2018-09-10 09:59:17', '2018-09-10 09:59:17', 0),
(7, NULL, 2, 98, '2018-09-11 05:37:07', '2018-10-11 05:37:07', 98, 98, '2018-09-11 05:37:07', '2018-09-15 08:21:06', 0),
(8, NULL, 1, 100, '2018-09-14 13:44:54', '2018-10-14 13:44:54', 100, 100, '2018-09-14 13:44:54', '2018-09-14 13:44:54', 0);

-- --------------------------------------------------------

--
-- Table structure for table `seller_sales`
--

CREATE TABLE `seller_sales` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `daily_sales` varchar(225) NOT NULL,
  `daily_revenue` varchar(225) NOT NULL,
  `purchased_by` int(11) NOT NULL,
  `sales_status_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `shipping_addresses`
--

CREATE TABLE `shipping_addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zipcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `social_accounts`
--

CREATE TABLE `social_accounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `facebook_link` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `insta_link` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `social_accounts`
--

INSERT INTO `social_accounts` (`id`, `user_id`, `facebook_link`, `twitter_link`, `insta_link`, `created_at`, `updated_at`) VALUES
(6, 1, 'https://www.facebook.com/FutureStarrcom/', 'https://twitter.com/futurestarrcom?lang=en', 'https://www.instagram.com/futurestarr2012/', '2018-01-02 01:00:19', '2018-01-02 01:00:19'),
(7, 1, NULL, NULL, NULL, '2018-02-06 08:18:21', '2018-02-06 08:18:21'),
(8, 1, NULL, NULL, NULL, '2018-02-06 08:18:36', '2018-02-06 08:18:36'),
(9, 1, NULL, NULL, NULL, '2018-02-06 08:18:44', '2018-02-06 08:18:44'),
(10, 1, NULL, NULL, NULL, '2018-02-06 08:19:24', '2018-02-06 08:19:24'),
(11, 1, NULL, NULL, NULL, '2018-02-06 08:20:56', '2018-02-06 08:20:56'),
(12, 1, NULL, NULL, NULL, '2018-02-06 08:23:50', '2018-02-06 08:23:50'),
(13, 1, NULL, NULL, NULL, '2018-02-06 08:23:59', '2018-02-06 08:23:59'),
(14, 45, NULL, NULL, NULL, '2018-08-06 06:16:40', '2018-08-06 06:16:40'),
(15, 45, NULL, NULL, NULL, '2018-08-06 06:16:59', '2018-08-06 06:16:59'),
(38, 163, 'StoltingMediaGroup', 'stoltingmedia', 'stoltingmediagroup', '2018-10-31 17:48:27', '2018-10-31 17:48:27'),
(39, 163, NULL, NULL, NULL, '2018-10-31 17:48:35', '2018-10-31 17:48:35'),
(40, 163, NULL, NULL, NULL, '2018-10-31 17:49:22', '2018-10-31 17:49:22');

-- --------------------------------------------------------

--
-- Table structure for table `social_buzzs`
--

CREATE TABLE `social_buzzs` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `comment` varchar(2042) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `product_link` varchar(225) DEFAULT NULL,
  `product_name` varchar(225) DEFAULT NULL,
  `product_img_path` varchar(225) DEFAULT NULL,
  `report` int(11) NOT NULL DEFAULT '1',
  `active` int(11) NOT NULL DEFAULT '1',
  `posted_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `social_buzzs`
--

INSERT INTO `social_buzzs` (`id`, `user_id`, `category_id`, `comment`, `product_link`, `product_name`, `product_img_path`, `report`, `active`, `posted_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(87, 80, 1, 'Video Streaming', NULL, '1535365059_182-s9qgz-g5flo.mp4', 'uploads/social-buzz/1535365059_182-s9qgz-g5flo.mp4', 0, 1, 80, 80, '2018-08-27 10:17:03', '2018-08-27 10:17:03'),
(88, 45, 1, 'http://futurestarr.com/future_star/social-buzz', 'http://futurestarr.com/future_star/social-buzz', NULL, NULL, 0, 1, 45, 45, '2018-08-28 10:29:26', '2018-08-28 10:29:26'),
(89, 80, 1, 'Product link for seller', 'http://futurestarr.com/future_star/talent-mall/product-info/212', '1535550341_National Geography.jpg', 'uploads/social-buzz/1535550341_National Geography.jpg', 0, 1, 80, 80, '2018-08-29 13:45:38', '2018-08-29 13:45:38'),
(90, 81, 12, 'New😁', 'http://futurestarr.com/future_star/talent-mall/product-info/174', '1535621414_National Geography.jpg', 'uploads/social-buzz/1535621414_National Geography.jpg', 1, 1, 81, 81, '2018-08-30 09:30:11', '2018-08-30 09:30:11'),
(91, 81, 12, 'Nutriotion', NULL, '1535621464_nutrition.jpg', 'uploads/social-buzz/1535621464_nutrition.jpg', 1, 1, 81, 81, '2018-08-30 09:30:58', '2018-08-30 09:30:58'),
(92, 80, 6, '😌😀😆🙀😈😈😈😈😈😀😆🙀😈😀😆🙀😈😀😃sellerIpad upload socal buzz', NULL, '1535982873_video2.mp4', 'uploads/social-buzz/1535982873_video2.mp4', 0, 1, 80, 80, '2018-09-03 13:54:31', '2018-09-03 13:54:31'),
(93, 1, 6, 'New FutureStarr coming soon! 😁', NULL, '1536030957_Where promote your new song or talent- - Go To www.FutureStarr.com ---- FutureStarr----.mp4', 'uploads/social-buzz/1536030957_Where promote your new song or talent- - Go To www.FutureStarr.com ---- FutureStarr----.mp4', 1, 1, 1, 1, '2018-09-04 03:15:56', '2018-09-04 03:15:56'),
(94, 70, 1, 'hiii', NULL, NULL, NULL, 0, 1, 70, 70, '2018-09-10 10:41:50', '2018-09-10 10:41:50'),
(95, 113, 13, 'Nothing', NULL, NULL, NULL, 0, 1, 113, 113, '2018-10-15 11:07:06', '2018-10-15 11:07:06'),
(96, 113, 9, 'qwe', NULL, '1539604386_science fiction.jpeg', 'uploads/social-buzz/1539604386_science fiction.jpeg', 1, 1, 113, 113, '2018-10-15 11:52:59', '2018-10-15 11:52:59'),
(97, 181, 1, 'Does anyone on here have any new products for sell?', NULL, NULL, NULL, 1, 1, 181, 181, '2018-11-24 03:00:26', '2018-11-24 03:00:26'),
(98, 185, 4, 'I like this new product', 'https://www.futurestarr.com/talent-mall/product-info/272', NULL, NULL, 1, 1, 185, 185, '2018-12-27 05:39:59', '2018-12-27 05:39:59'),
(99, 1, 1, 'Purchase my product here.', 'https://www.futurestarr.com/talent-mall/product-info/272', NULL, NULL, 1, 1, 1, 1, '2018-12-27 06:06:08', '2018-12-27 06:06:08'),
(100, 1, 1, 'I have new product', NULL, '1545890814_Club vegas.PNG', 'uploads/social-buzz/1545890814_Club vegas.PNG', 1, 1, 1, 1, '2018-12-27 06:06:53', '2018-12-27 06:06:53'),
(101, 1, 4, 'See my new music', NULL, '1545890847_videoplayback.mp4', 'uploads/social-buzz/1545890847_videoplayback.mp4', 1, 1, 1, 1, '2018-12-27 06:07:16', '2018-12-27 06:07:16'),
(102, 1, 4, 'Check out my new video', NULL, '1545891160_videoplayback.mp4', 'uploads/social-buzz/1545891160_videoplayback.mp4', 1, 1, 1, 1, '2018-12-27 06:12:30', '2018-12-27 06:12:30'),
(103, 1, 4, 'Purchase my video', 'https://www.futurestarr.com/talent-mall/product-info/272', NULL, NULL, 1, 1, 1, 1, '2018-12-27 06:13:14', '2018-12-27 06:13:14'),
(104, 183, 9, 'ssq', NULL, '1550131491_training.png', 'uploads/social-buzz/1550131491_training.png', 1, 1, 183, 183, '2019-02-14 08:04:50', '2019-02-14 08:04:50'),
(105, 197, 4, 'You guys. I really like this song. You should check out their link and purchase it.', 'https://php10.shaligraminfotech.com/futurestarr/talent-mall/product-info/272', '1552721099_Future Starr.PNG', 'uploads/social-buzz/1552721099_Future Starr.PNG', 1, 1, 197, 197, '2019-03-16 07:24:57', '2019-03-16 07:24:57'),
(106, 183, 5, 'Test', 'https://www.google.com/logos/doodles/2019/celebrating-johann-sebastian-bach-5702425880035328.3-s.png', NULL, NULL, 1, 1, 183, 183, '2019-03-22 09:04:56', '2019-03-22 09:04:56'),
(107, 183, 8, 'This is a test of main site', 'https://www.google.com/search?q=image&rlz=1C1CHBD_enIN796IN796&tbm=isch&source=iu&ictx=1&fir=A6JJqffgz3xzlM%253A%252CpFs_4Fcq5AgpmM%252C%252Fm%252F0jg24&vet=1&usg=AI4_-kStnHiKDbMRDo0Xa5QgWDtYbRLCkQ&sa=X&ved=2ahUKEwjdnoLZ5JXhA', NULL, NULL, 1, 1, 183, 183, '2019-03-22 12:50:39', '2019-03-22 12:50:39'),
(108, 211, 11, 'food', 'https://prnt.sc/n31fn6', '1553593377_casserole-dish-2776735__340.jpg', 'uploads/social-buzz/1553593377_casserole-dish-2776735__340.jpg', 1, 1, 211, 211, '2019-03-26 09:42:55', '2019-03-26 09:42:55'),
(109, 211, 12, 'food food food', 'https://pixabay.com/images/search/nutrition/', '1553594207_eat-321671__340.jpg', 'uploads/social-buzz/1553594207_eat-321671__340.jpg', 0, 1, 211, 211, '2019-03-26 09:56:45', '2019-03-26 09:56:45'),
(110, 212, 5, 'music', 'https://www.facebook.com/', '1553597192_smoothie-3697014__340.jpg', 'uploads/social-buzz/1553597192_smoothie-3697014__340.jpg', 1, 1, 212, 212, '2019-03-26 10:46:14', '2019-03-26 10:46:14'),
(111, 212, 5, 'music', NULL, NULL, NULL, 1, 1, 212, 212, '2019-03-26 10:46:22', '2019-03-26 10:46:22'),
(112, 212, 5, 'music', 'https://www.facebook.com/', NULL, NULL, 1, 1, 212, 212, '2019-03-26 10:46:32', '2019-03-26 10:46:32'),
(113, 216, 1, 'bbb', NULL, NULL, NULL, 1, 1, 216, 216, '2019-03-26 11:14:49', '2019-03-26 11:14:49'),
(114, 216, 1, 'bbb', NULL, NULL, NULL, 1, 1, 216, 216, '2019-03-26 11:15:00', '2019-03-26 11:15:00'),
(115, 216, 1, 'bbb', 'https://www.youtube.com/watch?v=6EEW-9NDM5k', NULL, NULL, 1, 1, 216, 216, '2019-03-26 11:15:37', '2019-03-26 11:15:37'),
(116, 216, 1, 'bbb', 'https://www.youtube.com/watch?v=6EEW-9NDM5k', NULL, NULL, 1, 1, 216, 216, '2019-03-26 11:18:14', '2019-03-26 11:18:14'),
(117, 216, 1, 'https://www.youtube.com/watch?v=6EEW-9NDM5k', NULL, NULL, NULL, 1, 1, 216, 216, '2019-03-26 11:18:22', '2019-03-26 11:18:22'),
(118, 217, 1, 'hi', NULL, NULL, NULL, 1, 1, 217, 217, '2019-04-04 18:08:47', '2019-04-04 18:08:47'),
(119, 217, 1, 'hi', NULL, NULL, NULL, 1, 1, 217, 217, '2019-04-04 18:09:08', '2019-04-04 18:09:08'),
(120, 221, 1, 'hi', NULL, NULL, NULL, 1, 1, 221, 221, '2019-04-04 18:10:43', '2019-04-04 18:10:43'),
(121, 217, 1, 'newnew', NULL, '1554403095_7QPLVl1lFT-59f8578d6c692.jpg', 'uploads/social-buzz/1554403095_7QPLVl1lFT-59f8578d6c692.jpg', 1, 1, 217, 217, '2019-04-04 18:38:14', '2019-04-04 18:38:14'),
(122, 217, 1, 'newnew', 'https://www.popsci.com/sites/popsci.com/files/styles/1000_1x_/public/images/2018/03/senna.jpg?itok=eYNPMGjA&fc=50,50', NULL, NULL, 1, 1, 217, 217, '2019-04-04 18:42:49', '2019-04-04 18:42:49'),
(123, 217, 1, 'newnew', 'https://www.popsci.com/sites/popsci.com/files/styles/1000_1x_/public/images/2018/03/senna.jpg?itok=eYNPMGjA&fc=50,50', NULL, NULL, 1, 1, 217, 217, '2019-04-04 18:43:00', '2019-04-04 18:43:00'),
(124, 217, 1, 'hi', NULL, '1554403534_CMS_Creative_164657191_Kingfisher.jpg', 'uploads/social-buzz/1554403534_CMS_Creative_164657191_Kingfisher.jpg', 1, 1, 217, 217, '2019-04-04 18:45:31', '2019-04-04 18:45:31'),
(125, 217, 1, 'hi', NULL, NULL, NULL, 1, 1, 217, 217, '2019-04-04 18:46:09', '2019-04-04 18:46:09'),
(126, 217, 1, 'hi', NULL, '1554403584_CMS_Creative_164657191_Kingfisher.jpg', 'uploads/social-buzz/1554403584_CMS_Creative_164657191_Kingfisher.jpg', 1, 1, 217, 217, '2019-04-04 18:46:21', '2019-04-04 18:46:21'),
(127, 217, 1, 'hi', NULL, NULL, NULL, 1, 1, 217, 217, '2019-04-04 18:52:31', '2019-04-04 18:52:31'),
(128, 217, 1, 'hi', NULL, '1554403967_CMS_Creative_164657191_Kingfisher.jpg', 'uploads/social-buzz/1554403967_CMS_Creative_164657191_Kingfisher.jpg', 1, 1, 217, 217, '2019-04-04 18:52:44', '2019-04-04 18:52:44'),
(129, 217, 1, 'hi', NULL, NULL, NULL, 1, 1, 217, 217, '2019-04-04 19:06:40', '2019-04-04 19:06:40'),
(130, 217, 1, 'hi', NULL, '1554404820_CMS_Creative_164657191_Kingfisher.jpg', 'uploads/social-buzz/1554404820_CMS_Creative_164657191_Kingfisher.jpg', 1, 1, 217, 217, '2019-04-04 19:06:57', '2019-04-04 19:06:57'),
(131, 217, 1, 'hi', NULL, NULL, NULL, 1, 1, 217, 217, '2019-04-04 19:17:02', '2019-04-04 19:17:02'),
(132, 217, 1, 'hi', NULL, '1554405451_CMS_Creative_164657191_Kingfisher.jpg', 'uploads/social-buzz/1554405451_CMS_Creative_164657191_Kingfisher.jpg', 1, 1, 217, 217, '2019-04-04 19:17:29', '2019-04-04 19:17:29'),
(133, 217, 1, 'hi', NULL, '1554405621_CMS_Creative_164657191_Kingfisher.jpg', 'uploads/social-buzz/1554405621_CMS_Creative_164657191_Kingfisher.jpg', 1, 1, 217, 217, '2019-04-04 19:20:18', '2019-04-04 19:20:18'),
(134, 197, 1, 'Test', NULL, NULL, NULL, 1, 1, 197, 197, '2019-04-06 22:50:17', '2019-04-06 22:50:17'),
(135, 197, 4, 'Another test sir', NULL, '1554591112_Learn the secrets to making a fortune from your own ( Home Based Business).mp4', 'uploads/social-buzz/1554591112_Learn the secrets to making a fortune from your own ( Home Based Business).mp4', 1, 1, 197, 197, '2019-04-06 22:51:47', '2019-04-06 22:51:47'),
(136, 1, 1, 'tetet', NULL, '1565363233_PayPal.jpeg', 'uploads/social-buzz/1565363233_PayPal.jpeg', 1, 1, 1, 1, '2019-08-09 15:07:13', '2019-08-09 15:07:13');

-- --------------------------------------------------------

--
-- Table structure for table `social_buzz_awards`
--

CREATE TABLE `social_buzz_awards` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `award` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `social_buzz_awards`
--

INSERT INTO `social_buzz_awards` (`id`, `user_id`, `post_id`, `award`, `created_at`, `updated_at`) VALUES
(1, 45, 1, 1, '2018-04-20 12:12:21', '2018-04-20 12:12:21'),
(2, 45, 2, 1, '2018-04-20 12:13:14', '2018-04-20 12:13:14'),
(3, 45, 3, 1, '2018-04-23 09:03:15', '2018-04-23 09:03:15'),
(4, 1, 1, 1, '2018-05-03 04:53:48', '2018-05-03 04:53:48'),
(5, 1, 3, 1, '2018-05-14 20:03:14', '2018-05-14 20:03:14'),
(6, 45, 13, 1, '2018-07-31 09:21:09', '2018-07-31 09:21:09'),
(7, 70, 12, 1, '2018-08-10 14:06:05', '2018-08-10 14:06:05'),
(8, 70, 41, 1, '2018-08-11 11:06:17', '2018-08-11 11:06:17'),
(9, 45, 12, 1, '2018-08-14 12:52:18', '2018-08-14 12:52:18'),
(10, 45, 39, 1, '2018-08-14 18:34:59', '2018-08-14 18:34:59'),
(11, 1, 40, 1, '2018-08-16 04:35:49', '2018-08-16 04:35:49'),
(12, 45, 36, 1, '2018-08-16 06:22:33', '2018-08-16 06:22:33'),
(13, 63, 46, 1, '2018-08-16 07:29:59', '2018-08-16 07:29:59'),
(14, 63, 47, 1, '2018-08-16 07:31:35', '2018-08-16 07:31:35'),
(15, 45, 51, 1, '2018-08-21 02:07:51', '2018-08-21 02:07:51'),
(16, 80, 53, 1, '2018-08-21 07:08:04', '2018-08-21 07:08:04'),
(17, 81, 84, 1, '2018-08-22 08:52:26', '2018-08-22 08:52:26'),
(18, 80, 87, 1, '2018-08-27 10:18:25', '2018-08-27 10:18:25'),
(19, 80, 89, 1, '2018-08-29 13:45:57', '2018-08-29 13:45:57'),
(20, 81, 26, 1, '2018-08-30 09:28:25', '2018-08-30 09:28:25'),
(21, 81, 31, 1, '2018-08-30 09:28:43', '2018-08-30 09:28:43'),
(22, 80, 36, 1, '2018-09-03 13:55:08', '2018-09-03 13:55:08'),
(23, 80, 92, 1, '2018-09-03 13:55:22', '2018-09-03 13:55:22'),
(24, 45, 31, 1, '2018-09-13 11:35:31', '2018-09-13 11:35:31'),
(25, 113, 95, 1, '2018-10-15 11:22:05', '2018-10-15 11:22:05'),
(26, 113, 96, 1, '2018-10-15 11:53:37', '2018-10-15 11:53:37'),
(27, 197, 99, 1, '2019-03-16 07:55:56', '2019-03-16 07:55:56'),
(28, 211, 109, 1, '2019-03-26 09:56:54', '2019-03-26 09:56:54'),
(29, 212, 110, 1, '2019-03-26 10:46:55', '2019-03-26 10:46:55');

-- --------------------------------------------------------

--
-- Table structure for table `social_buzz_comments`
--

CREATE TABLE `social_buzz_comments` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `posted_by` int(11) NOT NULL,
  `post_comment` varchar(2042) DEFAULT NULL,
  `post_date` datetime DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `social_buzz_comments`
--

INSERT INTO `social_buzz_comments` (`id`, `user_id`, `post_id`, `posted_by`, `post_comment`, `post_date`, `active`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(13, 1, 13, 13, 'nice product', '2018-07-31 11:17:36', 1, '2018-07-31 11:17:36', '2018-07-31 11:17:36', 1, 1),
(14, 45, 12, 12, 'hi', '2018-07-31 11:18:57', 1, '2018-07-31 11:18:57', '2018-07-31 11:18:57', 45, 45),
(15, 1, 13, 13, 'hi', '2018-07-31 11:20:21', 1, '2018-07-31 11:20:21', '2018-07-31 11:20:21', 1, 1),
(16, 45, 13, 13, 'hi', '2018-07-31 11:21:00', 1, '2018-07-31 11:21:00', '2018-07-31 11:21:00', 45, 45),
(17, 45, 12, 12, 'hi', '2018-07-31 11:31:15', 1, '2018-07-31 11:31:15', '2018-07-31 11:31:15', 45, 45),
(18, 70, 20, 20, 'kaaa', '2018-08-09 11:07:45', 1, '2018-08-09 11:07:45', '2018-08-09 11:07:45', 70, 70),
(19, 70, 20, 20, '22222', '2018-08-09 11:07:57', 1, '2018-08-09 11:07:57', '2018-08-09 11:07:57', 70, 70),
(20, 45, 15, 15, 'dasdasd', '2018-08-09 11:59:09', 1, '2018-08-09 11:59:09', '2018-08-09 11:59:09', 45, 45),
(21, 45, 15, 15, 'dasdasd', '2018-08-09 11:59:25', 1, '2018-08-09 11:59:25', '2018-08-09 11:59:25', 45, 45),
(22, 70, 12, 12, 'qqqqqqq', '2018-08-10 14:06:10', 1, '2018-08-10 14:06:10', '2018-08-10 14:06:10', 70, 70),
(23, 63, 29, 29, 'comment : Buyer 15', '2018-08-11 09:19:27', 1, '2018-08-11 09:19:27', '2018-08-11 09:19:27', 63, 63),
(24, 63, 29, 29, 'Comment : Buyer 15 2nd', '2018-08-11 09:19:53', 1, '2018-08-11 09:19:53', '2018-08-11 09:19:53', 63, 63),
(25, 63, 29, 29, 'comment : 3rd Buyer15', '2018-08-11 09:20:39', 1, '2018-08-11 09:20:39', '2018-08-11 09:20:39', 63, 63),
(26, 63, 40, 40, 'buyer 15', '2018-08-11 09:48:20', 1, '2018-08-11 09:48:20', '2018-08-11 09:48:20', 63, 63),
(27, 70, 41, 41, 'no link', '2018-08-11 11:06:24', 1, '2018-08-11 11:06:24', '2018-08-11 11:06:24', 70, 70),
(28, 45, 40, 40, 'This product pretty nice', '2018-08-13 17:41:18', 1, '2018-08-13 17:41:18', '2018-08-13 17:41:18', 45, 45),
(29, 45, 39, 39, 'Cool picture', '2018-08-14 18:35:09', 1, '2018-08-14 18:35:09', '2018-08-14 18:35:09', 45, 45),
(30, 1, 40, 40, 'I agree Five buyer. I think I may purchase this single.', '2018-08-16 04:34:57', 1, '2018-08-16 04:34:57', '2018-08-16 04:34:57', 1, 1),
(31, 1, 37, 37, 'Nice', '2018-08-16 04:36:50', 1, '2018-08-16 04:36:50', '2018-08-16 04:36:50', 1, 1),
(32, 45, 12, 12, 'fff', '2018-08-16 06:21:25', 1, '2018-08-16 06:21:25', '2018-08-16 06:21:25', 45, 45),
(33, 45, 12, 12, 'ffffffff', '2018-08-16 06:21:31', 1, '2018-08-16 06:21:31', '2018-08-16 06:21:31', 45, 45),
(34, 63, 46, 46, 'test', '2018-08-16 07:30:11', 1, '2018-08-16 07:30:11', '2018-08-16 07:30:11', 63, 63),
(35, 63, 46, 46, 'test', '2018-08-16 07:31:00', 1, '2018-08-16 07:31:00', '2018-08-16 07:31:00', 63, 63),
(36, 63, 47, 47, 'Post added sucessfully', '2018-08-16 07:31:47', 1, '2018-08-16 07:31:47', '2018-08-16 07:31:47', 63, 63),
(37, 63, 21, 21, 'qwe', '2018-08-16 08:52:25', 1, '2018-08-16 08:52:25', '2018-08-16 08:52:25', 63, 63),
(38, 63, 12, 12, 'qwe', '2018-08-16 08:58:46', 1, '2018-08-16 08:58:46', '2018-08-16 08:58:46', 63, 63),
(39, 70, 12, 12, 'ddfdfdf', '2018-08-16 13:26:49', 1, '2018-08-16 13:26:49', '2018-08-16 13:26:49', 70, 70),
(40, 70, 12, 12, 'dfdfdfdf', '2018-08-16 13:27:13', 1, '2018-08-16 13:27:13', '2018-08-16 13:27:13', 70, 70),
(41, 70, 12, 12, 'fsdfsdfsdf', '2018-08-16 13:27:31', 1, '2018-08-16 13:27:31', '2018-08-16 13:27:31', 70, 70),
(42, 70, 15, 15, 'hdh', '2018-08-17 09:23:07', 1, '2018-08-17 09:23:07', '2018-08-17 09:23:07', 70, 70),
(43, 45, 62, 62, 'sdsdsds', '2018-08-21 05:43:32', 1, '2018-08-21 05:43:32', '2018-08-21 05:43:32', 45, 45),
(44, 80, 53, 53, 'yi', '2018-08-21 07:08:10', 1, '2018-08-21 07:08:10', '2018-08-21 07:08:10', 80, 80),
(45, 63, 62, 62, 'ddddd', '2018-08-22 06:02:04', 1, '2018-08-22 06:02:04', '2018-08-22 06:02:04', 63, 63),
(46, 80, 87, 87, '123456', '2018-08-27 10:18:16', 1, '2018-08-27 10:18:16', '2018-08-27 10:18:16', 80, 80),
(47, 81, 62, 62, 'check', '2018-08-28 06:46:37', 1, '2018-08-28 06:46:37', '2018-08-28 06:46:37', 81, 81),
(48, 80, 38, 38, 'qwerty', '2018-08-28 14:15:12', 1, '2018-08-28 14:15:12', '2018-08-28 14:15:12', 80, 80),
(49, 80, 38, 38, ',,,', '2018-08-28 14:15:19', 1, '2018-08-28 14:15:19', '2018-08-28 14:15:19', 80, 80),
(50, 80, 89, 89, '1', '2018-08-29 13:46:04', 1, '2018-08-29 13:46:04', '2018-08-29 13:46:04', 80, 80),
(51, 80, 89, 89, '2', '2018-08-29 13:46:11', 1, '2018-08-29 13:46:11', '2018-08-29 13:46:11', 80, 80),
(52, 81, 37, 37, '2', '2018-08-30 09:21:39', 1, '2018-08-30 09:21:39', '2018-08-30 09:21:39', 81, 81),
(53, 81, 31, 31, '123', '2018-08-30 09:28:54', 1, '2018-08-30 09:28:54', '2018-08-30 09:28:54', 81, 81),
(54, 1, 89, 89, 'test', '2018-08-31 03:43:43', 1, '2018-08-31 03:43:43', '2018-08-31 03:43:43', 1, 1),
(55, 80, 92, 92, '1 commen', '2018-09-03 13:55:31', 1, '2018-09-03 13:55:31', '2018-09-03 13:55:31', 80, 80),
(56, 80, 92, 92, '2 comment', '2018-09-03 13:55:44', 1, '2018-09-03 13:55:44', '2018-09-03 13:55:44', 80, 80),
(57, 1, 89, 89, 'I like it.', '2018-09-04 02:58:46', 1, '2018-09-04 02:58:46', '2018-09-04 02:58:46', 1, 1),
(58, 85, 89, 89, 'own', '2018-09-08 13:52:53', 1, '2018-09-08 13:52:53', '2018-09-08 13:52:53', 85, 85),
(59, 70, 94, 94, 'hiii', '2018-09-10 10:42:02', 1, '2018-09-10 10:42:02', '2018-09-10 10:42:02', 70, 70),
(60, 45, 31, 31, 'dfgfdgdfg', '2018-09-13 11:35:36', 1, '2018-09-13 11:35:36', '2018-09-13 11:35:36', 45, 45),
(61, 99, 89, 89, 'No reply', '2018-09-15 08:09:59', 1, '2018-09-15 08:09:59', '2018-09-15 08:09:59', 99, 99),
(62, 113, 95, 95, '1', '2018-10-15 11:22:11', 1, '2018-10-15 11:22:11', '2018-10-15 11:22:11', 113, 113),
(63, 113, 88, 88, '1', '2018-10-15 11:34:37', 1, '2018-10-15 11:34:37', '2018-10-15 11:34:37', 113, 113),
(64, 113, 88, 88, '2222', '2018-10-15 11:34:58', 1, '2018-10-15 11:34:58', '2018-10-15 11:34:58', 113, 113),
(65, 113, 88, 88, '333', '2018-10-15 11:35:13', 1, '2018-10-15 11:35:13', '2018-10-15 11:35:13', 113, 113),
(66, 172, 88, 88, 'Karan, what do you think about this product? Is it cool?', '2018-11-12 00:35:18', 1, '2018-11-12 00:35:18', '2018-11-12 00:35:18', 172, 172),
(67, 1, 97, 97, 'I have a product for sale', '2018-12-27 06:05:22', 1, '2018-12-27 06:05:22', '2018-12-27 06:05:22', 1, 1),
(68, 197, 99, 99, 'I\'ll think about purchasing your product.', '2019-03-16 07:55:24', 1, '2019-03-16 07:55:24', '2019-03-16 07:55:24', 197, 197),
(69, 205, 88, 88, 'cxzc', '2019-03-24 18:14:48', 1, '2019-03-24 18:14:48', '2019-03-24 18:14:48', 205, 205),
(70, 211, 109, 109, 'comments comments', '2019-03-26 09:57:07', 1, '2019-03-26 09:57:07', '2019-03-26 09:57:07', 211, 211),
(71, 212, 110, 110, 'hello there!', '2019-03-26 10:47:10', 1, '2019-03-26 10:47:10', '2019-03-26 10:47:10', 212, 212),
(72, 216, 99, 99, 'good', '2019-03-26 11:16:46', 1, '2019-03-26 11:16:46', '2019-03-26 11:16:46', 216, 216);

-- --------------------------------------------------------

--
-- Table structure for table `social_buzz_replies`
--

CREATE TABLE `social_buzz_replies` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment_id` int(11) NOT NULL,
  `reply` varchar(2042) NOT NULL,
  `reply_date` datetime DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `social_buzz_riders`
--

CREATE TABLE `social_buzz_riders` (
  `id` int(11) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `post_id` varchar(255) NOT NULL,
  `rider` varchar(255) NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `social_buzz_riders`
--

INSERT INTO `social_buzz_riders` (`id`, `user_id`, `post_id`, `rider`, `created_at`, `updated_at`) VALUES
(1, '1', '11', '1', '2018-08-11 00:00:00', '2018-08-11 00:00:00'),
(2, '1', '12', '1', NULL, NULL),
(3, '1', '14', '1', NULL, NULL),
(4, '45', '13', '1', NULL, NULL),
(5, '45', '16', '1', NULL, NULL),
(6, '45', '19', '1', NULL, NULL),
(7, '45', '14', '1', NULL, NULL),
(8, '45', '40', '1', NULL, NULL),
(9, '63', '14', '1', NULL, NULL),
(10, '63', '13', '1', NULL, NULL),
(11, '63', '12', '1', NULL, NULL),
(12, '63', '15', '1', NULL, NULL),
(13, '63', '16', '1', NULL, NULL),
(14, '63', '17', '1', NULL, NULL),
(15, '63', '18', '1', NULL, NULL),
(16, '70', '12', '1', NULL, NULL),
(17, '70', '43', '1', NULL, NULL),
(18, '70', '44', '1', NULL, NULL),
(19, '1', '40', '1', NULL, NULL),
(20, '45', '51', '1', NULL, NULL),
(21, '80', '59', '1', NULL, NULL),
(22, '81', '66', '1', NULL, NULL),
(23, '80', '53', '1', NULL, NULL),
(24, '80', '66', '1', NULL, NULL),
(25, '80', '65', '1', NULL, NULL),
(26, '80', '64', '1', NULL, NULL),
(27, '80', '63', '1', NULL, NULL),
(28, '80', '62', '1', NULL, NULL),
(29, '80', '61', '1', NULL, NULL),
(30, '80', '60', '1', NULL, NULL),
(31, '80', '57', '1', NULL, NULL),
(32, '80', '56', '1', NULL, NULL),
(33, '80', '54', '1', NULL, NULL),
(34, '80', '55', '1', NULL, NULL),
(35, '80', '52', '1', NULL, NULL),
(36, '80', '50', '1', NULL, NULL),
(37, '80', '49', '1', NULL, NULL),
(38, '80', '48', '1', NULL, NULL),
(39, '80', '47', '1', NULL, NULL),
(40, '80', '46', '1', NULL, NULL),
(41, '80', '45', '1', NULL, NULL),
(42, '80', '44', '1', NULL, NULL),
(43, '80', '43', '1', NULL, NULL),
(44, '80', '42', '1', NULL, NULL),
(45, '80', '41', '1', NULL, NULL),
(46, '80', '22', '1', NULL, NULL),
(47, '80', '21', '1', NULL, NULL),
(48, '80', '20', '1', NULL, NULL),
(49, '80', '19', '1', NULL, NULL),
(50, '80', '18', '1', NULL, NULL),
(51, '80', '17', '1', NULL, NULL),
(52, '80', '16', '1', NULL, NULL),
(53, '80', '15', '1', NULL, NULL),
(54, '80', '14', '1', NULL, NULL),
(55, '81', '84', '1', NULL, NULL),
(56, '80', '87', '1', NULL, NULL),
(57, '80', '38', '1', NULL, NULL),
(58, '1', '85', '1', NULL, NULL),
(59, '80', '88', '1', NULL, NULL),
(60, '80', '89', '1', NULL, NULL),
(61, '81', '87', '1', NULL, NULL),
(62, '81', '37', '1', NULL, NULL),
(63, '81', '26', '1', NULL, NULL),
(64, '81', '31', '1', NULL, NULL),
(65, '80', '92', '1', NULL, NULL),
(66, '1', '89', '1', NULL, NULL),
(67, '1', '28', '1', NULL, NULL),
(68, '45', '31', '1', NULL, NULL),
(69, '113', '95', '1', NULL, NULL),
(70, '113', '88', '1', NULL, NULL),
(71, '1', '99', '1', NULL, NULL),
(72, '1', '103', '1', NULL, NULL),
(73, '197', '99', '1', NULL, NULL),
(74, '202', '97', '1', NULL, NULL),
(75, '205', '88', '1', NULL, NULL),
(76, '211', '109', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `social_login_profiles`
--

CREATE TABLE `social_login_profiles` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `facebook_id` varchar(255) NOT NULL,
  `google_id` varchar(255) NOT NULL,
  `twitter_id` varchar(255) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `social_promote_users`
--

CREATE TABLE `social_promote_users` (
  `id` int(11) NOT NULL,
  `promote_id` int(11) NOT NULL,
  `social_share_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `social_promote_users`
--

INSERT INTO `social_promote_users` (`id`, `promote_id`, `social_share_id`, `created_at`, `updated_at`) VALUES
(1, 17, 3, '2018-05-25 09:57:19', '2018-05-25 09:57:19'),
(2, 13, 4, '2018-07-13 14:02:35', '2018-07-13 14:02:35'),
(3, 14, 5, '2018-07-16 08:39:37', '2018-07-16 08:39:37'),
(4, 15, 6, '2018-07-28 12:42:19', '2018-07-28 12:42:19'),
(5, 16, 7, '2018-08-06 06:37:24', '2018-08-06 06:37:24'),
(6, 17, 8, '2018-08-09 21:56:06', '2018-08-09 21:56:06'),
(7, 18, 9, '2018-08-16 11:12:09', '2018-08-16 11:12:09'),
(8, 19, 10, '2018-08-16 11:12:56', '2018-08-16 11:12:56'),
(9, 20, 11, '2018-08-16 12:43:05', '2018-08-16 12:43:05'),
(10, 21, 12, '2018-08-20 10:15:08', '2018-08-20 10:15:08'),
(11, 22, 13, '2018-08-20 10:16:48', '2018-08-20 10:16:48'),
(12, 23, 14, '2018-08-21 13:08:04', '2018-08-21 13:08:04'),
(13, 24, 15, '2018-08-21 13:18:09', '2018-08-21 13:18:09'),
(14, 25, 16, '2018-08-21 13:18:13', '2018-08-21 13:18:13'),
(15, 26, 17, '2018-08-27 09:30:11', '2018-08-27 09:30:11'),
(16, 27, 18, '2018-08-27 09:34:17', '2018-08-27 09:34:17'),
(17, 28, 19, '2018-08-28 10:22:28', '2018-08-28 10:22:28'),
(18, 29, 20, '2018-08-29 09:12:39', '2018-08-29 09:12:39'),
(19, 30, 21, '2018-08-29 10:03:29', '2018-08-29 10:03:29'),
(20, 31, 22, '2018-09-03 10:26:45', '2018-09-03 10:26:45'),
(21, 32, 23, '2018-09-03 13:20:12', '2018-09-03 13:20:12'),
(22, 33, 24, '2018-09-04 10:09:31', '2018-09-04 10:09:31'),
(23, 34, 25, '2018-09-04 10:15:44', '2018-09-04 10:15:44'),
(24, 35, 26, '2018-09-13 10:44:35', '2018-09-13 10:44:35'),
(25, 36, 27, '2018-09-13 10:45:40', '2018-09-13 10:45:40'),
(26, 37, 28, '2018-09-13 10:56:17', '2018-09-13 10:56:17'),
(27, 38, 29, '2018-11-11 10:05:31', '2018-11-11 10:05:31'),
(28, 39, 30, '2018-12-27 06:04:12', '2018-12-27 06:04:12'),
(29, 40, 31, '2019-03-26 07:30:32', '2019-03-26 07:30:32'),
(30, 41, 32, '2019-03-26 08:49:13', '2019-03-26 08:49:13'),
(31, 42, 33, '2019-03-26 09:52:13', '2019-03-26 09:52:13'),
(32, 43, 34, '2019-03-26 10:15:59', '2019-03-26 10:15:59'),
(33, 44, 35, '2019-03-26 10:26:25', '2019-03-26 10:26:25'),
(34, 45, 36, '2019-04-21 12:27:07', '2019-04-21 12:27:07'),
(35, 46, 37, '2019-04-21 19:05:49', '2019-04-21 19:05:49');

-- --------------------------------------------------------

--
-- Table structure for table `social_shares`
--

CREATE TABLE `social_shares` (
  `id` int(11) NOT NULL,
  `name` varchar(225) DEFAULT NULL,
  `link` varchar(225) DEFAULT NULL,
  `total_count` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `social_shares`
--

INSERT INTO `social_shares` (`id`, `name`, `link`, `total_count`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'facebook', NULL, NULL, 1, 1, '2018-05-25 09:54:06', '2018-05-25 09:54:06'),
(2, 'facebook', NULL, NULL, 1, 1, '2018-05-25 09:54:35', '2018-05-25 09:54:35'),
(3, 'facebook', NULL, NULL, 1, 1, '2018-05-25 09:57:19', '2018-05-25 09:57:19'),
(4, 'linkedin', NULL, NULL, 45, 45, '2018-07-13 14:02:34', '2018-07-13 14:02:34'),
(5, 'facebook', NULL, NULL, 45, 45, '2018-07-16 08:39:37', '2018-07-16 08:39:37'),
(6, 'linkedin', NULL, NULL, 45, 45, '2018-07-28 12:42:19', '2018-07-28 12:42:19'),
(7, NULL, NULL, NULL, 45, 45, '2018-08-06 06:37:24', '2018-08-06 06:37:24'),
(8, 'instagram', NULL, NULL, 1, 1, '2018-08-09 21:56:06', '2018-08-09 21:56:06'),
(9, NULL, NULL, NULL, 70, 70, '2018-08-16 11:12:09', '2018-08-16 11:12:09'),
(10, NULL, NULL, NULL, 70, 70, '2018-08-16 11:12:56', '2018-08-16 11:12:56'),
(11, NULL, NULL, NULL, 79, 79, '2018-08-16 12:43:05', '2018-08-16 12:43:05'),
(12, NULL, NULL, NULL, 81, 81, '2018-08-20 10:15:08', '2018-08-20 10:15:08'),
(13, 'facebook', NULL, NULL, 81, 81, '2018-08-20 10:16:48', '2018-08-20 10:16:48'),
(14, NULL, NULL, NULL, 70, 70, '2018-08-21 13:08:04', '2018-08-21 13:08:04'),
(15, NULL, NULL, NULL, 80, 80, '2018-08-21 13:18:09', '2018-08-21 13:18:09'),
(16, NULL, NULL, NULL, 80, 80, '2018-08-21 13:18:13', '2018-08-21 13:18:13'),
(17, NULL, NULL, NULL, 81, 81, '2018-08-27 09:30:11', '2018-08-27 09:30:11'),
(18, NULL, NULL, NULL, 81, 81, '2018-08-27 09:34:17', '2018-08-27 09:34:17'),
(19, 'twitter', NULL, NULL, 45, 45, '2018-08-28 10:22:28', '2018-08-28 10:22:28'),
(20, NULL, NULL, NULL, 84, 84, '2018-08-29 09:12:39', '2018-08-29 09:12:39'),
(21, NULL, NULL, NULL, 80, 80, '2018-08-29 10:03:29', '2018-08-29 10:03:29'),
(22, 'facebook', NULL, NULL, 92, 92, '2018-09-03 10:26:45', '2018-09-03 10:26:45'),
(23, NULL, NULL, NULL, 92, 92, '2018-09-03 13:20:12', '2018-09-03 13:20:12'),
(24, NULL, NULL, NULL, 91, 91, '2018-09-04 10:09:31', '2018-09-04 10:09:31'),
(25, NULL, NULL, NULL, 91, 91, '2018-09-04 10:15:44', '2018-09-04 10:15:44'),
(26, NULL, NULL, NULL, 1, 1, '2018-09-13 10:44:35', '2018-09-13 10:44:35'),
(27, NULL, NULL, NULL, 1, 1, '2018-09-13 10:45:40', '2018-09-13 10:45:40'),
(28, NULL, NULL, NULL, 1, 1, '2018-09-13 10:56:17', '2018-09-13 10:56:17'),
(29, 'facebook', NULL, NULL, 168, 168, '2018-11-11 10:05:31', '2018-11-11 10:05:31'),
(30, NULL, NULL, NULL, 1, 1, '2018-12-27 06:04:12', '2018-12-27 06:04:12'),
(31, 'facebook', NULL, NULL, 212, 212, '2019-03-26 07:30:32', '2019-03-26 07:30:32'),
(32, 'twitter', NULL, NULL, 212, 212, '2019-03-26 08:49:13', '2019-03-26 08:49:13'),
(33, NULL, NULL, NULL, 212, 212, '2019-03-26 09:52:13', '2019-03-26 09:52:13'),
(34, NULL, NULL, NULL, 212, 212, '2019-03-26 10:15:59', '2019-03-26 10:15:59'),
(35, NULL, NULL, NULL, 212, 212, '2019-03-26 10:26:25', '2019-03-26 10:26:25'),
(36, NULL, NULL, NULL, 220, 220, '2019-04-21 12:27:07', '2019-04-21 12:27:07'),
(37, NULL, NULL, NULL, 220, 220, '2019-04-21 19:05:49', '2019-04-21 19:05:49');

-- --------------------------------------------------------

--
-- Table structure for table `talents`
--

CREATE TABLE `talents` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `talent_category_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(2014) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `product_info` varchar(2024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avg_rating` int(11) DEFAULT '0',
  `view` int(11) DEFAULT '0',
  `facebookLink` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagramLink` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitterLink` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` varchar(225) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `approved` tinyint(4) NOT NULL DEFAULT '0',
  `delete_flag` int(11) NOT NULL DEFAULT '0',
  `date` date DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `talents`
--

INSERT INTO `talents` (`id`, `user_id`, `talent_category_id`, `title`, `description`, `price`, `product_info`, `avg_rating`, `view`, `facebookLink`, `instagramLink`, `twitterLink`, `active`, `approved`, `delete_flag`, `date`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(170, 1, 1, 'test1', 'welcome1', '454', 'test data1', 3, 16, 'www.facebook.com', NULL, NULL, 'Deactive', 0, 0, '2018-05-01', 1, 1, '2018-05-01 01:04:21', '2018-05-01 01:04:21'),
(173, 1, 6, 'dcd', NULL, '33', 'updated/........', 5, 71, NULL, NULL, NULL, 'Active', 1, 0, '2018-05-01', 1, 1, '2018-05-01 03:28:22', '2018-05-01 03:28:22'),
(175, 1, 1, 'Commercial test', 'A very talented seller that has professional experience in the music industry.', '23', 'This is a text.', 0, 0, NULL, NULL, NULL, 'Deactive', 1, 0, '2018-05-14', 1, 1, '2018-05-14 20:10:38', '2018-05-14 20:10:38'),
(179, 1, 16, 'dfsdf', 'sdf', '645', 'sdf', 0, 2, NULL, NULL, NULL, 'Active', 0, 0, '2018-07-09', 1, 1, '2018-07-09 00:34:41', '2018-07-09 00:34:41'),
(180, 1, 1, 'Awesomw writing', 'gdg', '10', 'fcvbb', 0, 16, 'http:/facebook.com', 'http:/facebook.com', 'http:/facebook.com', 'Active', 0, 0, '2018-08-02', 1, 1, '2018-08-02 05:52:03', '2018-08-02 05:52:03'),
(181, 1, 1, 'qqqq updated', 'updated', '100', 'updated', 0, 10, NULL, NULL, NULL, 'Active', 0, 0, '2018-08-06', 1, 1, '2018-08-06 08:42:45', '2018-08-06 08:42:45'),
(182, 1, 1, '2 video', NULL, '100', NULL, 0, 3, NULL, NULL, NULL, 'Active', 0, 0, '2018-08-06', 1, 1, '2018-08-06 13:38:00', '2018-08-06 13:38:00'),
(183, 1, 14, 'cosmetic VIDEO PREVIEW', 'VIDEO PREVIEW', '99', 'VIDEO PREVIEW', 0, 5, NULL, NULL, NULL, 'Active', 0, 0, '2018-08-07', 1, 1, '2018-08-07 13:38:56', '2018-08-07 13:38:56'),
(230, 1, 2, 'Entertainment Night', 'check loader for multiple file', '222', 'check loader for multiple file', 0, 0, NULL, NULL, NULL, 'Active', 0, 0, '2018-08-29', 1, 1, '2018-08-29 10:12:12', '2018-08-29 10:12:12'),
(231, 1, 10, 'Science Fiction', 'videoUploader', '1452', 'videoUploader', 0, 0, NULL, NULL, NULL, 'Active', 0, 0, '2018-08-29', 1, 1, '2018-08-29 10:15:45', '2018-08-29 10:15:45'),
(232, 1, 1, 'pdf video', 'console.log("loader from appProducts");', '1452', 'hello ', 0, 0, NULL, NULL, NULL, 'Deactive', 0, 0, '2018-08-29', 1, 1, '2018-08-29 10:19:07', '2018-08-29 10:19:07'),
(233, 1, 6, 'loader for all', 'bbbbbbbbbb', '1452', 'aaaaaa', 0, 0, NULL, NULL, NULL, 'Deactive', 0, 0, '2018-08-29', 1, 1, '2018-08-29 10:23:33', '2018-08-29 10:23:33'),
(234, 1, 1, 'test', NULL, '12', NULL, 0, 78, NULL, NULL, NULL, 'Active', 1, 0, '2018-08-29', 1, 1, '2018-08-29 10:25:31', '2018-08-29 10:25:31'),
(235, 1, 4, 'My Music', 'Test data.', '23', '1.Bio Information of Product\n2. Bio Information of Seller', 0, 75, NULL, NULL, NULL, 'Active', 1, 0, '2018-08-29', 1, 1, '2018-08-29 10:35:08', '2018-08-29 10:35:08'),
(236, 1, 5, 'wild life photography', NULL, '44', NULL, 5, 25, NULL, NULL, NULL, 'Active', 1, 0, '2018-08-29', 1, 1, '2018-08-29 10:37:44', '2018-08-29 10:37:44'),
(238, 1, 13, 'Talent hunt', 'addeddsadd asdsadsad\'assdkasnlkdaskhd $sfgahlk\'laa', '123', 'added asdasdasdsads dssadasd asdsa', 0, 0, NULL, NULL, NULL, 'Deactive', 1, 0, '2018-08-29', 1, 1, '2018-08-29 11:12:37', '2018-08-29 11:12:37'),
(244, 1, 1, 'Writer', 'Writer', '120', 'Writer', 0, 11, NULL, NULL, NULL, 'Deactive', 1, 0, '2018-09-03', 1, 1, '2018-09-03 06:25:17', '2018-09-03 06:25:17'),
(250, 1, 6, 'I can make you laugh', 'Welcome to FutureStarr', '1000', 'Becoming a professional tattoo artist demands a lot of time, talent, and tenacity. Well-known and sought-after experts in this field go through numerous hurdles before they achieve their goal. The good news is that there are tried and true methods on how you can become a respected and certified body arts specialist. \n\nSurefire Ways to Become a Professional Tattoo Artist\n\nLearning the trade and continuously improving your craft as a tattoo artist is an ongoing process. There are new trends and techniques about body arts that debut in its niche every now and then. \n\nHere are some of the important ways to help you have a solid stronghold for a successful career:\n\nUnderstand your Artwork\n\nTattoos are not just about inking the body with graphics and images. As a certified tattoo artist, you need to know the heart and soul of your artwork. Tattooing is a craft and not just superficial skin arts. The best way to establish your career as a tattoo artist is to imbibe a passionate drive in learning tattoos from their origins to the latest trends. \n\nBe Versatile in your Talent', 0, 24, 'https://www.facebook.com/profile.php?id=100009493880629', NULL, NULL, 'Active', 1, 0, '2018-09-09', 1, 1, '2018-09-09 03:05:07', '2018-09-09 03:05:07'),
(254, 1, 8, 'Fitness 2', NULL, '1212', NULL, 0, 5, NULL, NULL, NULL, 'Deactive', 1, 0, '2018-09-10', 1, 1, '2018-09-10 05:50:34', '2018-09-10 05:50:34'),
(257, 1, 13, 'Mathematics', NULL, '456', 'Mathematics information', 0, 15, NULL, NULL, NULL, 'Active', 1, 0, '2018-09-10', 1, 1, '2018-09-10 06:13:03', '2018-09-10 06:13:03'),
(258, 1, 7, 'Car modal', NULL, '2000', 'A car model (or automobile model or model of car) is the name used by a manufacturer to market a range of similar cars. The way that car manufacturers group their product range into models varies between manufacturers.\n\nA model may also be referred to as a nameplate, specifically when referring to the product from the point of view of the manufacturer, especially a model over time. For example, the Chevrolet Suburban is the oldest automobile nameplate in continuous production, dating to 1934 (1935 model year), while the Chrysler New Yorker was (until its demise in 1996) the oldest North American car nameplate. "Nameplate" is also sometimes used more loosely, however, to refer to a brand or division of larger company (e.g., GMC), rather than a specific model.Given that the interior equipment, upholstery and exterior trim is usually determined by the trim level, the car model often defines the styling theme and platform that is used.[1][2][3][4] The model also defines the body style(s) and engine choice(s).[5]\n\nSome models have only one body style (e.g. the Mazda 2 hatchback),[6] while other models are produced in several body styles (e.g. the Audi A3, which has been produced in hatchback, sedan and convertible body styles).[7] Similarly, some models have a single engine/powertrain specification available (eg the Chevrolet Volt), while other models have multiple powertrains available (eg the Ford Mustang, which has been produced with inline-4, V6 and V8 engines).[8]', 0, 1, NULL, NULL, NULL, 'Deactive', 1, 0, '2018-09-10', 1, 1, '2018-09-10 13:37:08', '2018-09-10 13:37:08'),
(267, 1, 2, 'abc', 'aaa', '50', 'aaa', 0, 0, NULL, NULL, NULL, 'Active', 0, 0, '2018-10-22', 1, 1, '2018-10-22 13:00:56', '2018-10-22 13:00:56'),
(268, 1, 2, 'div', 'di', '10', 'aaa', 0, 0, NULL, NULL, NULL, 'Active', 0, 0, '2019-03-09', 1, 1, '2019-03-09 13:21:31', '2019-03-09 13:21:31'),
(269, 1, 1, 'This is testing for author', 'Testing products with images..', '100', 'Testing products with images..', 0, 0, NULL, NULL, NULL, 'Active', 0, 0, '2018-11-11', 1, 1, '2018-11-11 10:04:48', '2018-11-11 10:04:48'),
(270, 1, 2, 'This is a test commercial', 'Diligent, hardworking talent individual with a passion to be successful in all genres of talent.', '20', 'Hello, please disregard this product. DO NOT PURCHASE.', 0, 0, NULL, NULL, NULL, 'Active', 0, 0, '2018-11-12', 1, 1, '2018-11-12 00:08:46', '2018-11-12 00:08:46'),
(271, 1, 2, 'Test', 'Test', '0', 'Test', 0, 0, NULL, NULL, NULL, 'Active', 0, 0, '2018-11-12', 1, 1, '2018-11-12 00:12:51', '2018-11-12 00:12:51'),
(272, 1, 2, 'music system', 'music', '10', 'music', 0, 58, NULL, NULL, NULL, 'Active', 1, 0, '2018-11-14', 1, 1, '2018-11-14 13:38:28', '2018-11-14 13:38:28'),
(273, 1, 1, 'aa', 'aa', '100', 'aa', 0, 13, NULL, NULL, NULL, 'Active', 1, 0, '2018-11-14', 1, 1, '2018-11-14 14:25:31', '2018-11-14 14:25:31'),
(274, 1, 4, 'Music is "love" is music', 'Bio informations for product', '250', 'Bio information for product', 0, 4, 'https://www.facebook.com/', 'https://www.instagram.com/', 'https://twitter.com/', 'Active', 1, 0, '2019-03-26', 1, 1, '2019-03-26 06:39:10', '2019-03-26 06:39:10'),
(275, 1, 12, 'Food compotistions', 'Nutrition.gov is a USDA-sponsored website that offers credible information to help you make healthful eating choices.', '100', 'Nutrition.gov is a USDA-sponsored website that offers credible information to help you make healthful eating choices.', 0, 0, 'https://twitter.com/', 'https://twitter.com/', 'https://twitter.com/', 'Active', 0, 0, '2019-03-26', 1, 1, '2019-03-26 07:05:48', '2019-03-26 07:05:48'),
(276, 1, 11, 'food is first love', '“All you need is love. But a little chocolate now and then doesn\'t hurt.', '300', '“All you need is love. But a little chocolate now and then doesn\'t hurt.', 0, 3, NULL, NULL, NULL, 'Active', 1, 0, '2019-03-26', 1, 1, '2019-03-26 07:29:55', '2019-03-26 07:29:55'),
(277, 1, 11, 'I love you like a fat kid loves cake', 'Ask not what you can do for your country. Ask what’s for lunch', '300', 'One cannot think well, love well, sleep well, if one has not dined well', 0, 1, 'https://www.facebook.com/', 'https://www.instagram.com/', 'https://twitter.com/', 'Active', 1, 0, '2019-03-26', 1, 1, '2019-03-26 08:45:52', '2019-03-26 08:45:52'),
(278, 1, 5, 'photography', 'photography love', '400', 'phohtography love', 0, 0, 'https://www.facebook.com/', 'https://www.instagram.com/', 'https://twitter.com', 'Active', 1, 0, '2019-03-26', 1, 1, '2019-03-26 09:39:13', '2019-03-26 09:39:13'),
(279, 1, 2, 'Fun is entertainment', 'bio information of seller', '500', 'bio information of product', 0, 0, NULL, NULL, NULL, 'Active', 0, 0, '2019-03-26', 1, 1, '2019-03-26 09:41:15', '2019-03-26 09:41:15'),
(280, 1, 10, 'science', 'upload audion or video here', '10', 'upload image or video here', 0, 0, NULL, NULL, NULL, 'Active', 1, 0, '2019-03-26', 1, 1, '2019-03-26 10:03:17', '2019-03-26 10:03:17'),
(281, 1, 14, 'cosmetics', 'bio information o fseller', '20', 'bio informations', 0, 2, 'https://www.facebook.com/', 'https://www.instagram.com/', 'https://twitter.com/', 'Active', 1, 0, '2019-03-26', 1, 1, '2019-03-26 10:25:56', '2019-03-26 10:25:56'),
(282, 1, 8, 'fitness', 'this is for seller', '30', 'this fields for product', 0, 0, 'https://www.facebook.com/', 'https://www.instagram.com/', 'https://twitter.com/', 'Active', 0, 0, '2019-03-26', 1, 1, '2019-03-26 11:26:00', '2019-03-26 11:26:00'),
(283, 1, 6, 'Just for comedy', 'seller', '50', 'product', 0, 6, 'https://www.facebook.com/', 'https://www.instagram.com/', 'https://twitter.com/', 'Active', 1, 0, '2019-03-26', 1, 1, '2019-03-26 11:28:08', '2019-03-26 11:28:08'),
(284, 1, 7, 'modeling', 'for seller', '40', 'fo rproduct', 0, 6, 'https://www.facebook.com/', 'https://www.instagram.com/', 'https://twitter.com/', 'Active', 1, 0, '2019-03-26', 1, 1, '2019-03-26 11:32:02', '2019-03-26 11:32:02'),
(285, 1, 4, 'test', NULL, '1', NULL, 0, 4, NULL, NULL, NULL, 'Active', 1, 0, '2019-04-09', 1, 1, '2019-04-09 10:00:29', '2019-04-09 10:00:29'),
(286, 1, 5, 'Wild Photographer 2', 'I have 5+ years experience in wild photography about birds update', '1210', 'This is my best photograph shot of product. Here you can see an amazing birtd which is starting fly update', 0, 2, NULL, NULL, NULL, 'Active', 1, 0, '2019-04-10', 1, 1, '2019-04-10 18:03:52', '2019-04-10 18:03:52'),
(287, 1, 14, 'Cosmetic HD Bridal Mackup', 'Products Used :\nFace:\nMakeupRevolution Ultra Sculpt & Contour Kit ( Ultra Fair C01)\n\nMusic : Official by YOUTUBE\n♥♥♥ THANK YOU SO MUCH FOR WATCHING ♥♥♥\nthe Balm Balm Voyage Vol. 2 pallete\ntheBalm Mary Lou Manizer Luminizer', '500', 'Products Used :\nFace:\nMakeupRevolution Ultra Sculpt & Contour Kit ( Ultra Fair C01)\n\nMusic : Official by YOUTUBE\n♥♥♥ THANK YOU SO MUCH FOR WATCHING ♥♥♥\nthe Balm Balm Voyage Vol. 2 pallete\ntheBalm Mary Lou Manizer Luminizer', 0, 2, 'facebook.com/test-fb', 'instagram.com/test-insta', 'twitter.com/twitter', 'Active', 1, 0, '2019-04-21', 1, 1, '2019-04-21 12:26:00', '2019-04-21 12:26:00'),
(288, 1, 4, 'Pop Music Band', 'music bio information', '500', 'Popup music details', 0, 1, NULL, NULL, NULL, 'Active', 1, 0, '2019-04-21', 1, 1, '2019-04-21 19:05:14', '2019-04-21 19:05:14'),
(289, 1, 13, 'Methamatics teacher', 'personal inforamation of mehamatics', '100', 'Methatmatics teacher', 0, 0, NULL, NULL, NULL, 'Active', 0, 0, '2019-04-21', 1, 1, '2019-04-21 19:43:32', '2019-04-21 19:43:32'),
(290, 1, 15, 'Classic feshion design', 'classic fesign design product', '500', 'classic fesign design product', 0, 1, NULL, NULL, NULL, 'Active', 1, 0, '2019-04-21', 1, 1, '2019-04-21 19:57:09', '2019-04-21 19:57:09'),
(291, 1, 4, 'This is a new Test', 'I\'m just an entrepreneur from the dirty south baby!', '1', 'Teach online and reach millions of students today. It\'s a great way to earn money, build your online following, and give back.', 0, 2, NULL, 'https://www.instagram.com/p/BwmVGY-DzkC/', NULL, 'Active', 1, 0, '2019-04-23', 1, 1, '2019-04-23 17:17:39', '2019-04-23 17:17:39');

-- --------------------------------------------------------

--
-- Table structure for table `talent_awards`
--

CREATE TABLE `talent_awards` (
  `id` int(11) NOT NULL,
  `talent_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `awards` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `talent_awards`
--

INSERT INTO `talent_awards` (`id`, `talent_id`, `user_id`, `awards`, `created_at`, `updated_at`) VALUES
(27, 174, 45, 1, '2018-07-27 07:36:14', '2018-07-27 07:36:14'),
(28, 174, 1, 1, '2018-07-30 05:27:33', '2018-07-30 05:27:33'),
(29, 170, 1, 1, '2018-08-07 12:37:38', '2018-08-07 12:37:38'),
(30, 198, 63, 1, '2018-08-14 12:05:04', '2018-08-14 12:05:04'),
(31, 198, 63, 1, '2018-08-14 12:49:39', '2018-08-14 12:49:39'),
(32, 173, 45, 1, '2018-08-14 15:03:13', '2018-08-14 15:03:13'),
(33, 186, 45, 1, '2018-08-14 18:44:08', '2018-08-14 18:44:08'),
(34, 178, 63, 1, '2018-08-16 05:34:29', '2018-08-16 05:34:29'),
(35, 187, 70, 1, '2018-08-16 13:09:24', '2018-08-16 13:09:24'),
(36, 181, 70, 1, '2018-08-16 13:10:13', '2018-08-16 13:10:13'),
(37, 174, 63, 1, '2018-08-16 13:10:43', '2018-08-16 13:10:43'),
(38, 204, 45, 1, '2018-08-21 01:53:22', '2018-08-21 01:53:22'),
(39, 192, 80, 1, '2018-08-22 09:29:25', '2018-08-22 09:29:25'),
(40, 180, 1, 1, '2018-08-24 01:44:21', '2018-08-24 01:44:21'),
(41, 204, 80, 1, '2018-08-28 09:08:33', '2018-08-28 09:08:33'),
(42, 213, 80, 1, '2018-08-28 09:09:56', '2018-08-28 09:09:56'),
(43, 174, 81, 1, '2018-08-29 10:52:00', '2018-08-29 10:52:00'),
(44, 212, 81, 1, '2018-08-29 10:52:29', '2018-08-29 10:52:29'),
(45, 210, 81, 1, '2018-08-29 10:52:58', '2018-08-29 10:52:58'),
(46, 212, 80, 1, '2018-08-29 13:46:38', '2018-08-29 13:46:38'),
(47, 235, 45, 1, '2018-09-09 02:59:27', '2018-09-09 02:59:27'),
(48, 260, 99, 1, '2018-09-11 12:28:12', '2018-09-11 12:28:12'),
(49, 260, 81, 1, '2018-09-11 13:32:26', '2018-09-11 13:32:26'),
(50, 218, 1, 1, '2018-09-14 02:46:29', '2018-09-14 02:46:29'),
(51, 262, 100, 1, '2018-09-14 13:53:27', '2018-09-14 13:53:27'),
(52, 204, 104, 1, '2018-10-12 13:21:24', '2018-10-12 13:21:24'),
(53, 224, 104, 1, '2018-10-12 13:24:17', '2018-10-12 13:24:17'),
(54, 224, 104, 1, '2018-10-12 13:24:17', '2018-10-12 13:24:17'),
(55, 224, 104, 1, '2018-10-12 13:24:17', '2018-10-12 13:24:17'),
(56, 250, 181, 1, '2018-12-04 14:15:10', '2018-12-04 14:15:10'),
(57, 235, 184, 1, '2018-12-15 14:14:09', '2018-12-15 14:14:09'),
(58, 234, 185, 1, '2018-12-27 05:38:40', '2018-12-27 05:38:40'),
(59, 235, 183, 1, '2019-01-16 11:23:15', '2019-01-16 11:23:15'),
(60, 235, 197, 1, '2019-03-16 07:22:08', '2019-03-16 07:22:08'),
(61, 173, 197, 1, '2019-03-16 08:04:12', '2019-03-16 08:04:12'),
(62, 234, 183, 1, '2019-03-22 07:32:34', '2019-03-22 07:32:34'),
(63, 234, 211, 1, '2019-03-26 06:59:22', '2019-03-26 06:59:22'),
(64, 173, 211, 1, '2019-03-26 09:05:47', '2019-03-26 09:05:47'),
(65, 276, 211, 1, '2019-03-26 09:27:48', '2019-03-26 09:27:48'),
(66, 274, 211, 1, '2019-03-26 09:29:11', '2019-03-26 09:29:11');

-- --------------------------------------------------------

--
-- Table structure for table `talent_categories`
--

CREATE TABLE `talent_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_image_path` varchar(1020) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_desc` varchar(2040) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_main_banner` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_banner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_detailed_banner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_detailed_icon_img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tarending_category_sidebar_icon` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `talent_categories`
--

INSERT INTO `talent_categories` (`id`, `name`, `category_image_path`, `category_desc`, `category_main_banner`, `category_banner`, `category_detailed_banner`, `category_detailed_icon_img`, `tarending_category_sidebar_icon`, `created_at`, `updated_at`) VALUES
(1, 'Author', 'talent-mall-category-images/talentmallpage-categ-Author.jpg', '<p>Whether it comes to being an established author or an aspiring one promoting and selling their works; \r\nwriting has never been easy for anyone. What if you find a platform where you can share your vision as well\r\nas your quality work to the audience around the globe? Future Starr empowers you with one of such high-end and seamless platform.</p> \r\n<p><strong>Promote and Sell Your Work</strong></p>\r\n<p>You can sell any type of books including Fiction, Non-fiction, Cook Books, Travel Books, Children’s Books, \r\nBusiness Books, Comic Books and so on. You just need to upload your work with your complete author profile and \r\nget ready for a fan community you often dreamed of. Just upload your literary work for once and deliver it often. </p>\r\n<p><strong>Approach for Audience</strong></p>\r\n<p>Through Future Starr, reach your global audience as soon as you are finished up with your work. We at, \r\nFuture Starr, know how much time and efforts you have to put in to create your work so we help you to accomplish \r\nreal verified sales for your books, through our massive platform.</p> \r\n<p><strong>Be A Market Pro</strong></p>\r\n<p>So Sign up with future Starr, upload your work, make a great impact and target and reach millions of \r\nreaders/audience worldwide. Our user-focused platform will help raise your sales and popularity.</p>', 'talent-mall-category-images/main-banners/Talent-mall-banner-Author.jpg', 'talent-mall-category-images/author.jpg', 'talent-mall-category-images/detailed/author-banner.jpg', 'talent-mall-category-images/detailed/author-banner3.jpg', 'talent-mall-category-images/icon/blog-author.png', '2017-11-06 21:35:01', '2017-11-06 21:35:01'),
(2, 'Entertainment', 'talent-mall-category-images/talentmallpage-categ-Entertainment.jpg', '<p>There is no scarcity of entertainers in the industry, however, the problem arises when they do not get real adequate opportunities to showcase their talent and get united with their audiences and fan community. Future Starr is the one of its kind and unique online marketplace that not only provides the actors with the opportunity to reach out to their audiences but also helps them to build their own online film business.  Just upload your profile and transform your presence.</p>\r\n\r\n<p><strong>Build Up Your Reputation</strong></p>\r\n\r\n<p>Sign up with Future Starr and boost up your reputation among your followers. Our extensive and well-built industry network will also help you in your business venture. Joining hands with Future Starr will authenticate you as a star in the market and as well help you to retain a loyal fan population.</p>\r\n\r\n<p><strong>Be Your Own Boss!</strong></p>\r\n\r\n<p>After you upload your profile on the portal, you can edit and update it from time to time. You can have access to top vendors and get connected with your valuable audiences all over the globe. Showcase your talent and maximize your growth.</p>', 'talent-mall-category-images/main-banners/Talent-mall-banner-Entertainment.jpg', 'talent-mall-category-images/entertenment.jpg', 'talent-mall-category-images/detailed/entertainment-banner.jpg', 'talent-mall-category-images/detailed/entertainment-banner3.jpg', 'talent-mall-category-images/icon/blog-entertainment.png', '2017-11-06 21:35:01', '2017-11-06 21:35:01'),
(4, 'Music', 'talent-mall-category-images/talentmallpage-categ-Music.jpg', '<p>Every music composer dreams of getting connected with millions of fans and build up a legacy… Join Future Starr community and let your music speak for you. We let you interact with a worldwide audience and build up a strong fan base just by uploading your music compositions. Future Starr will help you gain new fans and earn huge scales. Sign Up now and get connected with potential fans, like-minded people, and your future customers. With us, you can promote your music everywhere you want. Be a global star with Future Star.</p>\r\n\r\n\r\n<p><strong> Give Yourself A Breakthrough </strong></p>\r\n\r\n<p>We let the emerging musicians thrive by boosting up their creativity and build up a strong fan base. Whatever your genre may be; Punk, Rap, Hip Hop, EDM, Pop, Metal, Rock; just upload your music and promote yourself! </p>\r\n\r\n<p><strong>Boost Up Your Career</strong> </p>\r\n\r\n<p>Uploading your music on Future Starr will help you get connected to various labels, brands, publishers and to a wider fan community. Our industry tie-ups’ will help you access the global music industry. So Sign Up today to create a buzz for your music.</p>\r\n', 'talent-mall-category-images/main-banners/Talent-mall-banner-Music.jpg', 'talent-mall-category-images/music.jpg', 'talent-mall-category-images/detailed/music-banner.jpg', 'talent-mall-category-images/detailed/music-banner3.jpg', 'talent-mall-category-images/icon/blog-music.png', '2017-11-06 21:35:01', '2017-11-06 21:35:01'),
(5, 'Photography', 'talent-mall-category-images/talentmallpage-categ-Photography.jpg', '<p>Beauty lies in the eyes of the beholder. This saying can aptly define the fearless job of a photographer. Through his creativity, he can compel the world to see and perceive through his eyes. If you are such an arduous and zealous photographer with a unique collection of your own photos and you want to showcase, promote and sell those quality shots then Future Starr is a platform for you. Future Starr is a global marketplace for the photographers and artists to display, share and sell their images. Whatever your niche may be including travel, fashion, cityscapes landmarks, weddings, portraitures, wildlife or more; you just need to upload your passionate projects and turn it into hard cash.</p>\r\n\r\n<p><strong>Simplify Your Business </strong></p>\r\n\r\n<p>Our state-of-art and easy to use platform will help you to:\r\n<p>* Delight your customers </p>\r\n<p>* Help in growing your business</p>\r\n<p>* Get rewarded for your photography efforts</p>\r\n\r\n<p>Future Starr helps you to meet your clients’ expectations. </p>\r\n\r\n<p><strong> Make Yourself Visible </strong> </p>\r\n\r\n<p>Future Starr will help you to expand your audience and credibility through our photo sharing platform. You can also get connected to other creative professionals like designers, publishers, and marketing departments. </p>\r\n\r\n<p>So whether you are a professional photographer or an amateur, getting linked with Future Starr will ultimately turn your creativity into cash and earn you worldwide clients. </p>', 'talent-mall-category-images/main-banners/Talent-mall-bannePhotography.jpg', 'talent-mall-category-images/photography.jpg', 'talent-mall-category-images/detailed/phography-banner.jpg', 'talent-mall-category-images/detailed/phography-banner3.jpg', 'talent-mall-category-images/icon/blog-photography.png', '2017-11-06 21:35:01', '2017-11-06 21:35:01'),
(6, 'Comedy', 'talent-mall-category-images/talentmallpage-categ-Comedy.jpg', '\r\n<p>How to build up a strong fanbase is the biggest fear and challenge that every comedian must face. Various aspiring and promising comedians that were on the rise disappear every year. Organizing a stellar comedy event and having a great comedy timing is not enough to reach the audience. What you need is a platform that not only helps you to reach your target audience but also helps to expand your fanbase. Sign Up with Future Starr and get connected to the global audiences. </p>\r\n\r\n<p><strong>Market the Entertainer in You! </strong></p>\r\n\r\n<p>Upload your best works, events, video clips from your T.V. shows, podcasts and expand your fan community way beyond and be a global star. </p>\r\n\r\n<p><strong>Target Your Fans</strong></p>\r\n<p>Future Starr will give you the opportunity to create a fanbase who comes again and again to you. With us, convert your online audience into download purchases. </p>\r\n\r\n<p><strong>Advertise Yourself</strong></p>\r\n\r\n<p>You can also advertise your approaching events through Future Starr promote your comedy event and get the word out to the right people and make yourself successful. </p>', 'talent-mall-category-images/main-banners/Talent-mall-banne-Comedy.jpg', 'talent-mall-category-images/comedy.jpg', 'talent-mall-category-images/detailed/commedy-banner.jpg', 'talent-mall-category-images/detailed/commedy-banner3.jpg', 'talent-mall-category-images/icon/blog-comedy.png', '2017-11-06 21:35:01', '2017-11-06 21:35:01'),
(7, 'Model', 'talent-mall-category-images/talentmallpage-categ-Model.jpg', '<p>Striking visual presentations and charismatic online portfolios are absolute musts for models if they want themselves to get noticed. To create a high-quality online modeling portfolio and promote yourself with Future Starr, you want to make the difference and outshine in the modeling industry. And for this promotion, you need not spend big a fortune; just upload your best modeling videos and pictures and raise your audiences and visibility. Upload your photos depicting your different moods with diverse looks, backgrounds, and poses and get yourself noticed by a worldwide audience including; various editors, modeling agencies, bloggers, and fan community while earning extra income.</p>\r\n\r\n<p><strong>Flaunt Your Talent</strong></p>\r\n\r\n<p>With Future Starr, show your competency to your potential clients, scouts, fans and employers. Upload your various projects and photoshoots showcasing your lovely features and artistic vision and make your presence impressive.\r\n</p>\r\n<p><strong>Share Your Uniqueness</strong></p>\r\n\r\n<p>Share the details that are unique to you; like your eye color, skin tone or tattoos, height etc. and spice up your online modelling portfolio.</p>', 'talent-mall-category-images/main-banners/Talent-mall-banner-Model.jpg', 'talent-mall-category-images/modeling.jpg', 'talent-mall-category-images/detailed/model-banner.jpg', 'talent-mall-category-images/detailed/model-banner3.jpg', 'talent-mall-category-images/icon/blog-model.png', '2017-11-06 21:35:01', '2017-11-06 21:35:01'),
(8, 'Fitness', 'talent-mall-category-images/talentmallpage-categ-Fitness.jpg', '\r\n<p>Either you are a personal trainer or fitness guru and you are zealous to share your expertise by promoting yourself globally then you need a platform that can get you connected with the global audience and help you attract clients worldwide. Join hands with Future Starr! Get an edge over the competition and give a much-needed boost to your fitness training career. Just upload your instructional videos, or video tutorials showcasing your training sessions, nutrition or diet plans, group or customize programs offered and track your career progress in real-time. With Future Starr, you can market your services or fitness plans effortlessly. Also, you can earn huge fan community or clients and revolutionize your business.</p>\r\n\r\n<p><strong>Grow your Business</strong></p>\r\n\r\n<p>Future Starr will help you promote and sell your services of fitness products instantly. We help you get connected with a huge digital fitness community including other fitness trainers as well.</p>\r\n\r\n<p><strong>Stand Out from the Rest</strong></p>\r\n<p>Upload your video presentations, clients’ testimonials, your products’ lists, service packages, initial guidance videos or one-on-one training videos and earn immense profits along with a loyal fanbase.</p>', 'talent-mall-category-images/main-banners/Talent-mall-banner-Fitness.jpg', 'talent-mall-category-images/health.jpg', 'talent-mall-category-images/detailed/health-banner.jpg', 'talent-mall-category-images/detailed/health-banner3.jpg', 'talent-mall-category-images/icon/blog-fitness.png', '2017-11-06 21:35:01', '2017-11-06 21:35:01'),
(9, 'National Geographic', 'talent-mall-category-images/talentmallpage-categ-National-Geographic.jpg', '	<p>Whether you are a dedicated environmental activist, start-up, or an international organization that wants to create a buzz in the society about environmental issues like global warming, saving oceans, nuclear testing or many other threats, Join hand with Future Starr to make mother earth a greener, fairer, and environmentally sustainable place to live.  To promote your ideas you just need to upload videos of your campaigns, your investigation, and the solutions you want to get executed. Empower yourself or your organization with the collective power audience or people you reach through Future Starr.</p>\r\n\r\n<p><strong>Get Connected </strong></p>\r\n\r\n<p>Future Starr enables you to get connected with the global audience. You can create public awareness and can encourage more and more people to donate to noble causes. You can promote the research you have made and your educational programs and get linked up with a like-minded community of experts, scientists and researchers. </p>\r\n\r\n<p><strong>Create a Global Platform</strong></p>\r\n\r\n<p>Upload videos of your training sessions, meetings, rallies or marches and engage the global audience to create awareness about various environmental issues and attract non-profit organizations, or other activists who can support you. This can also help you raise donations for your campaign. Stop the environmental destruction through direct communication with a world audience.</p>', 'talent-mall-category-images/main-banners/Talent-mall-banner-National-Geographic.jpg', 'talent-mall-category-images/nationalgeography.jpg', 'talent-mall-category-images/detailed/national-geography-banner.jpg', 'talent-mall-category-images/detailed/national-geography-banner3.jpg', 'talent-mall-category-images/icon/blog-ng.png', '2017-11-06 21:35:01', '2017-11-06 21:35:01'),
(10, 'Science', 'talent-mall-category-images/talentmallpage-categ-Science.jpg', '<p>Science brings into minds the wacky physics equations, bubbling analysis devices, telescope heredity, ecology, and many more puzzling ideas. If you are a geek at all these and want to provide virtual teachers aid to parents or students; Future Starr is the platform. Through us, we offer design-board learning strategies, lesson plans, illustrations, to make the student understand various scientific concepts. Upload your lesson plans related to biology, philosophy, earth science, astronomy or anthropology.\r\n</p>\r\n<p><strong>Provide Hands-on Experience</strong></p>\r\n\r\n<p>Future Starr let you provide online hand on experience to the students. You can upload animations or quizzes that you have developed related to different topics of science and keep your clients or students entertained and engaged as well.</p>\r\n\r\n<p><strong>Upload Your Classroom Teaching</strong></p>\r\n\r\n<p>Upload your actual classroom teaching visual clips or experiments in biology, chemistry, engineering or you can also upload charts graphs, or tables to provide detailed knowledge about various scientific concepts.  Future Starr enables you to provide a virtual lab to your students. Be accessible and searchable with Future Starr.</p>', 'talent-mall-category-images/main-banners/Talent-mall-banner-Science.jpg', 'talent-mall-category-images/scince.jpg', 'talent-mall-category-images/detailed/science-banner.jpg', 'talent-mall-category-images/detailed/science-banner3.jpg', 'talent-mall-category-images/icon/blog-scince.png', '2017-11-06 21:35:01', '2017-11-06 21:35:01'),
(11, 'Food', 'talent-mall-category-images/talentmallpage-categ-Food.jpg', '<p>If you are food enthusiasts and want to be a tomorrow’s trendsetter in the culinary industry through your exquisite tempting dishes, get connected with Future Starr. Promote and share your culinary expertise with a like-minded community of cooks, culinary fans, and food lovers. Just upload your video clips, cooking tutorials, share the pictures of your recipes, food items or other food-related activities, and Future Starr will provide you a platform where you can share, promote and interact with your fan community. Not only chefs even the restaurants can market themselves through our diversified platforms. </p>\r\n\r\n<p><strong>Advertise Your Skills</strong></p>\r\n<p>Through Future Starr, you can exhibit your culinary skills to not only to your fan community but also to culinary professionals including restaurant owners, hoteliers, executive chefs or sommeliers.</p>\r\n\r\n<p><strong>Grow with Future Starr</strong></p>\r\n\r\n<p>Create galleries through pictures of your recipes  video tutorials and reach worldwide food enthusiasts. Raise your professional culinary standards with Future Starr’s digital credential and realize your full potential.</p>', 'talent-mall-category-images/main-banners/Talent-mall-banner-Food.jpg', 'talent-mall-category-images/food.jpg', 'talent-mall-category-images/detailed/food-banner.jpg', 'talent-mall-category-images/detailed/food-banner3.jpg', 'talent-mall-category-images/icon/blog-food.png', '2017-11-29 13:00:00', '2017-11-29 13:00:00'),
(12, 'Nutrition', 'talent-mall-category-images/talentmallpage-categ-Nutrition.jpg', '<p>Whether you are a registered dietitian, nutritionist, registered dietetic technician, or any other registered dietetic professional and you are committed to shaping the very core of your clients while positively influencing their health then Future Starr is the platform that can help you to accomplish your goal. With Future Starr, you can target your specialized audience by uploading your blogs, or videos to share your healthy cooking recipes or your opinions about some important nutrition topics. Share your visually creative outlet to promote yourself. Keep your clients engaged with your beautifully displayed healthy recipes.</p>\r\n\r\n<p><strong>Forward Your Expert Opinion</strong></p> \r\n\r\n<p>Future Starr enables the nutritionists and dietitians to forward their views on foods, environmental issues, organic foods, science-based nutrition information or healthy living topics. </p>\r\n\r\n <p><strong>Guide Clients to Better Nutrition </strong></p>\r\n<p>With Future Starr advertising your nutritional plans, your health packed recipes can promote your views regarding various issues including weight control, heart health diabetes or reducing stress. We also help you to get connected with other nutrition experts and health professionals.</p>', 'talent-mall-category-images/main-banners/Talent-mall-banner-Nutrition.jpg', 'talent-mall-category-images/nutrition.jpg', 'talent-mall-category-images/detailed/Nutrition-banner.jpg', 'talent-mall-category-images/detailed/Nutrition-banner3.jpg', 'talent-mall-category-images/icon/blog-nutrition.png', '2017-11-29 13:00:00', '2017-11-29 13:00:00'),
(13, 'Mathematics', 'talent-mall-category-images/talentmallpage-categ-Mathematics.jpg', '<p>If you are a calculus or trigonometry Wizz or an expert in writing tricky math problems and want to be more accessible to your students, get connected with Future Starr. We will get you connected with the people who need math experts to teach them. Just upload your tutoring videos showcasing your expertise and be a part of the best online marketplace. Express your math knowledge and skills with Future Starr and get rid of costly advertisements.</p>\r\n\r\n\r\n<p><strong>Direct Communication</strong></p>\r\n<p>By uploading your videos on Future Star, you can establish an easy and direct communication with your students worldwide. Just upload your availability along with your tutoring profile and experience new ways of online tutoring.</p>\r\n\r\n\r\n<p><strong>Earn Real Results</strong></p>\r\n\r\n<p>Future Starr enables you to advertise your skills anywhere in the world or in your country while never leaving your place or home and be a member of a vetted experts’ community. Just upload your work and help the students to understand the importance of math in a better and more intellectual way.</p>', 'talent-mall-category-images/main-banners/Talent-mall-banner-Mathematics.jpg', 'talent-mall-category-images/methamatics.jpg', 'talent-mall-category-images/detailed/mathematics-banner.jpg', 'talent-mall-category-images/detailed/mathematics-banner3.jpg', 'talent-mall-category-images/icon/blog-mathematics.png', '2017-11-29 13:00:00', '2017-11-29 13:00:00'),
(14, 'Cosmetics', 'talent-mall-category-images/talentmallpage-categ-Cosmetics.jpg', '<P>If you dream of beauty and want to establish yourself as a beauty trailblazer in the market through your innovative and revolutionary beauty retail products, sign up with Future Starr. Future Starr will help you make a powerful presence around the world. Upload your range of beauty products and expose them to global clients or merchandising teams as well. You can directly interact with them through our platform. </p>\r\n\r\n<p><strong>Grow With US</strong></p>\r\n\r\n<p>Whether you deal in cosmetics, fragrance, skin care products, hair care products or salon services; linking up with Future Starr will earn your ultimate loyal fanbase and clientele. Showcase the world the beauty service you provide or exclusive offers they can get or engross them in your products that they will love and enjoy using.</p>\r\n\r\n<p><strong>Globalize your Business</strong></p>\r\n\r\n<p>With Future Starr creates a worldwide network of beauty through your unique and exotic beauty products. We give you the opportunity to develop a worldwide market for your new beauty products which in return will also grow your business at the global level. Future Starr fulfills your dreams and also make the dreams of your clients come true.</p>', 'talent-mall-category-images/main-banners/Talent-mall-banner-Cosmetics.jpg', 'talent-mall-category-images/cosmetics.jpg', 'talent-mall-category-images/detailed/cosmetics-banner.jpg', 'talent-mall-category-images/detailed/cosmetics-banner3.jpg', 'talent-mall-category-images/icon/blog-cosmetics.png', '2017-11-29 13:00:00', '2017-11-29 13:00:00'),
(15, 'Fashion designer', 'talent-mall-category-images/talentmallpage-categ-Fashion-Designer.jpg', '<p>If you have a fashion brand with original or unique concept designs while focusing on detailed refinements to satisfy your customers; Join hands with Future Starr. We at Future Starr provide you with bold solutions that not only allow you to stand apart from the crowd but also helps you to reach a much wider and global clientele. We offer you the opportunity to provide your customers with the same care and one-on-one service online as they get on the stores and began their shopping experience. Upload your products related videos, infographics, photos and improve your customers’ experience.\n</p>\n\n<p><b>Go An Extra Mile</b></p>\n\n<p>Through Future Starr, you can bring your calculations to the customers all over the world while providing them personalized service. Upload your clothing’ or your catalogs, displaying size and colors available and offer bespoke service to your customers. </p>\n\n<p><b>Launch your Brand with Future Starr</b></p> \n\n<p>Upload your business campaigns, share blog posts or user guides to update your customers about new dressing trends or lifestyles. Share the competitions you organized or celebrations you made and make your customers your partners and earn a loyal fanbase.</p>', 'talent-mall-category-images/main-banners/Talent-mall-banner-Fashion-designer.jpg', 'talent-mall-category-images/fashion.jpg', 'talent-mall-category-images/detailed/fashion-banner.jpg', 'talent-mall-category-images/detailed/fashion-banner3.jpg', 'talent-mall-category-images/icon/blog-fashion.png', '2017-11-29 13:00:00', '2017-11-29 13:00:00'),
(16, 'Tattoo Artist', 'talent-mall-category-images/talentmallpage-categ-tattoo.jpg', '<p>If you are a certified, professionally trained and licensed tattoo artist, who are determined to build and improve their tattoo art along with wanting to raise a reputable platform for their art, then Future Starr is your ultimate destination. Here we would facilitate your first steps in your journey towards success. Future Starr helps the tattoo artists, studios, and conventions as well to work and operate more efficiently. Future Starr is a one-stop destination for tattoo art and lifestyle. We provide artists with the opportunity to get connected with new clients as well as help them to find new tattoo ideas by linking them up with another artist for their business growth.\r\n</p>\r\n\r\n<p><strong>Revolutionize Your Tattoo Space</strong></p>\r\n\r\n<p>Through Future Starr, you can get connected with reputable tattoo studios, can book your consultations easily and that too around the world. Our global platform will bring you closer and as well make you accessible to the global audience.</p>\r\n\r\n<p><strong>Let Your Talent All Hang Out</strong></p>\r\n\r\n<p>Upload your curated art galleries, articles, blogs, client’s reviews, artists’ recommendations, tattoo making videos and let the world know that tattoo art is much higher than ink on skin and let them appreciate and share your tattoo art.</p>', 'talent-mall-category-images/main-banners/talentmallpage-tattos.jpg', 'talent-mall-category-images/tattoo.jpg', 'talent-mall-category-images/detailed/Tattoo-Artist-banner.jpg', 'talent-mall-category-images/detailed/tatoo_image.jpg', 'talent-mall-category-images/icon/blog-tattoo.png', '2017-11-29 13:00:00', '2017-11-29 13:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `talent_comments`
--

CREATE TABLE `talent_comments` (
  `id` int(11) NOT NULL,
  `talent_id` int(11) NOT NULL,
  `buyer_id` int(11) NOT NULL,
  `comment` varchar(225) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `talent_downloads`
--

CREATE TABLE `talent_downloads` (
  `id` int(11) NOT NULL,
  `talent_id` int(11) NOT NULL,
  `downloaded_by` int(11) NOT NULL,
  `daily_download` varchar(225) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `talent_favourites`
--

CREATE TABLE `talent_favourites` (
  `id` int(11) NOT NULL,
  `talent_id` int(11) NOT NULL,
  `buyer_id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `upadted_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `talent_multimedia`
--

CREATE TABLE `talent_multimedia` (
  `id` int(10) UNSIGNED NOT NULL,
  `talent_id` int(10) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `media` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `talent_orders`
--

CREATE TABLE `talent_orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `talent_id` int(10) UNSIGNED NOT NULL,
  `payment_id` int(10) UNSIGNED NOT NULL,
  `shipping_id` int(10) UNSIGNED NOT NULL,
  `total` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `talent_ratings`
--

CREATE TABLE `talent_ratings` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `talent_id` int(11) NOT NULL,
  `rating` int(225) DEFAULT NULL,
  `buyer_comment` varchar(1024) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `talent_ratings`
--

INSERT INTO `talent_ratings` (`id`, `user_id`, `talent_id`, `rating`, `buyer_comment`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(12, 45, 173, 5, 'A message is a discrete unit of communication intended by the source for consumption by some recipient or group of recipients. A message may be delivered by various means, including courier, telegraphy, carrier pigeon and electronic bus. A message can be the content of a broadcast. An interactive exchange of messages forms a conversation.\nThere are two main senses of the word "message" in computing: messages between the human users of computer systems that are delivered by those computer systems, and messages passed between programs or between components of a single program, for their own purposes.', '2018-07-27 13:07:33', '2018-09-13 13:22:11', 45, 45),
(13, 45, 174, 3, 'This product is amazing!', '2018-08-07 13:10:27', '2018-08-13 17:30:58', 45, 45),
(14, 81, 209, 4, NULL, '2018-08-20 10:10:49', '2018-08-24 09:31:23', 81, 81),
(15, 63, 193, 4, NULL, '2018-08-21 10:27:23', '2018-08-21 10:27:50', 63, 63),
(16, 63, 208, 5, 'music 2', '2018-08-22 05:49:34', '2018-08-22 05:49:34', 63, 63),
(17, 63, 197, 4, 'fit', '2018-08-22 05:51:08', '2018-08-22 05:51:08', 63, 63),
(18, 81, 192, 2, '2 Star', '2018-08-24 09:31:29', '2018-09-08 12:27:44', 81, 81),
(19, 81, 212, 3, '3 STAR', '2018-08-29 10:22:48', '2018-09-08 12:27:53', 81, 81),
(20, 81, 219, 5, NULL, '2018-08-29 10:22:58', '2018-08-29 10:22:58', 81, 81),
(21, 81, 210, 4, '1', '2018-08-29 10:50:27', '2018-08-29 10:50:27', 81, 81),
(22, 90, 240, 4, NULL, '2018-08-31 10:13:50', '2018-08-31 10:13:50', 90, 90),
(23, 92, 239, 5, NULL, '2018-09-03 10:29:04', '2018-09-03 10:32:06', 92, 92),
(24, 92, 212, 3, NULL, '2018-09-03 10:32:16', '2018-09-03 10:32:16', 92, 92),
(25, 81, 176, 4, '4 STAR', '2018-09-08 12:27:32', '2018-09-08 12:28:01', 81, 81),
(26, 99, 243, 4, '4 Star', '2018-09-11 11:20:59', '2018-09-11 11:20:59', 99, 99),
(27, 81, 260, 4, 'buyeripad', '2018-09-11 13:31:34', '2018-09-11 13:31:34', 81, 81),
(28, 99, 260, 5, '5satrr', '2018-09-11 13:32:06', '2018-09-14 06:23:54', 99, 99),
(29, 99, 236, 5, '5 starr', '2018-09-14 10:23:01', '2018-09-14 10:23:01', 99, 99);

-- --------------------------------------------------------

--
-- Table structure for table `talent_riders`
--

CREATE TABLE `talent_riders` (
  `id` int(11) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `talent_id` varchar(255) NOT NULL,
  `rider` int(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `talent_riders`
--

INSERT INTO `talent_riders` (`id`, `user_id`, `talent_id`, `rider`, `created_at`) VALUES
(2, '45', '173', 1, '2018-08-20 16:22:57'),
(3, '45', '176', 1, '2018-08-21 05:18:35'),
(4, '45', '187', 1, '2018-08-21 05:23:22'),
(5, '81', '178', 1, '2018-08-21 05:24:27'),
(6, '81', '187', 1, '2018-08-21 05:24:42'),
(7, '80', '181', 1, '2018-08-21 07:05:41'),
(8, '80', '202', 1, '2018-08-21 07:06:03'),
(9, '80', '186', 1, '2018-08-21 07:06:29'),
(10, '80', '197', 1, '2018-08-21 07:07:17'),
(11, '63', '197', 1, '2018-08-22 05:51:53'),
(12, '80', '209', 1, '2018-08-22 08:44:53'),
(13, '45', '211', 1, '2018-08-23 10:18:21'),
(14, '1', '180', 1, '2018-08-24 01:43:56'),
(15, '81', '212', 1, '2018-08-24 08:56:53'),
(16, '80', '204', 1, '2018-08-28 09:09:36'),
(17, '80', '213', 1, '2018-08-28 09:10:03'),
(18, '80', '174', 1, '2018-08-29 14:38:58'),
(19, '81', '174', 1, '2018-08-30 06:05:21'),
(20, '1', '212', 1, '2018-08-31 03:27:51'),
(21, '90', '212', 1, '2018-08-31 13:38:56'),
(22, '92', '174', 1, '2018-09-03 13:21:13'),
(23, '92', '239', 1, '2018-09-03 13:22:18'),
(24, '1', '240', 1, '2018-09-04 03:05:54'),
(25, '1', '174', 1, '2018-09-05 03:20:53'),
(26, '80', '217', 1, '2018-09-07 07:16:25'),
(27, '45', '235', 1, '2018-09-09 02:59:31'),
(28, '91', '203', 1, '2018-09-12 13:38:09'),
(29, '98', '203', 1, '2018-09-12 13:38:40'),
(30, '45', '219', 1, '2018-09-14 12:36:27'),
(31, '1', '219', 1, '2018-09-14 12:37:31'),
(32, '100', '262', 1, '2018-09-14 13:53:36'),
(33, '104', '204', 1, '2018-10-12 13:34:15'),
(34, '181', '250', 1, '2018-12-04 14:16:32'),
(35, '185', '234', 1, '2018-12-27 05:38:26'),
(36, '1', '250', 1, '2018-12-27 06:00:20'),
(37, '183', '250', 1, '2019-03-15 13:00:14'),
(38, '183', '250', 1, '2019-03-15 13:00:15'),
(39, '197', '235', 1, '2019-03-16 07:16:59'),
(40, '197', '173', 1, '2019-03-16 08:03:49'),
(41, '211', '276', 1, '2019-03-26 10:00:01');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `display_name` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paypal_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stripe_acc` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profile_pic` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `provider_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `provider_token` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_verified` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no',
  `vacation_mode` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'off',
  `experience_level` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `visibilty` varchar(225) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Public',
  `city` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip_code` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `auto_reply` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1- Auto Reply ON, 0- Auto reply OFF',
  `automatic_message` longtext COLLATE utf8_unicode_ci,
  `counter` int(100) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `first_name`, `last_name`, `username`, `display_name`, `phone`, `address`, `email`, `paypal_email`, `stripe_acc`, `profile_pic`, `password`, `remember_token`, `provider_id`, `provider`, `provider_token`, `email_verified`, `vacation_mode`, `experience_level`, `description`, `visibilty`, `city`, `state`, `zip_code`, `created_by`, `updated_by`, `created_at`, `updated_at`, `auto_reply`, `automatic_message`, `counter`) VALUES
(1, 4, 'Five', 'Seller', 'Stars band', 'five display name', '56578661', '156 mayaur nagar1', 'five_seller@fiveexceptions.com', 'kelly@paypal.com', NULL, 'userImage/1545890346.Club vegas.PNG', '$2y$10$c2Jx8aBIS0E0JCUi5HUro.0TbjacQ9QV91334BiK.fmlcUVpjE0Xm', '3ZZ3G7EoOhcvZs9VVLjXJGUy4jGrLayIV2LjEzu8XaiaR0wwJJn23VOsSW8w', NULL, NULL, NULL, 'yes', 'off', 'Entermediate', NULL, 'Private', NULL, NULL, NULL, 0, 0, '2017-11-07 04:18:07', '2018-02-05 08:04:31', 0, 'Automatic message system', NULL),
(44, 4, 'anu', 'Agrawal', 'ANU', NULL, NULL, NULL, 'meghana.singhal@zehntech.com', 'meghana.singhal@zehntech.com', NULL, NULL, '$2y$10$c2Jx8aBIS0E0JCUi5HUro.0TbjacQ9QV91334BiK.fmlcUVpjE0Xm', '2DefUf8TYuuR6LZQUaMRC07aTxPQ8oEWrqvvAqG97FuvdDfQF5unL9BUwMWl', NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-02-01 08:14:32', '2018-02-05 23:48:40', 0, NULL, NULL),
(45, 3, 'fivessssss', 'buyers', 'fivebuyer1', 'John Smithaaaaaaa', '452001', 'palasiaaaaaazzzzzzz', 'five@fiveexceptions.com', NULL, NULL, 'userImage/1540619122.emails-on-a-laptop-screen.jpg', '$2y$10$c2Jx8aBIS0E0JCUi5HUro.0TbjacQ9QV91334BiK.fmlcUVpjE0Xm', 'JuLndIIwDlzhOhNI5iRZOY4Y8YZsmHqeqNw6b26b5qqFJUhWlNWTWF4MrGrv', NULL, NULL, NULL, 'yes', 'off', NULL, 'helloo asdadsasdasdsd', 'Public', 'indore', 'mp1545121', '452001', NULL, NULL, '2018-03-16 03:06:51', '2018-04-30 01:26:43', 1, 'Send automatic replies to Incoming messages', NULL),
(46, 3, 'Piyush', 'Rathod', 'piyushrathod', NULL, NULL, NULL, 'pshrathod96@gmail.com', NULL, NULL, NULL, '$2y$10$RsYvMkWykWXpYEBcnmfyw.FoSv5ishmYEh6oV3.DVd9KEdC4EN8R6', NULL, NULL, NULL, NULL, 'no', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-03-16 03:38:54', '2018-03-16 03:38:54', 0, NULL, 4),
(47, 3, 'Sherlok', 'Holmes', 'sherlockholmes', NULL, NULL, NULL, 'sherlockholmes@zehntech.com', NULL, NULL, NULL, '$2y$10$S7owdJR8kZqYeLRD87bl6eEh1ae8O.Gqn0lNSYyGy5eoKMt9u.UjG', 'gySiP3mTC83zpecTAWzL9xZjuuieBzimn3K5ky1uwmGPISTVGBQDIjKlCRoa', NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-04-04 04:47:40', '2018-04-04 04:47:40', 0, NULL, NULL),
(85, 4, 'praveen', NULL, 'praveen patel', NULL, NULL, NULL, 'praveen.5exceptions@gmail.com', NULL, NULL, NULL, '$2y$10$1LIZQTroukTYKK2mQ4yU4eyGWYxvOPSSc6emqIiJjiVmo3wOoLskq', 'Pnr54g9KWDgScq9yNYiIJHjzvRJzfTmpzXMuirKIH8x3Q0jUoMW0rnmbfsLn', '102563515560862416038', 'GOOGLE', NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-08-29 09:21:50', '2018-08-29 09:21:50', 0, NULL, NULL),
(94, 3, 'Roshan', 'parmar', 'testing122', NULL, NULL, NULL, 'roshan@5exceptions.com', NULL, NULL, NULL, '$2y$10$0Q7k4i6OMAp8ISfKQjsx8.9A1xNHoB73ltr6fM5l/uW3un07BiR8W', NULL, NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-09-08 06:59:40', '2018-09-08 07:06:58', 0, NULL, NULL),
(103, 4, 'Tiya', 'Pol', 'Tiya Pol', NULL, NULL, NULL, 'tester.apk3@yahoo.com', NULL, NULL, NULL, '$2y$10$PNJHQ0KYbD3mqS.99F/.yewMLoB8QKsqNaRzIkeTjk1tnpA14bEZ2', NULL, '307980756451616', 'FACEBOOK', NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-10-11 05:31:10', '2018-10-11 05:31:10', 0, NULL, NULL),
(112, 3, 'Qqwwt', 'Tester', 'Qqwwt Tester', NULL, NULL, NULL, 'tester.apk15@gmail.com', NULL, NULL, NULL, '$2y$10$DDHWXtMVGO5MF1BS7Hq6Buxrfni5JzpSWHN4MvPzmhkXXpThCZb6S', NULL, '240322063509512', 'FACEBOOK', NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-10-15 05:27:27', '2018-10-15 05:27:27', 0, NULL, NULL),
(113, 3, 'Karan', 'Sharma', 'qwerty18', 'Karan Sharma', NULL, NULL, '5exception.test@gmail.com', NULL, NULL, 'userImage/1539586794.comedy 1.jpeg', '$2y$10$O8N..BbwaHQstyLR6uCIBuDPVP5rbAIDsal6uDUNRsRaYmFq5DkNi', NULL, NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-10-15 06:16:59', '2018-10-15 06:18:08', 1, '123', NULL),
(114, 4, 'Preston', 'Lux', 'planetheaven', NULL, NULL, NULL, 'preston.lux1@gmail.com', 'prestonlux@rocketmail.com', NULL, NULL, '$2y$10$CcIpSIcH7QCLik7LC2PZGOczj1H4WGSou9qerD7IJeDGBNosKjf0C', NULL, NULL, NULL, NULL, 'no', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-10-16 05:34:44', '2018-10-16 05:34:44', 0, NULL, 4),
(115, 3, 'Charlie', 'Hooley', 'charliehooley', NULL, NULL, NULL, 'charlieehooley@gmail.com', NULL, NULL, NULL, '$2y$10$iQ.MkhCeJi2nE65SbXEOcuw2sP.myFz3DSABvUTzknxrJgmNDyYz6', NULL, NULL, NULL, NULL, 'no', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-10-16 18:47:31', '2018-10-16 18:47:31', 0, NULL, 4),
(118, 3, 'abhishek', 'rathore', 'adr12123534', NULL, NULL, NULL, 'abhsuuuu@nada.ltd', NULL, NULL, NULL, '$2y$10$PD05M3iOX7PIOyz6iPPTleAPZ041sqiZpM.BPXDYURorai0LY4ZTa', NULL, NULL, NULL, NULL, 'no', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-10-18 12:47:13', '2018-10-18 12:47:13', 0, NULL, 4),
(123, 3, 'abc', 'def', 'abhi123456789', NULL, NULL, NULL, 'surbhit143143@gmail.com', NULL, NULL, NULL, '$2y$10$MGzE5yECfPBFf0KFyAzdyeUO1rXFP9F6AdWvZDGMb5f1INYNWEFIG', NULL, NULL, NULL, NULL, 'no', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-10-18 13:09:35', '2018-10-18 13:09:35', 0, NULL, 4),
(124, 3, 'praveen', 'patel', 'pravi', 'Praveen', '9876543210', 'Indore', 'praveen.patel@5exceptions.com', NULL, NULL, 'userImage/1539870794.arrow.png', '$2y$10$9o0kpIQ2EhOLIFQqd662vOStBTVpFsz8VNzQAjilXuS2CIvQ6zrsa', 'T1hvAwEmAhH9FaHL8iIKJQyIqqdiRQCT7xZSBGX7MUblMoWDFMSf9DdrdUPB', NULL, NULL, NULL, 'yes', 'off', NULL, 'Bio', 'Public', 'Indore', 'MP', '452001', NULL, NULL, '2018-10-18 13:14:25', '2018-10-18 13:14:39', 0, NULL, NULL),
(125, 3, 'Testing', 'Testing', 'testingroshan', 'asdasdasd', '7697307036', 'asdsadasdsad', 'elanuwutez@banit.club', NULL, NULL, 'userImage/1539871422.WIN_20161021_19_32_24_Pro.jpg', '$2y$10$lo46jlta8yc4Oo2zE07tXeJ61tSp9oYBJlmYy/PRFmIg/5DqlNPH2', 'HdgQtvBFJsUCJoXcDTpITfYvI8IAtl5vpwh48o2p4doY9BXnodLmrxddMxc4', NULL, NULL, NULL, 'yes', 'off', NULL, 'asdasdasd', 'Public', 'Ideavate', 'Padliya', '452001', NULL, NULL, '2018-10-18 13:14:38', '2018-10-18 13:21:32', 0, NULL, NULL),
(127, 4, 'aa', 'aa', '21565a45d5', NULL, NULL, NULL, 'jijaz@nada.ltd', 'jijaz@nada.ltd', NULL, NULL, '$2y$10$xcb7z0.y0GfdMsbQLy7Y2./uvzPim4Zy07ziZc5aypYqcIfAdh/4i', NULL, NULL, NULL, NULL, 'no', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-10-18 13:27:55', '2018-10-18 13:27:55', 0, NULL, 4),
(137, 3, 'aaa', 'aaa', 'aaa545454', NULL, NULL, NULL, 'z@gmail.com', NULL, NULL, NULL, '$2y$10$9VdA30nx/etWZFRXhh5e0eiO94LzHgHIpBebLiK45JrdS/VRC1MD6', NULL, NULL, NULL, NULL, 'no', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-10-23 14:01:32', '2018-10-23 14:01:32', 0, NULL, NULL),
(140, 4, 'abc', 'abc', 'abhi12345678', NULL, NULL, NULL, 'abs@gmail.com', 'abs@gmail.com', NULL, NULL, '$2y$10$pvyvJthGNu9ZIEZLUUfQz.RwPhcMX.VCnhcXCeDRSkonrCGJeQCAy', NULL, NULL, NULL, NULL, 'no', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-10-24 05:05:03', '2018-10-24 05:05:03', 0, NULL, 4),
(141, 4, 'a', 'a', 'a56412', NULL, NULL, NULL, 'akkira576@gmail.comm', 'akkira576@gmail.comm', NULL, NULL, '$2y$10$B8ytVwGxsHi6CMOKx09yZe2qBPzxoKEmIa0GaQYsJdkj2pWFGpV4G', NULL, NULL, NULL, NULL, 'no', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-10-24 05:11:31', '2018-10-24 05:11:31', 0, NULL, 4),
(146, 4, 'BillionaireDiamonds', 'Tycoon', 'BillionaireDiamonds888', NULL, NULL, NULL, 'mbymonisha@gmail.com', 'ladyravenm@gmail.com', NULL, NULL, '$2y$10$nOwQLt9GIxqs3bWc.ADld.Xzy49ygfuN50Q1siFylG7YJ39cwHLb2', NULL, NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-10-26 08:29:05', '2018-10-26 08:29:45', 0, NULL, 0),
(155, 3, 'Abhishek', 'Rathore', 'Abhishek Rathore', 'aaaa', NULL, NULL, 'abhishekar81@yahoo.co', NULL, NULL, 'userImage/1540649621.emails-on-a-laptop-screen.jpg', '$2y$10$0qvx8x.xPpxJEnd5EP4FA.ohtxkMqyXR40cb5.t0/UtwZd0NW5p/m', 'g8rAtbn1E1qia6rLIzX6ZiCkSDLFl6Iuo6vuEZXPz3ndZb3xgyjriOcoH1BI', '1918143211612053', 'FACEBOOK', NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-10-27 06:50:01', '2018-10-27 06:50:01', 0, NULL, 0),
(160, 3, 'Chanachal', 'Gupta', 'Chanachal Gupta', 'helloo', '8817034140', '16 bijali nagar', 'akkira576@gmail.com', NULL, NULL, 'userImage/1540626632.emails-on-a-laptop-screen.jpg', '$2y$10$Nu1RXmLOVSsfl1/e1FVwJuMFa0fh2YOqaIbWVskSZtSiywA/PWt4S', 'arhoEYjy4Km2shWIYBFX8TV05unG439kPuzMqdmZokZTky8dger7XU6sJUZR', '178987793019409', 'FACEBOOK', NULL, 'yes', 'off', NULL, '1234', 'Public', 'indore', 'mp', '452016', NULL, NULL, '2018-10-27 07:48:42', '2018-10-27 07:48:42', 0, NULL, 0),
(161, 4, 'John', 'Ellis', 'JohnLEllis', NULL, NULL, NULL, 'n8awais@aol.com', 'n8awais@aol.com', NULL, NULL, '$2y$10$yRNoEDlN30yVNwv9BuxSretYtoXR1AnVdFcD6zO9lHnezBN9tnUUO', NULL, NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-10-31 16:21:17', '2018-10-31 16:38:11', 0, NULL, 0),
(162, 3, 'Steve', 'Ambrose', 'drtunes', NULL, NULL, NULL, 'ambroses@me.com', NULL, NULL, NULL, '$2y$10$ewLGPS4w.OiRkmDHcDHOQ.PltBIHYnQA9I5eRjg.A/xw9XmxE3C7y', NULL, NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-10-31 17:27:01', '2018-10-31 17:29:22', 0, NULL, 0),
(163, 4, 'Arnold', 'Stolting', 'stoltingmediagroup', 'Stolting Media Group', '4163581251', '5 DRIFTWOOD CRESCENT.', 'info@stoltingmediagroup.com', 'info@stoltingmediagroup.com', NULL, 'userImage/1541007897.BIZZCARD-SQUARE-FRONT-FX.png', '$2y$10$BqV4tvvbtg3eu3SULTEcYehwAiSV3bDXENAYYtU8gAevPGje07WZW', NULL, NULL, NULL, NULL, 'yes', 'off', 'Expert', NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-10-31 17:42:31', '2018-10-31 17:42:57', 0, NULL, 0),
(164, 4, 'Peter', 'Britt', 'TheCowboyandTheViking', NULL, NULL, NULL, 'peterbritt129@aol.com', 'peterbritt@peterbritt.us', NULL, NULL, '$2y$10$lFUi8MQVzb4BV.sJoa9dj.FxRw.sFdrytihbkeSGjGdhY3My5/Rhm', NULL, NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-10-31 20:48:01', '2018-10-31 20:49:08', 0, NULL, 0),
(165, 3, 'abhishek', 'rathore', 'adr123', NULL, NULL, NULL, 'abhishekar5769@gmail.com1', NULL, NULL, NULL, '$2y$10$psEVVAgFbZzMbFvFPsxTvetrzi4uCcXIv2hjeuzvIoHcXaLSHksPC', 'wz1czO7SGZXlUXJTtHTB260SS8uNjqEgk7kGcMMyAAKDpItHzVG6AflE54qR', NULL, NULL, NULL, 'no', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-11-02 05:51:53', '2018-11-02 05:51:53', 0, NULL, 4),
(166, 3, 'abhishek', 'rathore', 'a12345', NULL, NULL, NULL, 'abhishekar81@yahoo.com', NULL, NULL, NULL, '$2y$10$5Kaa8bePUlO0cpJEWr56K.RGD/Z2hWFpMvO4dTZw.SWneMlUubsCi', 'ERb0KwOUqFYvw1OhsvbOJ8cc11ocFpaMlZL6CPK3lBlgIfGEVty9cR8fgpkS', NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-11-02 06:50:57', '2018-11-02 06:52:30', 0, NULL, 0),
(167, 3, 'abhishek', 'rathore', 'a123454', NULL, NULL, NULL, 'wezu@wmail.club', NULL, NULL, NULL, '$2y$10$PdrIa.6e6gJa1rA2h9Ay9eCn2VX8auiB57dLrp3p/ALUMqLJt1xLG', '5IC0iPuxRm8jl0mbjMurBXj3bCqj5Y7UyE6AcVGouo2yKLv0Fkv8hpDNO1ZD', NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-11-02 06:54:51', '2018-11-02 06:55:06', 0, NULL, 0),
(168, 4, 'abhishek', 'rathore', 'a1234512121', 'Roshan', NULL, NULL, 'qysen@wmail.club', 'qysen@wmail.club', NULL, 'userImage/1542201562.banner_de.gif', '$2y$10$kVCTkd.59SlTPDvhdXOI9OB73pGiGbWwrkoUL7HslYdMWa1judjJS', 'uJHm9gQ8lOz6ll1hIBpfclJlvv8B40q5m7kdPZlDdQoLlajTEQ98aCxeFoWJ', NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-11-02 06:56:19', '2018-11-02 06:56:36', 0, NULL, 0),
(169, 4, 'abhishek', 'rathore', 'a1234578', NULL, NULL, NULL, 'pelowur@wmail.club', 'pelowur@wmail.club', NULL, NULL, '$2y$10$KBHXuDOsUAKjjqBUDRhMd.dqc5ri.F8Izo7mSUknfhKI18mk2hOb2', 'XeT2LC2TvIoC3VUz5jxMZ50X63t1yWgWnsgFWAJFKvsTPB3n7Iw1Pa2E6Xps', NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-11-02 06:59:48', '2018-11-02 06:59:59', 0, NULL, 0),
(170, 3, 'abhishek', 'rathore', 'a123451212', NULL, NULL, NULL, 'tatem@wmail.club', NULL, NULL, NULL, '$2y$10$w.UF.Ui8zImEhf1k4rrgROUcoieWfwe5maUQNWs7IMYIRtZ5ewWJq', 'l2uU21VVwrIW4MsGATtRM0QHy7ZZIIGP9ZAPtUV0HYguijefyLKkRq2xJLNG', NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-11-02 07:01:27', '2018-11-02 07:01:36', 0, NULL, 0),
(171, 3, 'ravi', 'mayuri', 'ravimayuri', NULL, NULL, NULL, 'ravimayuri@gmail.com', NULL, NULL, NULL, '$2y$10$.MfuVIRnP6KsfD7zDo6aLeskkSTew3Hws4sRCiw31HI8AXHncW6Cu', 'HFNEwuHfTVlrlFar7X7uloZWLESTkIo9vMziD6EOOxUskqrcIUuM9YLBUAt9', NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-11-02 08:11:12', '2018-11-02 08:12:37', 0, NULL, 0),
(172, 4, 'Edward', 'Starr', 'Edward Starr', NULL, NULL, NULL, 'futurestarr2012@yahoo.com', NULL, NULL, 'userImage/1541920966.Club vegas.PNG', '$2y$10$jwYcJsE.cH3yxhZrt5CWSull40FWZdlM4DbPKFnK1tLRG66P95F2u', 'ndC7KykuzDOcYZLtVIWhYDCaXgWyLCzSo4Z9PQ9gdHvnKAKn9MHp2mUBgaMR', '2349331475142200', 'FACEBOOK', NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-11-11 07:18:21', '2018-11-11 07:18:21', 0, NULL, 0),
(173, 3, 'Test', 'User', 'testuser', NULL, NULL, NULL, 'testing.gaugan2@gmail.com', NULL, NULL, NULL, '$2y$10$BCBlmjKicnsljUqNicou..jzdIhM/.Y/VtBVxYk9Nfb/23GlZar/6', NULL, NULL, NULL, NULL, 'no', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-11-11 16:09:27', '2018-11-11 16:09:27', 0, NULL, 4),
(174, 3, 'Marsels', 'Pencis', 'marsels', NULL, NULL, NULL, 'marsels.pencis@hotmail.com', NULL, NULL, NULL, '$2y$10$/77N6b7fY2iqEKZoHce./eLyrlOHUZV/lTUcxh91N1/64pqCZkprW', NULL, NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-11-11 16:23:54', '2018-11-11 16:25:03', 0, NULL, 0),
(175, 3, 'vikas', 'var', 'vikasvar', NULL, NULL, NULL, 'mobsoftpad@gmail.com', NULL, NULL, NULL, '$2y$10$TtVUnX.isc3jZH/nQ4qwW.ScvhW0t5v/jVE1aUfnWq4fh3Yh.E/a2', NULL, NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-11-15 09:25:09', '2018-11-15 09:34:55', 0, NULL, 0),
(176, 3, 'test', 'test', 'test', NULL, NULL, NULL, 'vishal.vdesks@gmail.com', NULL, NULL, NULL, '$2y$10$3Ad58RiPDGnRvWCtq.Nbh./8nRvxRVixhp4XuwyX4YpetiwOlUi/.', '7pwEpDllOl28oWj3ZW58m91B75suppD2gIvGFrOYPHudwmC1QFQsYXyidXXv', NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-11-16 07:18:19', '2018-11-16 07:22:53', 0, NULL, 0),
(177, 4, 'testnew', 'testnew', 'testnew', NULL, NULL, NULL, 'vishal.singh@vdesks.com', 'tarunvarshney17@gmail.com', NULL, NULL, '$2y$10$HhzbKI2rPvi3bU5IUqZCceivsIxEKvHkj5G20R24yL.cva5wYLvUO', NULL, NULL, NULL, NULL, 'no', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-11-16 07:21:29', '2018-11-16 07:21:29', 0, NULL, 4),
(178, 4, 'sellertest', 'sellertest', 'sellertest', NULL, NULL, NULL, 'er.vishusinghra@gmail.com', 'vishal.singh@vdesks.com', NULL, NULL, '$2y$10$hQ0lEKlvbW5jukREyZneKOwoT.Iq7WrgcSxY62YbZYSqENa8M43T6', NULL, NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-11-16 08:30:19', '2018-11-16 08:31:35', 0, NULL, 0),
(179, 3, 'sathish', 'kumar', 'satz', NULL, NULL, NULL, 'sathish.thi@gmail.com', NULL, NULL, NULL, '$2y$10$DWG74lW0ObZz/mkByNTIdep8LQe/1AvHMIEzYU62pGjDTdMYDAXaK', 'JZRTbkuXeFs3YgyoYq5E9hjMWODp5z0bzSJ0JTc5bqHh06l7IuWd2zXVMIB7', NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-11-20 15:25:09', '2018-11-20 15:28:56', 0, NULL, 0),
(180, 3, 'Clive', 'Mair', 'Mistamajahp', NULL, NULL, NULL, 'mistamajahp@yahoo.com', NULL, NULL, NULL, '$2y$10$gk9f7IzAcN3JXOVoXysxxeLpj3wv6ndqPSKT8c9GbhRgic.seioJO', NULL, NULL, NULL, NULL, 'no', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-11-20 19:50:20', '2018-11-20 19:50:20', 0, NULL, 4),
(181, 3, 'Eddie', 'Jones', 'Eddie Jones', NULL, NULL, NULL, 'jrayray2017@gmail.com', NULL, NULL, NULL, '$2y$10$e2NFjRL4fc80HhmIqUt2Pej9kScYmr55w4aVDc4D376J18KjJSMXK', 'bEhnQJ3zl6upe5G4LEib65RJpZAm3E6HL98DYbgcovMe2hARDkvJ1fBK6IA4', '2197006690625772', 'FACEBOOK', NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-11-24 02:59:40', '2018-11-24 02:59:40', 0, NULL, 0),
(182, 4, 'Ankita', 'Raikar', 'Ankita21', NULL, NULL, NULL, 'ankitaraikar2104@yahoo.in', 'ankitaraikar212@gmail.com', NULL, NULL, '$2y$10$HKdiZSQR9qigoX792iMB7OBdhvG6xLF.kIibPRN/EosThLFDwbkSW', NULL, NULL, NULL, NULL, 'no', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-12-05 05:30:27', '2018-12-05 05:30:27', 0, NULL, 4),
(183, 3, 'parag', 'khalas', 'parag104', NULL, NULL, NULL, 'parag.k@tridhya.com', NULL, NULL, 'userImage/1547628805.talent-mall15.jpg', '$2y$10$ho8.QQ8PIG92V5uA56F4Tup1pgUEYgYD22WSK33/RfdvJxJnRMMJO', 'SFY8wRd4sB0iINTTrLn7Rzs46WskUT3IkygWn5wQrZYXrXgs74DWc8iuK5Oq', NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-12-06 12:54:50', '2019-01-16 11:51:49', 0, NULL, 0),
(184, 3, 'Edward', 'Standley', 'Edward Standley', NULL, NULL, NULL, 'estandley2013@yahoo.com', NULL, NULL, NULL, '$2y$10$4U9Zpn8DNdEg3dX0wTA6/OvTqfaCIQ/ODxsSjp96VamdIrTYXGeVK', 'YHaQoP7xICNZzQd8HAhKZ1WxQq7IuGsqLZeJt5pd4oXDVp64RJVjmtIjadwN', 'estandley2013@yahoo.com', 'LINKEDIN', NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-12-15 14:12:28', '2019-04-19 07:49:28', 0, NULL, 0),
(185, 3, 'ED', 'Standley', 'ED Standley', NULL, NULL, NULL, 'jrayray1@gmail.com', NULL, NULL, NULL, '$2y$10$qFz2iSKF/t1Y6auZSSSd5.h60mV6liB7F6YekNrwmchVJlmNCZI72', 'ScMn6CvgLHPmiLt668AcC0Q4WZyBkdxy09a0F4TUzFKBD7vYCURLJWwgIhsP', '2415740031793643', 'FACEBOOK', NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-12-27 05:36:25', '2018-12-27 05:36:25', 0, NULL, 0),
(186, 4, 'Geezy', 'Ghettokaya', 'Geezyghettokaya', NULL, NULL, NULL, 'gideongidou0@gmail.com', 'gideongidou0@gmail.com', NULL, NULL, '$2y$10$kqYXEZVMf8Y.oJgrG78SFus/UGnyLEZ888HwIDbVMGDXauFJYC7de', NULL, NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2018-12-27 11:27:29', '2018-12-27 11:29:45', 0, NULL, 0),
(187, 4, 'Homeopathy Lagenda', 'Himalaya', 'Homeopathy Lagenda Himalaya', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$ViUcbnZH62fPEy0Wn2YdFOJPCoA34zHlduT7ytJQQG0CRYAFgvkv6', 'QSeUB4Gj6BD9M7egpanJmkIHXJw2ET2svCRomApe4QRT54KzkIXmkikkruLT', '119381429111696', 'FACEBOOK', NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-01-09 13:25:19', '2019-01-09 13:25:19', 0, NULL, 0),
(188, 3, 'Ditto', 'Joy', 'dittojoy', NULL, NULL, NULL, 'machoditto@gmail.com', NULL, NULL, NULL, '$2y$10$cVuODxE3fWy3sX23csv83Oe0F9XWzb0LaID17lfBz8ui.FMrtF.Q2', NULL, NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-01-17 07:38:07', '2019-01-22 10:05:04', 0, NULL, 4),
(189, 3, 'Farah', 'Diyana', 'Farah77', NULL, NULL, NULL, 'zirafarah@hotmail.com', NULL, NULL, NULL, '$2y$10$a9YGF7tPxdLAaATUmryYOOeF.rK1kuFv9qTTHwYUSMUx6n1g6z6WC', 'a1lctEEk9s8K0hPbGudB5s5396beZbniU6sXnrmvUiQYGs6JAOhb5e0lSAhT', NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-01-22 08:46:34', '2019-01-22 08:47:03', 0, NULL, 0),
(190, 3, 'Simon', 'Says', 'simonsays', NULL, NULL, NULL, 'spitfire.die.legende.lebt@web.de', NULL, NULL, NULL, '$2y$10$HMB/kFmjqMB0NeI4d8VOxu9zctIymMF/0GcFPj4/MNATtSqmvz3oG', NULL, NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-01-25 03:10:28', '2019-01-25 03:13:03', 0, NULL, 0),
(191, 4, 'php', 'testing', 'phptesting2018', NULL, NULL, NULL, 'phptesting2018@gmail.com', 'phptesting2018@gmail.com', NULL, NULL, '$2y$10$kRLk8vCMT6chiqT5F3PLc.WHXqKRf9/y1jFE7msDmFvSMKZXkl8lO', '6Liq7BFvRssrYIXIEFrwwQmaH0gTGXPJIdQ5JnktRLtHSrKRH31EfEl4frBZ', NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-01-25 05:48:58', '2019-03-04 10:07:22', 0, NULL, 0),
(192, 3, 'Ashley', 'Lamb', 'Lambashley191', NULL, NULL, NULL, 'lambashley191@gmail.com', NULL, NULL, NULL, '$2y$10$6DhNdGLHsachEqrwWfLsOuLIxuchLwAe9LbKlHQihjI3ULdcVwcgC', NULL, NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-02-09 04:31:43', '2019-02-09 04:33:58', 0, NULL, 0),
(193, 4, 'demo', 'demo', 'demotest', NULL, NULL, NULL, 'demotest@feb.com', 'demotest@feb.com', NULL, NULL, '$2y$10$0HXxZc6w42OMjU28ZAdNueByxx9ME.ITRC6RXSN.byjZsB2RJ3UsG', NULL, NULL, NULL, NULL, 'no', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-02-20 10:40:19', '2019-02-20 10:40:19', 0, NULL, 4),
(194, 4, 'Uttkarsh', 'Yadav', 'Uttkarsh Yadav', NULL, NULL, NULL, 'rockersyadav@gmail.com', NULL, NULL, NULL, '$2y$10$vnbi.KL0LdyAuIKgcaBTKui3gx4ZJRStFH/qncZIDpsETQTkkoUQ6', 'um49KBU6k0rNukjnVj6NwGCK4IE8cfFe9HrXzFfrltZSiXmULO6TeWZSw1rE', '10211099667527184', 'FACEBOOK', NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-02-27 15:39:42', '2019-02-27 15:39:42', 0, NULL, 0),
(195, 4, 'Ajay', 'Karki', 'Ajay22', NULL, NULL, NULL, 'ajaykarki20@gmail.com', 'Uttkarshyadav@gmail.com', NULL, NULL, '$2y$10$5ia3xvUZBHrfjPeJPWi9CeVmNY6hZYiWQuhp1XWcff6QCPn7ot.nq', NULL, NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-02-27 15:45:59', '2019-02-27 15:48:36', 0, NULL, 0),
(196, 3, 'Avi', 'Singh', 'Avisingh', NULL, NULL, NULL, 'avi.singh@stercodigitex.com', NULL, NULL, NULL, '$2y$10$2h6Uhen3u06o7yAyFFDVnuhwXCr06gm7M8oOomlQXXPD0WVQq90tG', NULL, NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-03-12 06:25:37', '2019-03-12 06:59:26', 0, NULL, 0),
(197, 3, 'E', 'Ski', 'Eski', 'Eski Money Baby!', '2222222222', NULL, 'jrayray2@gmail.com', NULL, 'acct_1EeMQHDDEqW93OvQ', 'userImage/1552721575.Future Starr.PNG', '$2y$10$.ds445Zg8hQaoYx4p5nQ9Ooqc4fZkGnm15foKTB6YgRtHAemVaOWq', 'CemIan1a8qPAI8nZg5miHhiY1xzsmBlHBL6Tihqrwp8DbNCSKIyYgZg6Kf5e', NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-03-16 07:10:30', '2019-03-16 07:12:31', 0, NULL, 0),
(198, 4, 'Ana', 'Ulan', 'ThatGirl', NULL, NULL, NULL, 'ulananita3@gmail.com', 'ulananita3@gmail.com', NULL, NULL, '$2y$10$0KNoIfSckrMOs8j9EOWQD.memIyWoYWPo0eBJv88oasHHsm7w9vMS', NULL, NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-03-17 08:43:32', '2019-03-17 08:43:47', 0, NULL, 0),
(199, 3, 'Shashank', 'Sharma', 'Shaan', NULL, NULL, NULL, 'shashankmehta0203@gmail.com', NULL, NULL, NULL, '$2y$10$dtNgTqsQbhqM7gdRe3OpneEWoHNE6imPI3Re4i.g3WLAWXZ/988N2', NULL, NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-03-18 03:48:35', '2019-03-18 04:10:15', 0, NULL, 0),
(200, 3, 'Shashank', 'Sharmaaa', 'Earthshaan123', NULL, NULL, NULL, 'shashankmehta0204@gmail.com', NULL, NULL, NULL, '$2y$10$NngtAD.o.s9bkswmZmoJGuwIRedYHJNvKd4X6DrprHpOEW6q6Wjm6', NULL, NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-03-18 04:11:57', '2019-03-18 04:12:09', 0, NULL, 0),
(201, 3, 'Raj', 'J', 'raj13', NULL, NULL, NULL, 'raj@mailinator.com', NULL, NULL, NULL, '$2y$10$3oN70XZTW6cSX0qaPXEpGuRSls9UAlPz4/98PZmN2tbr9c1aakmAS', NULL, NULL, NULL, NULL, 'no', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-03-23 05:59:42', '2019-03-23 05:59:42', 0, NULL, 4),
(202, 3, 'Rahul', 'J', 'rahul13', 'Rahul J', NULL, NULL, 'rahul@mailinator.com', NULL, NULL, 'userImage/1553321187.images.jpg', '$2y$10$gf14ZaXgN.hsct7Kqno1U.WtRD0t2yMXYgY2RvzR5SQD7V.xQCHz.', NULL, NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-03-23 06:02:33', '2019-03-23 06:03:12', 0, NULL, 0),
(203, 3, 'test', 'test', 'test0308', NULL, NULL, NULL, 'jainpiyush68@gmail.com', NULL, NULL, NULL, '$2y$10$SK4tVegroFvKXX9gGdMLV.99HfJkOelI.BCpTjJGxWzVk9iPI8k8K', NULL, NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-03-23 14:09:14', '2019-03-23 14:09:37', 0, NULL, 0),
(204, 3, 'a', 'a', 'aa', NULL, NULL, NULL, 'whitecalm@outlook.com', NULL, NULL, NULL, '$2y$10$y8jxMJwsipqMAjk0He0zluExE/S1A3iDfD4UMrJ3huQVO3wOjVqvC', NULL, NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-03-23 19:00:44', '2019-03-23 19:01:08', 0, NULL, 0),
(205, 3, 'rutu', 'dev', 'rutu', 'rutu', NULL, NULL, 'rzdevloper@gmail.com', NULL, NULL, 'userImage/1553450562.temp-calender.PNG', '$2y$10$WUOzd0jjihov1YeVVJc6Ken/Oh6K0ldLk4knJcVYSxkWtK60eKqxC', NULL, NULL, NULL, NULL, 'yes', 'off', NULL, 'tst', 'Public', NULL, NULL, NULL, NULL, NULL, '2019-03-23 19:47:27', '2019-03-23 19:47:42', 0, NULL, 0),
(206, 4, 'Lakhveer Singh', 'Rajewalia', 'Lakhveer Singh Rajewalia', NULL, NULL, NULL, 'lakhveer.singh51@gmail.com', NULL, NULL, NULL, '$2y$10$/TgA3QVFiAuWfW9Tsm4MgOMiexjVupLUTda9nM32Hq.SYL.h3EZ5O', NULL, '1482470525217051', 'FACEBOOK', NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-03-23 20:14:50', '2019-03-23 20:14:50', 0, NULL, 0),
(207, 4, 'John', 'Doe', 'johndoe', NULL, NULL, NULL, 'johndoe@example.com', 'johndoe@exmaple.com', NULL, NULL, '$2y$10$YFXOu1Fkx/yenbgb.qpSVu4rv9FWM/QQLyvkk/zZXzAKvZDYHaDwi', NULL, NULL, NULL, NULL, 'no', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-03-25 04:27:30', '2019-03-25 04:27:30', 0, NULL, 4),
(208, 3, 'techapp', 'techapp', 'techapp', NULL, NULL, NULL, 'techappg@gmail.com', NULL, NULL, NULL, '$2y$10$HZeStowuWXYo/JR5jlGj9uAzmrw/dPwxAODbRJ.yx8VRCWaiYy0KC', 'BeCoo754Y9B1J6ljf8tJW8uxwfN5Q3wm7Q4pXQaWdqgiwrMboFzbSeH5dclf', NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-03-25 05:00:12', '2019-03-25 05:02:50', 0, NULL, 0),
(209, 4, 'anuj', 'setia', 'anujsetia', NULL, NULL, NULL, 'anujsetia8@gmail.com', 'anujsetia8@gmail.com', NULL, NULL, '$2y$10$7./aw7eerbgh2tIDoUwdpO35EYRIYzVA6IeAx8nkFCJwZ0u5cCNP.', NULL, NULL, NULL, NULL, 'no', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-03-25 05:25:51', '2019-03-25 05:26:55', 0, NULL, 4),
(210, 3, 'test', 'test', 'test535', NULL, NULL, NULL, 'test@cmail.in', NULL, NULL, NULL, '$2y$10$g4D9ChTZbdXddNLPC/clYuZ0kWGTbEHpDQQWTwLfjHcX1u15er6j.', NULL, NULL, NULL, NULL, 'no', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-03-25 12:17:14', '2019-03-25 12:17:14', 0, NULL, 4),
(211, 3, 'anjali', 'tanwar', 'Anjalitanwar', 'Anjali tanwar', '12121212121212123', 'Sector-74, Mohali 8b,', 'anjali.tanwar@walkwel.in', NULL, NULL, 'userImage/1553579263.music-1914913__340.png', '$2y$10$J4JF7/0fLVLx4bnaiv7zpOyzhhNk0Us9dC.odQ3gsy99jMEvkPv4a', 'PT3THaizcZ3W2P8K7DLvJL2eMbsbfUbkfaOlL30hUDPUM5YltxDNwcbMPah2', NULL, NULL, NULL, 'yes', 'off', NULL, 'buyer information', 'Public', 'mohali', '6677', '165000', NULL, NULL, '2019-03-26 05:41:25', '2019-03-26 11:04:57', 1, 'status in on', 0),
(212, 4, 'lakshay', 'verma', 'Lakshayverma', 'lakshay verma', '8872228074', 'Sector-71, mohali punjab', 'lakshay.verma@walkwel.in', 'developer.walkwel@gmail.com', NULL, 'userImage/1553581452.Music-Notes.jpg', '$2y$10$z2r5E3waQllkD26e/Gh3ieUzxuIwFZJp4KXnMaVXyL7j8IXfCSyxm', 'b6Gt0Z0AjpJGaCXGLtaPOZ0NyJ5I1kFMzYlchYFBMC1xCv71KFoSGHHo6PK4', NULL, NULL, NULL, 'yes', 'off', 'Bignner', NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-03-26 06:12:06', '2019-03-26 06:17:12', 1, 'send automatic', 0),
(213, 3, 'Roshan', 'parmar', 'roshanparmar123', NULL, NULL, NULL, 'roshanparmar67781@gmail.com', NULL, NULL, NULL, '$2y$10$FW4rcJA/ELnuwlSwnzRQmemr0AvmWNbWTtbLdONh4D2kM4jBJ1wX.', NULL, NULL, NULL, NULL, 'no', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-03-26 07:17:44', '2019-03-26 07:17:44', 0, NULL, 4),
(214, 3, 'abhishek', 'rathore', 'adtech', 'aaa', NULL, NULL, 'lavufijo@getnada.com', NULL, NULL, NULL, '$2y$10$QDc54rrjUh0UTPBC2Zi0huyhAVhquUpUOaaKMQq3pCSdcpzyUyfH6', 'honbg9uIknTLEHuxnrmS7Kmp3mAyP3kSZDGvdhXc6wP2MSbxoJcotJDLpPRe', NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-03-26 07:23:01', '2019-03-26 07:23:34', 0, NULL, 0),
(215, 4, 'abhishek', 'rathore', 'adalfa', NULL, NULL, NULL, 'capi@getnada.com', 'capi@getnada.com', NULL, NULL, '$2y$10$02R2WMCq9oRg/KbIdpLFpufvkmB/Uv4AmgaVY4VkSC6gp9RN3AxOO', 'B2NZvT2ZaEVpn1c1ufwoEcw8AkJOFEMz3WIu4tFaeiPRgwGnJhWoRTWEQ7Ux', NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-03-26 07:25:49', '2019-03-26 07:25:57', 0, NULL, 0),
(216, 3, 'testuss', 'uss', 'testusr', NULL, NULL, NULL, 'zaaaari1@gmail.com', NULL, NULL, NULL, '$2y$10$gfDa3Ftah/Dt60t8I2953OFy4J1E7myMyi6JWGJeW7zODvYO9pf6m', NULL, NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-03-26 10:59:14', '2019-03-26 10:59:42', 0, NULL, 0),
(218, 3, 'Debra', 'Johanssen', 'Debra Johanssen', NULL, NULL, NULL, 'tpbqlcific_1541428749@tfbnw.net', NULL, NULL, NULL, '$2y$10$I9tMXME660tbAZys.w4HEO/2wyBY1GmC.32ClNvFNdlQsAfafUkaq', NULL, '10150060484266600', 'FACEBOOK', NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-03-26 21:44:44', '2019-03-26 21:44:44', 0, NULL, 0),
(219, 3, 'Vishnu', 'Surendranath', 'vinsnathan', NULL, NULL, NULL, 'vins.nathan@gmail.com', NULL, NULL, NULL, '$2y$10$7TC2goAMqKxRSeDEwG0X3ul3OMwAz.ErhDwkayQ5h5J6FL3LieKu.', NULL, NULL, NULL, NULL, 'no', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-03-27 09:33:54', '2019-03-27 09:33:54', 0, NULL, 4),
(220, 4, 'bhaskar', 'bhatt', 'bhaskarseller', NULL, NULL, NULL, 'bhatt.bhaskar88@gmail.com', 'bhaskar.b.upwork@gmail.com', NULL, NULL, '$2y$10$xdVOM92qDhszP267PRqYmOlGY70nGIao3aDbAY9ggqZpAkWVXmGOW', NULL, NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-04-02 18:57:58', '2019-04-10 17:56:58', 0, NULL, 4),
(221, 4, 'Bhaskar', 'Bhatt', 'bhaskar1', NULL, NULL, NULL, 'bhaskar.b.upwork.1@gmail.com', 'bhaskar.b.upwork.1@gmail.com', NULL, NULL, '$2y$10$YFb7cNipPuhROgWknNGBauTPFAuLxWt1bbqQw.tMGPlHOgbW0ln/S', '0FoPwUJPIYGLGWbAfGH3dNNCeyOzorjQ3YA1KYSfwUsVxj2QLIpqEfCLgtlP', NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-04-02 19:21:00', '2019-04-02 19:21:00', 0, NULL, 1),
(222, 3, 'test', 'test', 'buyertest', NULL, NULL, NULL, 'chirutest1@gmail.com', NULL, NULL, NULL, '$2y$10$r9tj96ZMhvUHfxEFDkQlTOyEp98gnsQsLMA1qZC0oQmfmnaxgvuW2', NULL, NULL, NULL, NULL, 'no', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-04-09 09:54:49', '2019-04-09 09:54:49', 0, NULL, 4),
(223, 4, 'test', 'test', 'testseller', NULL, NULL, NULL, 'EuniceJBastian@teleworm.us', 'chiru@gmail.com', NULL, NULL, '$2y$10$cdXKoNo9x3oxHa4kipFPfOv5zSPutt0OG5bwmCwGXwzxPscPwB6d2', NULL, NULL, NULL, NULL, 'no', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-04-09 09:55:50', '2019-04-09 09:55:50', 0, NULL, 4),
(224, 4, 'test', 'test', 'sellertest2', NULL, NULL, NULL, 'chiru689@gmail.com', 'chiru689@gmail.com', NULL, NULL, '$2y$10$dIgP/4qlJBosFVpNn7m0jeYR66T3aph4MUeVMzswHo30L.rCnif3i', 'qBmdLnBWrOaXoVTKZnFgjQyQRGbREDyYBNBpbW6KrcAfTlHqipu0NQ6BbWbI', NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-04-09 09:57:18', '2019-04-09 09:58:07', 0, NULL, 0),
(227, 3, 'Rahul', 'Solanki', 'rahul', NULL, NULL, NULL, 'rahul.botad@gmail.com', NULL, NULL, NULL, '$2y$10$LoM4rY3bsbMlqXM.TfAYWeqJz0J2C2JddbmfZqppS7O0dC7swjKbq', NULL, NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-04-09 18:00:14', '2019-04-09 18:00:54', 0, NULL, 0),
(228, 4, 'Johnny', 'Joseph', 'BlizzyInkGalley', NULL, NULL, NULL, 'johnnyjosephfcc@gmail.com', 'johnnyjosephfcc@gmail.com', NULL, NULL, '$2y$10$5khtsYKbZqBH69Y.QooeGeX8lpvIJZDFMtGOSrsdHmVS63agG23G.', NULL, NULL, NULL, NULL, 'no', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-04-14 10:57:14', '2019-04-14 10:57:14', 0, NULL, 4),
(230, 3, 'Bhaskar', 'Bhatt', 'bhaskarb', NULL, NULL, NULL, 'Bhaskar.b.upwork@gmail.com', NULL, NULL, NULL, '$2y$10$Lsg2FE7X9bQ6LSh.T2EvhuCDbv.7.vlk/TcPJJi2CrXidc0oYRjJG', NULL, NULL, NULL, NULL, 'no', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-04-18 19:42:11', '2019-04-18 19:42:11', 0, NULL, 4),
(234, 3, 'Bhaskar', 'Bhatt', 'bhaskar1112', NULL, NULL, NULL, 'bhatt.bhaskar1112@gmail.com', NULL, NULL, NULL, '$2y$10$ent/NO6/DSRTDc/wv81fg.6PV1tIytlmyiskcCpEbpBbLOaJG3FHC', NULL, NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-04-24 18:06:42', '2019-04-24 19:39:00', 0, NULL, 0),
(235, 4, 'Shreya', 'Tewari', 'shreya', NULL, NULL, NULL, 'shreya.twr8@gmail.com', 'shreya.twr8@gmail.com', NULL, NULL, '$2y$10$qTAsNlt7D23kjiIUbkdQouP8M0MWTvv8J2KrdtW0ZQyMhHowvII1y', NULL, NULL, NULL, NULL, 'yes', 'off', NULL, NULL, 'Public', NULL, NULL, NULL, NULL, NULL, '2019-05-03 23:58:38', '2019-05-03 23:59:11', 0, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_carts`
--

CREATE TABLE `user_carts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT '1',
  `title` varchar(225) DEFAULT NULL,
  `total_amount` varchar(225) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_carts`
--

INSERT INTO `user_carts` (`id`, `user_id`, `status_id`, `title`, `total_amount`, `quantity`, `active`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(2, 45, 1, 'cart _45', '0', 0, 0, 45, 45, '2019-04-14 12:19:55', '2019-04-14 12:19:55'),
(3, 1, 1, 'cart _1', '0', 0, 0, 1, 1, '2019-04-14 12:19:55', '2019-04-14 12:19:55'),
(4, 63, 1, 'cart _63', '0', 0, 0, 63, 63, '2019-04-14 12:19:55', '2019-04-14 12:19:55'),
(5, 81, 1, 'cart _81', '0', 0, 0, 81, 81, '2019-04-14 12:19:55', '2019-04-14 12:19:55'),
(6, 90, 1, 'cart _90', '0', 0, 0, 90, 90, '2019-04-14 12:19:55', '2019-04-14 12:19:55'),
(7, 92, 1, 'cart _92', '0', 0, 0, 92, 92, '2019-04-14 12:19:55', '2019-04-14 12:19:55'),
(8, 96, 1, 'cart _96', '0', 0, 0, 96, 96, '2019-04-14 12:19:55', '2019-04-14 12:19:55'),
(9, 99, 1, 'cart _99', '0', 0, 0, 99, 99, '2019-04-14 12:19:55', '2019-04-14 12:19:55'),
(10, 104, 1, 'cart _104', '0', 0, 0, 104, 104, '2019-04-14 12:19:55', '2019-04-14 12:19:55'),
(11, 113, 1, 'cart _113', '0', 0, 0, 113, 113, '2019-04-14 12:19:55', '2019-04-14 12:19:55'),
(12, 181, 1, 'cart _181', '0', 0, 0, 181, 181, '2019-04-14 12:19:55', '2019-04-14 12:19:55'),
(13, 184, 1, 'cart _184', '-23', -1, 0, 184, 184, '2019-04-19 07:50:14', '2019-04-19 07:50:14'),
(14, 185, 1, 'cart _185', '0', 0, 0, 185, 185, '2019-04-14 12:19:55', '2019-04-14 12:19:55'),
(15, 183, 1, 'cart _183', '0', 0, 0, 183, 183, '2019-04-14 12:19:55', '2019-04-14 12:19:55'),
(16, 189, 1, 'cart _189', '0', 0, 0, 189, 189, '2019-04-14 12:19:55', '2019-04-14 12:19:55'),
(17, 197, 1, 'cart _197', '500', 1, 0, 197, 197, '2019-06-11 18:13:38', '2019-06-11 12:43:38'),
(18, 202, 1, 'cart _202', '0', 0, 0, 202, 202, '2019-04-14 12:19:55', '2019-04-14 12:19:55'),
(19, 198, 1, 'cart _198', '0', 0, 0, 198, 198, '2019-04-14 12:19:55', '2019-04-14 12:19:55'),
(20, 204, 1, 'cart _204', '0', 0, 0, 204, 204, '2019-04-14 12:19:55', '2019-04-14 12:19:55'),
(21, 205, 1, 'cart _205', '0', 0, 0, 205, 205, '2019-04-14 12:19:55', '2019-04-14 12:19:55'),
(22, 211, 1, 'cart _211', '0', 0, 0, 211, 211, '2019-04-14 12:19:55', '2019-04-14 12:19:55'),
(23, 214, 1, 'cart _214', '0', 0, 0, 214, 214, '2019-04-14 12:19:55', '2019-04-14 12:19:55'),
(24, 216, 1, 'cart _216', '0', 0, 0, 216, 216, '2019-04-14 12:19:55', '2019-04-14 12:19:55'),
(25, 217, 1, 'cart _217', '0', 0, 0, 217, 217, '2019-04-21 12:12:25', '2019-04-21 12:12:25'),
(26, 125, 1, 'cart _125', '0', 0, 0, 125, 125, '2019-04-14 12:19:55', '2019-04-14 12:19:55'),
(27, 227, 1, 'cart _227', '0', 0, 0, 227, 227, '2019-04-14 12:19:55', '2019-04-14 12:19:55');

-- --------------------------------------------------------

--
-- Table structure for table `user_tokens`
--

CREATE TABLE `user_tokens` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `token` varchar(500) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_tokens`
--

INSERT INTO `user_tokens` (`id`, `user_id`, `token`, `created_at`, `updated_at`) VALUES
(84, 46, '5d5a754d2e55fdd481154cc3468f8a93', '2018-10-23 10:53:36', '2018-10-23 10:53:36'),
(120, 114, '392c01b2337703cdc9df59237893dbf1', '2018-10-23 12:18:16', '2018-10-23 12:18:16'),
(121, 115, 'dba71e3f9feec3fb00059e1a0c4ec374', '2018-10-23 12:18:17', '2018-10-23 12:18:17'),
(122, 118, 'ab90f468eef4f98c19e34446f1ba93aa', '2018-10-23 12:18:18', '2018-10-23 12:18:18'),
(123, 123, '13bb3c184b1e36953cfae99bade96d88', '2018-10-23 12:18:19', '2018-10-23 12:18:19'),
(124, 127, '9f24da339d5288721928f10d25574c37', '2018-10-23 12:18:20', '2018-10-23 12:18:20'),
(145, 155, '8cdd34b0d6ea26cb0d8dab01dac4a39e', '2018-10-27 06:50:01', '2018-10-27 06:50:01'),
(146, 160, '1fa3f98567accada9fac364fa30d2e30', '2018-10-27 07:48:42', '2018-10-27 07:48:42'),
(151, 165, '249faa92b7e5db11eda25a3c62dd23b2', '2018-11-02 05:51:53', '2018-11-02 05:51:53'),
(158, 172, '0421a4ab82fcd822df0181d5946e7813', '2018-11-11 07:18:21', '2018-11-11 07:18:21'),
(159, 173, '9dfb5ed89d6c9da2d42f25597e2b9f26', '2018-11-11 16:09:27', '2018-11-11 16:09:27'),
(163, 177, '1649444d44fac658823fb0d08d5ac38a', '2018-11-16 07:21:29', '2018-11-16 07:21:29'),
(166, 180, '704070e1c217f2f0f6c8ef2617de120e', '2018-11-20 19:50:20', '2018-11-20 19:50:20'),
(167, 181, '6ccf0a0e8c7c97a74a592780b103f7b0', '2018-11-24 02:59:40', '2018-11-24 02:59:40'),
(168, 182, 'd27f65a311e11b5492c5db2463a2fb23', '2018-12-05 05:30:27', '2018-12-05 05:30:27'),
(171, 185, '43b4e0820869734b5e3ee1769bb6bb1f', '2018-12-27 05:36:25', '2018-12-27 05:36:25'),
(173, 187, '6eeb981704b62ae7d4871dd549c9caf3', '2019-01-09 13:25:19', '2019-01-09 13:25:19'),
(175, 193, 'fa868b9553bb006558cf5f4e39ebbbe9', '2019-02-20 10:40:19', '2019-02-20 10:40:19'),
(177, 194, 'bd831b4945c7d407197a522455fddbf6', '2019-02-27 15:39:42', '2019-02-27 15:39:42'),
(185, 201, '7eabc1e239939f0e720150f33f79d6ba', '2019-03-23 05:59:42', '2019-03-23 05:59:42'),
(190, 206, 'a2ebef96ade6a2f80168daee6d429827', '2019-03-23 20:14:50', '2019-03-23 20:14:50'),
(191, 207, 'ade4a445458abc5564f1c341c381ed3f', '2019-03-25 04:27:30', '2019-03-25 04:27:30'),
(195, 210, '503e0196d6534019c1ce014ea43ad9bb', '2019-03-25 12:17:14', '2019-03-25 12:17:14'),
(198, 213, '0ce7bbaae47b654052042ab8a8d8e686', '2019-03-26 07:17:44', '2019-03-26 07:17:44'),
(200, 214, 'b4229acb112d4e6f2e65cabce40ffd67', '2019-03-26 07:24:06', '2019-03-26 07:24:06'),
(205, 218, 'fdc5c18907f1164f4b5c59b29ee7891d', '2019-03-26 21:44:44', '2019-03-26 21:44:44'),
(206, 219, '4fee4612a45007a714a28fa55b642d2a', '2019-03-27 09:33:54', '2019-03-27 09:33:54'),
(209, 221, '10f947d863a2658c889006ba4d15a186', '2019-04-02 19:21:00', '2019-04-02 19:21:00'),
(211, 222, 'ca312655afd10b2279ce9059df2085cf', '2019-04-09 09:54:49', '2019-04-09 09:54:49'),
(212, 223, '25c5b4fe2b76f7d9aedd24665d384fb8', '2019-04-09 09:55:50', '2019-04-09 09:55:50'),
(217, 228, '41ed105dc47cc1169c1d56c137e3c68e', '2019-04-14 10:57:14', '2019-04-14 10:57:14'),
(219, 230, '356f662654c619c20128e99572afff0c', '2019-04-18 19:42:11', '2019-04-18 19:42:11'),
(224, 234, '8bb3d65998e00af498e9d9bcc4153d2a', '2019-04-24 18:06:42', '2019-04-24 18:06:42'),
(225, 235, 'f51054d5b21483dd6278148178f22dcb', '2019-05-03 23:58:38', '2019-05-03 23:58:38');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog_comments`
--
ALTER TABLE `blog_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_contents`
--
ALTER TABLE `blog_contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `buyer_contacts`
--
ALTER TABLE `buyer_contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `buyer_products`
--
ALTER TABLE `buyer_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chats`
--
ALTER TABLE `chats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat_messages`
--
ALTER TABLE `chat_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `commercial_ads`
--
ALTER TABLE `commercial_ads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `commercial_ad_views`
--
ALTER TABLE `commercial_ad_views`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `commercial_media`
--
ALTER TABLE `commercial_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contactuses`
--
ALTER TABLE `contactuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `custom_plans`
--
ALTER TABLE `custom_plans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favriote_user`
--
ALTER TABLE `favriote_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payment_histories`
--
ALTER TABLE `payment_histories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payment_histories_user_id_foreign` (`user_id`);

--
-- Indexes for table `payment_history`
--
ALTER TABLE `payment_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_statuses`
--
ALTER TABLE `payment_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plans`
--
ALTER TABLE `plans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_media`
--
ALTER TABLE `product_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promote_products`
--
ALTER TABLE `promote_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchased_products`
--
ALTER TABLE `purchased_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sale_statuses`
--
ALTER TABLE `sale_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sample_media`
--
ALTER TABLE `sample_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seller_audios`
--
ALTER TABLE `seller_audios`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seller_contacts`
--
ALTER TABLE `seller_contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seller_inboxes`
--
ALTER TABLE `seller_inboxes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seller_likes`
--
ALTER TABLE `seller_likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seller_plans`
--
ALTER TABLE `seller_plans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seller_sales`
--
ALTER TABLE `seller_sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipping_addresses`
--
ALTER TABLE `shipping_addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shipping_addresses_user_id_foreign` (`user_id`);

--
-- Indexes for table `social_accounts`
--
ALTER TABLE `social_accounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `social_accounts_user_id_foreign` (`user_id`);

--
-- Indexes for table `social_buzzs`
--
ALTER TABLE `social_buzzs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_buzz_awards`
--
ALTER TABLE `social_buzz_awards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_buzz_comments`
--
ALTER TABLE `social_buzz_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_buzz_replies`
--
ALTER TABLE `social_buzz_replies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_buzz_riders`
--
ALTER TABLE `social_buzz_riders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_login_profiles`
--
ALTER TABLE `social_login_profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_promote_users`
--
ALTER TABLE `social_promote_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_shares`
--
ALTER TABLE `social_shares`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `talents`
--
ALTER TABLE `talents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `talents_user_id_foreign` (`user_id`),
  ADD KEY `talents_talent_category_id_foreign` (`talent_category_id`);

--
-- Indexes for table `talent_awards`
--
ALTER TABLE `talent_awards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `talent_categories`
--
ALTER TABLE `talent_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `talent_comments`
--
ALTER TABLE `talent_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `talent_downloads`
--
ALTER TABLE `talent_downloads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `talent_favourites`
--
ALTER TABLE `talent_favourites`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `talent_multimedia`
--
ALTER TABLE `talent_multimedia`
  ADD PRIMARY KEY (`id`),
  ADD KEY `talent_multimedia_talent_id_foreign` (`talent_id`);

--
-- Indexes for table `talent_orders`
--
ALTER TABLE `talent_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `talent_orders_user_id_foreign` (`user_id`),
  ADD KEY `talent_orders_talent_id_foreign` (`talent_id`),
  ADD KEY `talent_orders_payment_id_foreign` (`payment_id`),
  ADD KEY `talent_orders_shipping_id_foreign` (`shipping_id`);

--
-- Indexes for table `talent_ratings`
--
ALTER TABLE `talent_ratings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `talent_riders`
--
ALTER TABLE `talent_riders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `provider_id` (`provider_id`),
  ADD UNIQUE KEY `paypal_email` (`paypal_email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_carts`
--
ALTER TABLE `user_carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_tokens`
--
ALTER TABLE `user_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog_comments`
--
ALTER TABLE `blog_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `blog_contents`
--
ALTER TABLE `blog_contents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `buyer_contacts`
--
ALTER TABLE `buyer_contacts`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `buyer_products`
--
ALTER TABLE `buyer_products`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `chats`
--
ALTER TABLE `chats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `chat_messages`
--
ALTER TABLE `chat_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;
--
-- AUTO_INCREMENT for table `commercial_ads`
--
ALTER TABLE `commercial_ads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `commercial_ad_views`
--
ALTER TABLE `commercial_ad_views`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `commercial_media`
--
ALTER TABLE `commercial_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=191;
--
-- AUTO_INCREMENT for table `contactuses`
--
ALTER TABLE `contactuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT for table `custom_plans`
--
ALTER TABLE `custom_plans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `favriote_user`
--
ALTER TABLE `favriote_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `payment_histories`
--
ALTER TABLE `payment_histories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `payment_history`
--
ALTER TABLE `payment_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;
--
-- AUTO_INCREMENT for table `payment_statuses`
--
ALTER TABLE `payment_statuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `plans`
--
ALTER TABLE `plans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `product_media`
--
ALTER TABLE `product_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=238;
--
-- AUTO_INCREMENT for table `promote_products`
--
ALTER TABLE `promote_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `purchased_products`
--
ALTER TABLE `purchased_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=222;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sale_statuses`
--
ALTER TABLE `sale_statuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sample_media`
--
ALTER TABLE `sample_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT for table `seller_audios`
--
ALTER TABLE `seller_audios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `seller_contacts`
--
ALTER TABLE `seller_contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `seller_inboxes`
--
ALTER TABLE `seller_inboxes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `seller_likes`
--
ALTER TABLE `seller_likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `seller_plans`
--
ALTER TABLE `seller_plans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `seller_sales`
--
ALTER TABLE `seller_sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shipping_addresses`
--
ALTER TABLE `shipping_addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `social_accounts`
--
ALTER TABLE `social_accounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `social_buzzs`
--
ALTER TABLE `social_buzzs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;
--
-- AUTO_INCREMENT for table `social_buzz_awards`
--
ALTER TABLE `social_buzz_awards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `social_buzz_comments`
--
ALTER TABLE `social_buzz_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;
--
-- AUTO_INCREMENT for table `social_buzz_replies`
--
ALTER TABLE `social_buzz_replies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `social_buzz_riders`
--
ALTER TABLE `social_buzz_riders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT for table `social_login_profiles`
--
ALTER TABLE `social_login_profiles`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `social_promote_users`
--
ALTER TABLE `social_promote_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `social_shares`
--
ALTER TABLE `social_shares`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `talents`
--
ALTER TABLE `talents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=292;
--
-- AUTO_INCREMENT for table `talent_awards`
--
ALTER TABLE `talent_awards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT for table `talent_categories`
--
ALTER TABLE `talent_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `talent_comments`
--
ALTER TABLE `talent_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `talent_downloads`
--
ALTER TABLE `talent_downloads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `talent_favourites`
--
ALTER TABLE `talent_favourites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `talent_multimedia`
--
ALTER TABLE `talent_multimedia`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `talent_orders`
--
ALTER TABLE `talent_orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `talent_ratings`
--
ALTER TABLE `talent_ratings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `talent_riders`
--
ALTER TABLE `talent_riders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=236;
--
-- AUTO_INCREMENT for table `user_carts`
--
ALTER TABLE `user_carts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `user_tokens`
--
ALTER TABLE `user_tokens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=226;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `payment_histories`
--
ALTER TABLE `payment_histories`
  ADD CONSTRAINT `payment_histories_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `shipping_addresses`
--
ALTER TABLE `shipping_addresses`
  ADD CONSTRAINT `shipping_addresses_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `social_accounts`
--
ALTER TABLE `social_accounts`
  ADD CONSTRAINT `social_accounts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `talents`
--
ALTER TABLE `talents`
  ADD CONSTRAINT `talents_talent_category_id_foreign` FOREIGN KEY (`talent_category_id`) REFERENCES `talent_categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `talents_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `talent_multimedia`
--
ALTER TABLE `talent_multimedia`
  ADD CONSTRAINT `talent_multimedia_talent_id_foreign` FOREIGN KEY (`talent_id`) REFERENCES `talents` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `talent_orders`
--
ALTER TABLE `talent_orders`
  ADD CONSTRAINT `talent_orders_payment_id_foreign` FOREIGN KEY (`payment_id`) REFERENCES `payment_histories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `talent_orders_shipping_id_foreign` FOREIGN KEY (`shipping_id`) REFERENCES `shipping_addresses` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `talent_orders_talent_id_foreign` FOREIGN KEY (`talent_id`) REFERENCES `talents` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `talent_orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_tokens`
--
ALTER TABLE `user_tokens`
  ADD CONSTRAINT `user_tokens_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
